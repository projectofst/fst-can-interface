#include "arms_torque_set.hpp"
#include "ui_arms_torque_set.h"
#include "COM_SerialPort.hpp"
#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "Console.hpp"
#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
#include "can-ids/Devices/IIB_CAN.h"
#include <QDebug>
arms_torque_set::arms_torque_set(QWidget *parent, QString nombre, int num):
    QWidget(parent)  ,
    ui(new Ui::arms_torque_set)
{
    ui->setupUi(this);
    ui->name->setText(nombre);
    current_value = -1;
    parameter = num;
    name = nombre;
}

void arms_torque_set::clear_value(){
    ui->current_value->setText("");
    current_value = 0;
    return;
}
void arms_torque_set::set_new_current_value(double new_value){
    ui->current_value->setText(QString::number(new_value));
    return;
}

double arms_torque_set::get_current_value(){
    return current_value;
}

int arms_torque_set::get_parameter(){
    return parameter;
}
arms_torque_set::~arms_torque_set()
{
    delete ui;
}

void arms_torque_set::on_send_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_ARM;
    msg.candata.data[1] = uint16_t(parameter);
    msg.candata.data[2] = uint16_t(ui->new_value->text().toInt());
    msg.candata.data[3] = 0;
    emit send_CAN(msg);
    if(name == "Torque Front [Nmm]"){
        msg.candata.data[0] = DEVICE_ID_IIB;
        msg.candata.data[1] = P_IIB_FL_MAX_TORQUE_OP;
        emit send_CAN(msg);
        msg.candata.data[1] = P_IIB_FR_MAX_TORQUE_OP;
        emit send_CAN(msg);
    }
    else if (name == "Torque Rear[Nmm]") {
        msg.candata.data[0] = DEVICE_ID_IIB;
        msg.candata.data[1] = P_IIB_RL_MAX_TORQUE_OP;
        emit send_CAN(msg);
        msg.candata.data[1] = P_IIB_RR_MAX_TORQUE_OP;
        emit send_CAN(msg);
    }
    else if (name == "Motor Speed [RPM]") {
        msg.candata.data[0] = DEVICE_ID_IIB;
        msg.candata.data[1] = P_IIB_MAX_RPM;
        emit send_CAN(msg);
    }
    return;
}


void arms_torque_set::get_values(){
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_GET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_ARM;
    msg.candata.data[1] = uint16_t (parameter);
    msg.candata.data[2] = msg.candata.data[3] = 0;
    emit send_CAN(msg);
    return;
}

void arms_torque_set::send_set_message(){
    if(ui->new_value->text() != ""){
        on_send_clicked();
    }
    return;
}

QString arms_torque_set::get_name(){
    return name;
}

QString arms_torque_set::return_current_text(){
    return ui->new_value->text();
}
void arms_torque_set::write_label(QString text){
    ui->new_value->setText(text);
    return;
}

