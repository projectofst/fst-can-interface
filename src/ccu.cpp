﻿#include "ccu.hpp"
#include "ui_ccu.h"
#include "CAN_IDs.h"
#include <QDebug>

CCU::CCU(QWidget *parent) :
    ui(new Ui::CCU)
{
    ui->setupUi(this);
    names_right << "PUMP RIGHT" << "FAN RIGHT";
    names_left  << "PUMP LEFT" << "FAN LEFT";
    dev_ids << DEVICE_ID_CCU_RIGHT << DEVICE_ID_CCU_LEFT;
    set_parameters << P_CCU_SET_PUMP << P_CCU_SET_FAN;
    get_parameters << P_CCU_GET_PUMP << P_CCU_GET_FAN;
    clear_timer = new QTimer(this);
    connect(clear_timer, &QTimer::timeout, this, &CCU::clear_values);
    clear_timer->start(3000);
    get_timer = new QTimer(this);
    connect(get_timer, &QTimer::timeout, this, &CCU::get_current_values);
    get_timer->start(1000);
    init();
}

CCU::~CCU()
{
    delete ui;
}


void CCU::init(){
    create_new(names_right[0], dev_ids[0], set_parameters[0], get_parameters[0]);
    create_new(names_left[0], dev_ids[1], set_parameters[0], get_parameters[0]);
    create_new(names_right[1], dev_ids[0], set_parameters[1], get_parameters[1]);
    create_new(names_left[1], dev_ids[1], set_parameters[1], get_parameters[1]);
    return;

}
void CCU::process_CAN_message(CANmessage msg){
    clear_timer->start(3000);
    uint16_t dev_id = msg.candata.dev_id;
    uint16_t msg_id = msg.candata.msg_id;
    if(dev_id == DEVICE_ID_CCU_LEFT || dev_id == DEVICE_ID_CCU_RIGHT){
        if(msg_id == CMD_ID_COMMON_GET){
            uint16_t new_value = msg.candata.data[2];
            uint16_t parameter =  msg.candata.data[1];
            for(int i = 0; i < lines.size(); i++){
                if((lines[i]->dev_id == dev_id) && (lines[i]->get_parameter == parameter)){
                    lines[i]->set_new_curr_value(new_value);
                }
            }
        }
    }
}

void CCU::retransmit_can(CANmessage msg){
    emit send_to_CAN(msg);
}

void CCU::on_send_all_clicked()
{
    for(int i = 0; i < lines.size(); i++){
        if(lines[i]->ui->value->text() != "")
            lines[i]->on_set_but_clicked();
    }
    return;
}

void CCU::create_new(QString name, uint16_t dev_id, uint16_t set_parameter, uint16_t get_parameter){
    ccu_indiv* ccu_line;
    ccu_line = new ccu_indiv(this, name, dev_id,set_parameter,get_parameter);
    ui->this_is_scroll_area->addWidget(ccu_line);
    connect(ccu_line, &ccu_indiv::send_CAN, this, &CCU::retransmit_can);
    lines.append(ccu_line);
    return;
}

void CCU::get_current_values(){
    for(int i = 0; i < lines.size(); i++){
        lines[i]->get_value();
    }
    return;
}

void CCU::clear_values(){
    for(int i = 0; i < lines.size(); i++){
        lines[i]->clear();
    }
    return;
}

void CCU::on_set_0_clicked()
{
    for(int i = 0; i < lines.size(); i++){
        lines[i]->send_custom_set_msg(0);
    }
    get_current_values();
    return;
}
