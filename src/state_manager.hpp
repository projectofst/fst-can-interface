#ifndef STATE_MANAGER_HPP
#define STATE_MANAGER_HPP

#include <QObject>
#include <QSettings>
#include <QApplication>
#include <QPair>
#include <QVector>
#include <QDir>
#include <QDebug>
class state_manager : public QObject
{
    Q_OBJECT
public:
    explicit state_manager(QObject *parent = nullptr);

signals:

public slots:
    void save(QString name, QString specific);
    QVector<QPair<QString, int>> load(QString name, QString specific);

    QString find_ini(QString name, QString specific);
    void add(QString name, int value);

private:
    QVector<QPair<QString, int>> added;

};

#endif // STATE_MANAGER_HPP
