/* TODO
 * replace table with unmutable list so we can remove lines correctly
 * Implement Log Play
 */


#include <QComboBox>
#include <QPushButton>
#include <QTableWidget>
#include <QScrollArea>
#include <QVBoxLayout>

#include "comselectline.hpp"
#include "COM_Select.hpp"
#include "COM_Manager.hpp"

COMSelect::COMSelect(QWidget *parent,Comsguy *parentcom) :
    UiWindow(parent),
    ui(new Ui::COMSelect)
{
    ui->setupUi(this);

    /*connect(ui->Add,&QPushButton::clicked,
            this,&COMSelect::generateTableLines);

    */
    connect(ui->Add,&QPushButton::clicked,
            this,&COMSelect::add_line);

    ComsManager = parentcom;

    COMSelectLine *new_line = new COMSelectLine(NULL,ComsManager);
    ui->connection_list->addWidget(new_line);
}

COMSelect::~COMSelect()
{
    delete ui;
}

void COMSelect::add_line(void) {

    COMSelectLine *new_line = new COMSelectLine(NULL,ComsManager);
    ui->connection_list->addWidget(new_line);
}
