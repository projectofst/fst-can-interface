#ifndef AMS_HPP
#define AMS_HPP

#include <QWidget>

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/BMS_VERBOSE_CAN.h"

#include "UiWindow.hpp"

#include "AMS_Details.hpp"
#include "AMS_Stack_Summary.hpp"
#include "AMS_Battery.hpp"

namespace Ui {
class AMS;
}

class AMS : public UiWindow
{
    Q_OBJECT

public:
    explicit AMS(QWidget *parent = nullptr);
    void timer_clear_ams(void);
    AMSDetails ams_details;
    void updateSumary(int stack);
    Ui::AMS *ui;
    ~AMS(void) override;

private:

    void clear_ams();
    int received_messages;
    Battery *battery;
    QVector<StackSummary *> stacks;
    int verbose_flag;

private slots:
    void update_details(void);
    void update_battery(void);
    void process_CAN_message(CANmessage) override;
    void on_pushButton_2_clicked();
    void on_pushButton_5_clicked();
    void on_pushButton_6_clicked();
    void on_pushButton_4_clicked();
    void on_pushButton_7_clicked();
    void on_pushButton_8_clicked();
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_10_clicked();

    void on_send_set_clicked();

signals:
    virtual void send_to_CAN(CANmessage) override;

};

#endif // AMS_HPP
