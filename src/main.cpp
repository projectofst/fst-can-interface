#include <QApplication>
#include <QObject>

#include "Main_Window.hpp"
#include "Main_Toolbar.hpp"
#include "COM_Manager.hpp"
#include "COM_Select.hpp"
#include "Picasso.hpp"

int main(int argc, char *argv[])
{
	QApplication GUI(argc, argv);
    QApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
    MainWindow *window = new MainWindow;

    MainToolbar *maintoolbar = new MainToolbar(window);

    Comsguy *comsguy = new Comsguy(window);
    comsguy->connectMainWin(window);

    new Picasso(maintoolbar,window,comsguy);

    COMSelect *comselect = new COMSelect(window,comsguy);
    
    window->setCentralWidget(comselect);

    QObject::connect(comselect->ui->Hide,&QPushButton::clicked,
                     maintoolbar,&MainToolbar::toggleCOM);

    window->show();
    comselect->show();
    comselect->activateWindow();
    comselect->raise();

	return GUI.exec();
}
