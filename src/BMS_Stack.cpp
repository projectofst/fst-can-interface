#include "BMS_Stack.hpp"
#include "ui_BMS_Stack.h"

StackT::StackT(QWidget *parent,uint Id) :
    UiWindow(parent),
    ui(new Ui::StackT)
{
    ui->setupUi(this);
    QString *IdText = new QString;
    ui->StackIdLabel->setText(IdText->sprintf("#%d",Id));
}

StackT::~StackT()
{
    delete ui;
}

void StackT::process_CAN_message(CANmessage message)
{
    int Temp = message.candata.data[1];
    int Volt = message.candata.data[2];

    if (Temp < MinTemp) {
        MinTemp = Temp;
        ui->MINTemp->setValue(MinTemp);
    }
    if (Temp > MaxTemp) {
        MaxTemp = Temp;
        ui->MAXTemp->setValue(MaxTemp);
    }
    if (Volt < MinVolt) {
        MinVolt = Volt;
        ui->MINVoltage->setValue(MinVolt);
    }
    if (Volt > MaxVolt) {
        MaxVolt = Volt;
        ui->MAXVoltage->setValue(MaxVolt);
    }
}
