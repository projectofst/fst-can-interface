#include "iib_motor_control.hpp"
#include "ui_iib_motor_control.h"

iib_motor_control::iib_motor_control(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::iib_motor_control)
{
    ui->setupUi(this);
}
void iib_motor_control::clear(){
    ui->IGBT_FL->clear();
    ui->IGBT_FR->clear();
    ui->IGBT_RL->clear();
    ui->IGBT_RR->clear();
    ui->Error_FL->clear();
    ui->Error_FR->clear();
    ui->Error_RL->clear();
    ui->Error_RR->clear();
    ui->Speed_FL->clear();
    ui->Speed_FR->clear();
    ui->Speed_RL->clear();
    ui->Speed_RR->clear();
    ui->MotorTemp_FL->clear();
    ui->Current_FL->clear();
    ui->Torque_FL->clear();
    ui->InvTemp_FL->clear();
    ui->FL_MT_LED->turnOff();
    ui->FL_CURRENT_LED->turnOff();
    ui->FL_INV_TEMP_LED->turnOff();
    ui->FL_MAX_OP_POWER->clear();
    ui->FL_IGBT_TEMP_LED->turnOff();
    ui->FL_MAX_OP_TORQUE->clear();
    ui->FL_MAX_SAF_POWER->clear();
    ui->FL_MAX_SAF_TORQUE->clear();
    ui->MotorTemp_FR->clear();
    ui->Current_FR->clear();
    ui->Torque_FR->clear();
    ui->InvTemp_FR->clear();
    ui->FR_MOTOR_TEMP_LED->turnOff();
    ui->FR_CURRENT_LED->turnOff();
    ui->FR_INV_TEMP_LED->turnOff();
    ui->FR_MAX_OP_POWER->clear();
    ui->FR_IGBT_TEMP_LED->turnOff();
    ui->FR_MAX_OP_TORQUE->clear();
    ui->FR_MAX_SAF_POWER->clear();
    ui->FR_MAX_SAF_TORQUE->clear();
    ui->MotorTemp_RR->clear();
    ui->Current_RR->clear();
    ui->Torque_RR->clear();
    ui->InvTemp_RR->clear();
    ui->RR_MOTOR_TEMP_LED->turnOff();
    ui->RR_INV_TEMP_LED->turnOff();
    ui->RR_MAX_OP_POWER->clear();
    ui->RR_IGBT_TEMP_LED->turnOff();
    ui->RR_MAX_OP_TORQUE->clear();
    ui->RR_MAX_SAF_POWER->clear();
    ui->RR_MAX_SAF_TORQUE->clear();
    ui->MotorTemp_RL->clear();
    ui->Current_RL->clear();
    ui->Torque_RL->clear();
    ui->InvTemp_RL->clear();
    ui->RL_MOTOR_TEMP_LED->turnOff();
    ui->RL_CURRENT_LED->turnOff();
    ui->RL_INV_TEMP_LED->turnOff();
    ui->RL_MAX_OP_POWER->clear();
    ui->RL_IGBT_LED->turnOff();
    ui->RL_MAX_OP_TORQUE->clear();
    ui->RL_MAX_SAF_POWER->clear();
    ui->RL_MAX_SAF_TORQUE->clear();
    return;
}

iib_motor_control::~iib_motor_control()
{
    delete ui;
}
