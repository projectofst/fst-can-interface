#include "comselectline.hpp"
#include "ui_comselectline.h"
#include "COM_Manager.hpp"

#include <QComboBox>
#include <QDebug>
#include <QMessageBox>

COMSelectLine::COMSelectLine(QWidget *parent, Comsguy *comsguy) :
    QWidget(parent),
    ui(new Ui::COMSelectLine)

{
    this->comsguy = comsguy;

    ui->setupUi(this);
    QList<QString> args = this->comsguy->ComList.keys();
    args.detach();
    ui->COM1->addItems(args);
    ui->COM2->addItems(args);

    int default_index;
    if ((default_index = ui->COM1->findText("Serial")) == -1) {
        default_index = 0;
    }

    ui->COM1->setCurrentIndex(default_index);

   if ((default_index = ui->COM2->findText("Interface@0")) == -1) {
       default_index = 0;
   }

   ui->COM2->setCurrentIndex(default_index);
}

COMSelectLine::~COMSelectLine()
{
    delete ui;
}

void COMSelectLine::on_pushButton_clicked()
{
    return;
}

void COMSelectLine::on_close_clicked()
{
    delete this;
}


void COMSelectLine::on_COM1_currentTextChanged(const QString &arg1)
{
    COM *targetcom = NULL;
    if (comsguy->ComList.contains(arg1)) {
        targetcom = comsguy->ComList.value(arg1);
    }
    else {
        return;
    }
    ui->address1->clear();
    ui->address1->addItems(targetcom->options());
    return;
}

void COMSelectLine::on_address1_currentTextChanged(const QString &arg1)
{
    return;
}


void COMSelectLine::on_COM2_currentTextChanged(const QString &arg1)
{
    COM *targetcom = NULL;
    if (comsguy->ComList.contains(arg1)) {
        targetcom = comsguy->ComList.value(arg1);
    }
    else {
        qDebug() << "Failed to find the requested COM";
        return;
    }
    ui->address2->clear();
    ui->address2->addItems(targetcom->options());
    return;
}

void COMSelectLine::on_address2_currentTextChanged(const QString &arg1)
{
    return;
}

void COMSelectLine::on_pushButton_clicked(bool checked)
{
    return;
}

void COMSelectLine::on_pushButton_toggled(bool checked)
{
    /* when we set the button state to false this slot runs again, so we need to
     * check who triggered the function: the user or the code
     */
    static bool visited = false;

    QString COM1 = ui->COM1->currentText();
    QString COM2 = ui->COM2->currentText();
    QString address1 = ui->address1->currentText();
    QString address2 = ui->address2->currentText();

    COM *targetcom1 = comsguy->ComList.value(COM1);
    COM *targetcom2 = comsguy->ComList.value(COM2);

    bool log = ui->log->isChecked();

    if (COM1 == "" || COM2 == "" || (COM1 == COM2)) {
        if (!visited) {
            visited = true;
            QMessageBox::information(this, tr("Interface"), tr("Failed to connect\nCheck your connection settings"));
            qDebug() << "Failed to connect";
            ui->pushButton->setChecked(false);
            return;
        }
        visited = false;
        return;
    }

    if (checked) {
        comsguy->connectCOM(COM1, address1, COM2, address2, log);
        if (targetcom1->status() == 0) {
            ui->pushButton->setChecked(false);
            return;
        }
        if (targetcom2->status() == 0) {
            ui->pushButton->setChecked(false);
            return;
        }
    }
    else {
        comsguy->disconnectCOM(COM1, COM2);
    }
}
