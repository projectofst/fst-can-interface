#ifndef COMSELECTLINE_HPP
#define COMSELECTLINE_HPP

#include "COM_Manager.hpp"

#include <QWidget>
#include <QStringList>

namespace Ui {
class COMSelectLine;
}

class COMSelectLine : public QWidget
{
    Q_OBJECT

public:
    explicit COMSelectLine(QWidget *parent=0, Comsguy *comsguy=NULL);
    ~COMSelectLine();

private slots:
    void on_pushButton_clicked();
    void on_close_clicked();
    void on_address1_currentTextChanged(const QString &arg1);
    void on_COM1_currentTextChanged(const QString &arg1);
    void on_COM2_currentTextChanged(const QString &arg1);
    void on_address2_currentTextChanged(const QString &arg1);

    void on_pushButton_clicked(bool checked);

    void on_pushButton_toggled(bool checked);

private:
    Ui::COMSelectLine *ui;
    Comsguy *comsguy;

signals:
    void close_com1(void);
    void close_com2(void);
};

#endif // COMSELECTLINE_HPP
