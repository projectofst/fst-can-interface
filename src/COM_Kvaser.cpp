#include "COM.hpp"
#include "COM_Kvaser.hpp"

#include "can-ids/CAN_IDs.h"

#include <QtSerialBus>
#include <QQueue>

#include <stdlib.h>

COM_Kvaser::COM_Kvaser()
{
    fifo = new QQueue<CANmessage>;
    Status = 0;
}

int COM_Kvaser::status(void)
{
    return Status;
}

bool COM_Kvaser::open(const QString)
{
	if (!QCanBus::instance()->plugins().contains(QStringLiteral("socketcan"))) {
        printf("Failed to find socket can\n");
		return false;
	}

    QString errorString;
    device1 = QCanBus::instance()->createDevice(
            QStringLiteral("socketcan"), QStringLiteral("can0"), &errorString);

    if (!device1) {
        // Error handling goes here
        qDebug() << errorString;
    } else {
        printf("Succesfull\n");
        device1->connectDevice();
    }

    device2 = QCanBus::instance()->createDevice(
                QStringLiteral("socketcan"), QStringLiteral("can1"), &errorString);

    if (!device2) {
          // Error handling goes here
          qDebug() << errorString;
      } else {
          printf("Succesfull\n");
          device2->connectDevice();
      }
    connect(device1, &QCanBusDevice::framesReceived, this, &COM_Kvaser::read);
    connect(device2, &QCanBusDevice::framesReceived, this, &COM_Kvaser::read);

    Status = 1;
    return true;
}

int COM_Kvaser::close(void)
{
    Status = -1;
    device1->disconnectDevice();
    delete device1;
    device2->disconnectDevice();
    delete device2;
    //delete this;
    return 0;
}

int COM_Kvaser::send(CANmessage msg)
{
    QCanBusFrame frame;
    frame.setFrameId(msg.candata.sid);
    QByteArray payload((const char *) msg.candata.data);
    frame.setPayload(payload);
    device1->writeFrame(frame);
    device2->writeFrame(frame);

    return 0;
}

CANmessage COM_Kvaser::pop(void)
{
    return fifo->dequeue();
}


void COM_Kvaser::read(void)
{
    QCanBusFrame frame;

    if (device1->framesAvailable()) {
        frame = device1->readFrame();
    }
    else if (device2->framesAvailable()) {
        frame = device2->readFrame();
    }
    else {
        return;
    }

    CANmessage msg;
    msg.timestamp = frame.timeStamp().microSeconds()/100;

    msg.candata.sid = frame.frameId();
    QByteArray payload = frame.payload();

    auto dlc = frame.payload().length();
    char *datas =  payload.data();


    for (int i=0; i<dlc/2; i++) {
        msg.candata.data[i] = (datas[2*i+1] << 8) + datas[2*i];
    }

    //fifo->enqueue(msg);

    emit new_messages(msg);


    return;

}

QStringList COM_Kvaser::options(void)
{
    return QStringList();
}
