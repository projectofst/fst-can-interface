#ifndef IIB_MOTOR_CONTROL_HPP
#define IIB_MOTOR_CONTROL_HPP

#include <QWidget>

namespace Ui {
class iib_motor_control;
}

class iib_motor_control : public QWidget
{
    Q_OBJECT

public:
    explicit iib_motor_control(QWidget *parent = nullptr);
    ~iib_motor_control();
    Ui::iib_motor_control *ui;
    void clear();
private:
};

#endif // IIB_MOTOR_CONTROL_HPP
