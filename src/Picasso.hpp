#ifndef PICASSO_H
#define PICASSO_H

// Qt Classes
#include <QtWidgets>
#include <QDockWidget>
#include <QList>
#include <QMetaEnum>
#include <QToolBar>

// Widgets
#include "General.hpp"
#include "Console.hpp"
#include "AMS.hpp"
#include "Inverters.hpp"
#include "Isabelle.hpp"
#include "Sensors.hpp"
#include "COM_LogGen.hpp"
#include "Plotting.hpp"
#include "MotorBench.hpp"
#include "DevStatus.hpp"
#include "Main_Toolbar.hpp"
#include "General_DriverInputs.hpp"
#include "Main_Window.hpp"
#include "UiWindow.hpp"
#include "BMS.hpp"
#include "versions.hpp"
#include "drs_cal.hpp"
#include "src/versions.hpp"
#include "versionline.hpp"
#include "memoryview.hpp"
#include "arm.hpp"
#include "ccu.hpp"
#include "lap_time.hpp"

// Other
#include "COM_Manager.hpp"
#include "COM_Interface.hpp"
#include "SQL_CANID.hpp"
#include "SQL_CANID_Select.hpp"

class Picasso : public QObject
{
    Q_OBJECT

public:
    Picasso(MainToolbar *,MainWindow *,Comsguy *);
    ~Picasso();

    enum Types {
            GenDock = 1,
            ConDock = 2,
            AMSDock = 3,
            InvDock = 4,
            IsaDock = 5,
            SenDock = 6,
            PloDock = 8,
            MTBDock = 9,
            DevDock = 10,
            DriDock = 11,
            BatDock = 12,
            ShuDock = 13,
            CDBDock = 14,
            VerDock = 15,
            DRS_cal = 16,
            MVDock = 17,
            logconsole = 18,
            Versions = 19,
            ArmDock = 20,
            ccu = 21,
            LapDock = 22



        }; Q_ENUM(Types)

    SQL_CANID *test;

    void updateObjectName(QObject *, QString);
    void closeLayout(void);
    MainToolbar *MainToolBar;

public slots:
    void saveLayout(void);
    void loadLayout(QString);
    void openDockWidget(QString type);

private:
    MainWindow *mainwindow;

    QDockWidget *dockwidget;
    UiWindow *widget;

        QList<QDockWidget*> ListOfChildren;
        QStringList ListOfTabDocks;
        QStringList ListOfDocks;
        int repeated;
        QString FinalName;

    QDir LayoutDir;
    QString LayoutName;
        void updateToolbar(void);

};
#endif
