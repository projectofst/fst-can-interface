#ifndef MAIN_TOOLBAR_HPP
#define MAIN_TOOLBAR_HPP

#include "Main_Window.hpp"

#include <QMainWindow>
#include <QtWidgets>
#include <QToolBar>
#include <QIcon>
#include <QAction>
#include <QApplication>

class MainToolbar : public QMainWindow {

  Q_OBJECT

public:
    MainToolbar(MainWindow *parent = nullptr);
    QToolBar *toolbar;
    QComboBox *LayoutCombo;
    QAction *COM;
    QAction *temp;
    MainWindow *mainwindow;

public slots:
    void widgetAction();
    void toggleCOM();

signals:
    void openUiWindow(QString);
    void saveLayout(void);
    void deleteLayout(void);
    void loadLayout(QString);
};

#endif // MAIN_TOOLBAR_HPP
