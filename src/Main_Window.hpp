#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include "AMS_Details.hpp"
#include "Main_About.hpp"
#include "General.hpp"
#include "AMS.hpp"
#include "Inverters.hpp"
#include "Report.hpp"
#include "Sensors.hpp"
#include "Resources/ledform.hpp"
#include "Sitrep.hpp"
#include "COM_Interface.hpp"

#include <QMainWindow>
#include <QDockWidget>
#include <QSettings>
#include <QPainter>
#include <QResizeEvent>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    Interface *interfaceCom;

public slots:
    void updateStatusBar();
    void actionToggleCOM_triggered();

private:
    Ui::MainWindow *ui;

protected:
  bool eventFilter(QObject *, QEvent *);
/*#ifndef QT_NO_CONTEXTMENU
    void contextMenuEvent(QContextMenuEvent *event) override;
#endif // QT_NO_CONTEXTMENU*/

private slots:
    void paintEvent(QPaintEvent *);
    void on_actionAbout_triggered();
    void on_actionOpen_Log_triggered();
    void on_actionNew_triggered();

signals:
    void resizeDev(int);
    void resizeBat(int);
    void broadcast(CANmessage);
    void send(CANmessage);
};

#endif
