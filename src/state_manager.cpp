#include "state_manager.hpp"

state_manager::state_manager(QObject *parent) : QObject(parent)
{

}

QVector<QPair<QString, int>> state_manager::load(QString name, QString specific){
    QVector<QPair<QString, int>> states;
    QPair<QString, int> pair;
    QString loc = QDir::currentPath() +"/set/states/" + name.toUpper()+ "_"+ specific.toUpper() + "_settings.ini";
    QSettings settings(loc, QSettings::NativeFormat);

    foreach(QString key, settings.allKeys()){
        pair.first = key;
        pair.second = settings.value(key, "").toInt();
        states.append(pair);
    }
    return states;
}

void state_manager::save(QString name, QString specific){
    QPair<QString, int> pair;
    QString loc = QDir::currentPath() +"/set/states/" + name.toUpper()+ "_"+ specific.toUpper() + "_settings.ini";
    QSettings settings(loc, QSettings::NativeFormat);
    foreach (pair, added) {
        settings.setValue(pair.first, pair.second);
    }
}
QString state_manager::find_ini(QString name, QString specific = "NO_INFO"){ /*Specific track*/
    QDir directory(QDir::currentPath()+ "/set/states/");
    QStringList inis = directory.entryList(QStringList() << "*.ini" ,QDir::Files);
    foreach(QString filename, inis) {
        if((filename.contains(name.toUpper())) && (specific == "")){
            return filename;
        }
        else if((filename.contains(name.toUpper())) && (name.contains(specific))){
            return filename;
        }
    }
    return "";
}

void state_manager::add(QString name, int value){
    QPair<QString, int> pair;
    pair.first = name;
    pair.second = value;
    added.append(pair);
}
