#ifndef SITREP_H
#define SITREP_H

#include <QObject>
#include <QPixmap>
#include <QImageReader>
#include <QLabel>
#include <QStatusBar>

namespace Ui {
    class Sitrep;
}

class Sitrep : public QObject
{
    Q_OBJECT

public:
    Sitrep();
    void connectionSuccess();
    void testerRunning();

private:
    QPixmap icon;

signals:
    void update_status(QString);

private slots:
    void update();
};

#endif // SITREP_H
