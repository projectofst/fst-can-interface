#ifndef ARMS_TORQUE_SET_HPP
#define ARMS_TORQUE_SET_HPP

#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "Console.hpp"
#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
namespace Ui {
class arms_torque_set;
}

class arms_torque_set : public QWidget
{
    Q_OBJECT

public:
    explicit arms_torque_set(QWidget *parent = nullptr, QString nombre = "", int num = 0);
    ~arms_torque_set();
    void set_new_current_value(double new_value);
    void get_values();
    int get_parameter();
    void send_set_message();
    double get_current_value();
    void clear_value();
    QString get_name();
    QString return_current_text();
    void write_label(QString text);


signals:
    void send_CAN(CANmessage);

private slots:
    void on_send_clicked();


private:
    int parameter;
    double current_value;
    QString name;
    Ui::arms_torque_set *ui;
};

#endif // ARMS_TORQUE_SET_HPP
