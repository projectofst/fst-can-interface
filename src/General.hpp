#ifndef GENERAL_H
#define GENERAL_H

#include <QWidget>
#include <QMap>

#include "ledform.hpp"
#include "General_Details.hpp"
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "SQL_CANID.hpp"

namespace Ui {
	class General;
}

class General : public UiWindow
{
    Q_OBJECT

public:
    explicit General(QWidget *parent = nullptr,SQL_CANID *test = nullptr);
    ~General() override;
    void updateDCUGeneralUI();
    void updateTEGeneralUI();
    void updateDashGeneralUI();
    void updateIIBUI();
    void updateSteerUI();
    void updateBMSUI();

    void dcu_toggle(DCU_TOGGLE code);
    void debug_toggle(DEBUG_TOGGLE code);


private slots:
    void process_CAN_message(CANmessage message) override;
    void on_dcu_toggle_dcdc_drs_clicked();
    void on_debug_mode_te_clicked();
    void on_debug_mode_dcu_clicked();
    void on_debug_mode_dash_clicked();
    void set_pedal_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select);
    void on_clearButton_clicked();
    void disable_brake_thresholds();
    void set_steer_threshold(INTERFACE_MSG_STEER_THRESHOLDS select);
    void on_steer_set_left_clicked();
    void on_steer_set_middle_clicked();
    void on_steer_set_right_clicked();
    void on_start_log_button_clicked();
    void on_details_clicked();
    void on_dcu_toggle_pumps_clicked();
    void retransmit_can(CANmessage msg);
    void on_brake_upper_clicked();
    void on_brake_lower_clicked();
    void on_accel_upper_clicked();
    void on_accel_lower_clicked();
    void clear_all();

private:
    Ui::General *ui;
    SQL_CANID *teste;
    GeneralDetails general_details;
    QMap<unsigned int, LEDForm *> forms;
    QMap<unsigned int, bool> active_modules;
    QTimer *clear_timer;
    int clear_flag;

signals:
    void send_to_CAN(CANmessage msg) override;
private slots:
    void update();
};


#endif // GENERAL_H
