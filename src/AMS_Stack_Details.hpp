#ifndef STACKDETAILS_H
#define STACKDETAILS_H

#include <QWidget>
#include <QVector>

#include "AMS_Cell_Line.hpp"

namespace Ui {
class StackDetails;
}

class StackDetails : public QWidget
{
    Q_OBJECT

public:
    explicit StackDetails(QWidget *parent = 0, QString title = "");
    ~StackDetails();
    QVector<CellLine *> cells;
    void clear(void);



private:
    Ui::StackDetails *ui;
};

#endif // STACKDETAILS_H
