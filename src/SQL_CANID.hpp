#ifndef COM_SQL_CANID_HPP
#define COM_SQL_CANID_HPP

#include <QtSql>
#include <QSqlDatabase>
#include <QSqlQuery>

#include "COM.hpp"

class SQL_CANID : public COM
{
    Q_OBJECT

public:
    SQL_CANID();
    virtual ~SQL_CANID() {}
    QSqlDatabase CANId;
    QString giveDbName() {return CANId.databaseName();}
    QStringList giveTables() {return CANId.tables(QSql::Tables);}
    QStringList giveViews() {return CANId.tables(QSql::Views);}
    int giveId(QString);
    QMap<QString,QVector<QVariant>> giveView(QString);
    bool translateMsg(CANdata,QString,int*);
    int bitwiseThis(int64_t Input,int Start,int End);
    void setDatabase(QString);
};

#endif // COM_SQL_CANID_HPP
