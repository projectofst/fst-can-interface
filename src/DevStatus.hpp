#ifndef DEVSTATUS_HPP
#define DEVSTATUS_HPP

#include <QWidget>
#include <QMap>
#include <QString>
#include <QDebug>
#include <QVBoxLayout>
#include <QTextEdit>
#include <QTimer>
#include <QCommandLinkButton>
#include <QSettings>
#include <QQuickWidget>

#include "UiWindow.hpp"
#include "ui_DevStatus.h"
#include "can-ids/CAN_IDs.h"
#include "table.h"

namespace Ui {
class DevStatus;
}

class DevStatus : public UiWindow
{
    Q_OBJECT

public:
    explicit DevStatus(QWidget *parent = nullptr,QObject *picasso = nullptr);
    ~DevStatus() override;

signals:
    void send_to_CAN(CANmessage) override;

public slots:
    void process_CAN_message(CANmessage) override;
    void resize(int);

private:
    Ui::DevStatus *ui;
    QList<int> DevOrder;
    QMap<int,QWidget*> DevButtons;
    QMap<int,QTimer*> DevTimers;
    QMap<int,int> DevStates;
    int Id;

    QLinearGradient red;
    QLinearGradient green;
    QLinearGradient yellow;

    void createDevs();
    void layoutDevs(int);
    int currentK;
    int lastK;

private slots:
    void silentWarning(void);
    void setButtonOrder();
    QList<int> saveButtonOrder();
    void on_SettingsButton_toggled(bool checked);
};

#endif // DEVSTATUS_HPP
