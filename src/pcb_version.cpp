#include "pcb_version.hpp"
#include "ui_pcb_version.h"
#include <QDebug>
#include <QPalette>

pcb_version::pcb_version(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::pcb_version)
{
    ui->setupUi(this);
    name = "UNDEFINED";
    for(int k = 0; k < 4; k++){
        current_version.append(0);
    }
    expected_version = "NONE";
    QPalette palette;
    palette.setColor(ui->name->foregroundRole(), Qt::red);
    ui->name->setPalette(palette);
}

pcb_version::~pcb_version()
{
    delete ui;
}

QString pcb_version::get_name(){
    return name;
}
uint64_t pcb_version::cast_versions(){
    /* The version has 32 bits whih are sent on 4 8 bit messages. Here we put them all together on a 32 bit number */
    return uint32_t((current_version[0] << 24) + (current_version[1] << 16) + (current_version[2] << 8) + current_version[3]);
}
uint64_t pcb_version::get_current_version(){
    uint64_t casted_version = cast_versions();
    return casted_version;
}
QString pcb_version::get_expected_version(){
    return expected_version;
}

void pcb_version::set_expected_version(QString version){
    expected_version = version;
    ui->expected_version->setText(version);
    return;
}
void pcb_version::set_current_version(uint64_t version, int byte){
    QPalette palette;
    if (byte < current_version.size())
        current_version[byte] = version;

    uint64_t current_v = cast_versions();
    ui->current_version->setText(QString::number(current_v, 16));

    (ui->current_version->text() == ui->expected_version->text()) ?
                palette.setColor(ui->name->foregroundRole(), Qt::green) :
                palette.setColor(ui->name->foregroundRole(), Qt::red);
    ui->name->setPalette(palette);
    return;
}

void pcb_version::set_name(QString new_name){
    name = new_name;
    ui->name->setText(new_name);
    return;
}

void pcb_version::destroy(){
    delete this;
}
