/**********************************************************************
 *	 FST CAN tools --- interface
 *
 *	 Console widget
 *	 _______________________________________________________________
 *
 *	 Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *	 This program is free software; you can redistribute it and/or
 *	 modify it under the terms of the GNU General Public License
 *	 as published by the Free Software Foundation; version 2 of the
 *	 License only.
 *
 *	 This program is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	 GNU General Public License for more details.
 *
 *	 You should have received a copy of the GNU General Public License
 *	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

/*#define BYTETOBINARYPATTERN "%d%d%d%d%d%d%d%d"
#define BYTETOBINARY(byte)  \
	(byte & 0x80 ? 1 : 0), \
	(byte & 0x40 ? 1 : 0), \
	(byte & 0x20 ? 1 : 0), \
	(byte & 0x10 ? 1 : 0), \
	(byte & 0x08 ? 1 : 0), \
	(byte & 0x04 ? 1 : 0), \
	(byte & 0x02 ? 1 : 0), \
    (byte & 0x01 ? 1 : 0) */

#include <qmessagebox.h>

#include "Console.hpp"
#include "ui_Console.h"
#include <QThreadPool>
#include <QMutex>
#include <QLineEdit>
#include <QDateTime>
#include <QCheckBox>
#include <QVector>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QFile>
#include <iostream>
#include <QComboBox>

#include "Printer.hpp"
#include "Resources/filterwidget.hpp"
#include "ui_filterwidget.h"
#include "COM_SerialPort.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/INTERFACE_CAN.h"

#define DEFAULT_BAUD_RATE 1000000

/**********************************************************************
 * Name:	Console
 * Args:	-
 * Return:	-
 * Desc:	Console class constructor.
 **********************************************************************/
Console::Console(QWidget *parent) : UiWindow(parent) , ui(new Ui::Console)
{
	ui->setupUi(this);
    ui->Display->setMaximumBlockCount(MAX_LOG_LINES_CONSOLE);

    this->_logging = false;
    this->no_output = 0;

    ui->var_type->addItem("Signed");
    ui->var_type->addItem("Unsigned");
    ui->var_type->addItem("Hex");
    connect(ui->logbutton, SIGNAL(clicked(bool)), this, SLOT(_log_changed(bool)));
    connect(&_log_timer, SIGNAL(timeout()), this, SLOT(_flush_log()));
    connect(&make_id, SIGNAL(id_value(int)), this, SLOT(set_id_value(int)));

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    //timer->start(50);


    LED_timer = new QTimer(this);
    connect(LED_timer, SIGNAL(timeout()), this, SLOT(update_LED()));
    LED_timer->start(100);
    tester_timer = new QTimer(this);
}

/**********************************************************************
 * Name:	~Console
 * Args:	-
 * Return:	-
 * Desc:	Console class destructor.
 **********************************************************************/
Console::~Console(void)
{
    delete ui;
}

void Console::update() {
    if (messages.empty()) {
        return;
    }

    CANmessage msg;
    unsigned timeout = 10*MAX_LOG_LINES_CONSOLE;
    while(!messages.empty() && timeout-- > 0) {
        msg = messages.dequeue();
        write_to_console(msg);
    }

    if (!filter_message(msg) || no_output) {
            return;
    }

    /*while(!messages.empty()) {
        messages.dequeue();
    }*/

    ui->Display->moveCursor(QTextCursor::End);

}

/**********************************************************************
 * Name:	process_print_messages
 * Args:	-
 * Return:	-
 * Desc:	Prints all the messages in the console screen from Comsguy
 *          signal.
 **********************************************************************/
void Console::process_CAN_message(CANmessage msg)
{
    ui->CAN_LED->turnGreen();
    LED_timer->start(100);
        // write to console
        write_to_console(msg);
        //messages.enqueue(msg);

        // Log to file
        log_to_file(msg);
}

void Console::process_print_CAN_raw_message(QByteArray /*_new_data*/)
{
//    QString DataAsString = QTextCodec::codecForMib(106)->toUnicode(_new_data);

//    printer.append_string(DataAsString);
//    ui->consoleoutput->appendPlainText(DataAsString);
}

void Console::send_start_log(){
	int year = QDate::currentDate().year();
	int month = QDate::currentDate().month();
	int day = QDate::currentDate().day();

	int hour = QTime::currentTime().hour();
	int minute = QTime::currentTime().minute();
	int second = QTime::currentTime().second();

    CANmessage msg;

    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_INTERFACE_START_LOG;

    msg.candata.dlc = 6;

    msg.candata.data[0] = static_cast<uint16_t>(year | day << 11);
    msg.candata.data[1] = static_cast<uint16_t>(month | hour << 4);
    msg.candata.data[2] = static_cast<uint16_t>(minute | second << 6);

	INTERFACE_CAN_Data interface_data;

    parse_can_interface(msg.candata, &interface_data);

	year = interface_data.start_log.year;
	month = interface_data.start_log.month;
	day = interface_data.start_log.day;
	hour = interface_data.start_log.hour;
	minute = interface_data.start_log.minute;
	second = interface_data.start_log.second;

    qDebug("I'm sending");
	emit send_to_CAN(msg);
}

void Console::log_to_file(CANmessage msg) {
    char log_string[512];

    if (_logging) {
        sprintf(log_string, "%u,%u,%u,%u,%u,%u,%u", msg.candata.sid, msg.candata.dlc, msg.candata.data[0],
        msg.candata.data[1], msg.candata.data[2], msg.candata.data[3], msg.timestamp);
        _logstream << log_string << "\n"; // Do not flush here!!! Computer would be bogged down flushing every message
    }
}

bool Console::filter_message(CANmessage msg) {
    FilterWidget *filter_aux;
    bool device_match = false, message_match = false;

    if (this->filter_vector.size() == 0) {
        device_match = true;
        message_match = true;
    }

    for (int i=0; i < this->filter_vector.size(); i++) {
        filter_aux = this->filter_vector.at(i);

        if(filter_aux->active && (filter_aux->ui->dev_id->displayText().size() == 0 ||  msg.candata.dev_id == filter_aux->ui->dev_id->displayText().toInt())) {
            device_match = true;
        }

        if(filter_aux->active && (filter_aux->ui->msg_id->displayText().size() == 0 ||  msg.candata.msg_id == filter_aux->ui->msg_id->displayText().toInt())) {
            message_match = true;
        }
    }

    return device_match && message_match;
}

void Console::write_to_console(CANmessage msg) {
    char string[512];
    if (!filter_message(msg) || no_output) {
        return;
    }

    if (var_type == "Unsigned") {
        sprintf(string,"> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %05d  %05d  %05d  %05d\tTIME: %d",
            msg.candata.dev_id, msg.candata.msg_id, msg.candata.dlc, msg.candata.data[0],
            msg.candata.data[1], msg.candata.data[2], msg.candata.data[3], msg.timestamp);
    }
    else if (var_type == "Signed") {
        sprintf(string,"> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %06d  %06d  %06d  %06d\tTIME: %d",
            msg.candata.dev_id, msg.candata.msg_id, msg.candata.dlc, (int16_t) msg.candata.data[0],
            (int16_t) msg.candata.data[1], (int16_t) msg.candata.data[2], (int16_t) msg.candata.data[3], (int16_t) msg.timestamp);
    }
    else if (var_type == "Hex") {
        sprintf(string,"> DEV_ID: %6d\tMSG_ID: %6d\tDLC: %d\tData: %04x  %04x  %04x  %04x\tTIME: %d",
                   msg.candata.dev_id, msg.candata.msg_id, msg.candata.dlc, msg.candata.data[0],
                   msg.candata.data[1], msg.candata.data[2], msg.candata.data[3], msg.timestamp);
    }
    ui->Display->appendPlainText(string);
}

/**********************************************************************
 * Name:	on_consoleclear_clicked
 * Args:	-
 * Return:	-
 * Desc:	Clear the console output.
 **********************************************************************/
void Console::on_consoleclear_clicked(void)
{
    ui->Display->clear();
}


/**********************************************************************
 * Name:	on_sendbutton_clicked
 * Args:	-
 * Return:	-
 * Desc:	Sends message to CAN.
 **********************************************************************/
void Console::on_sendbutton_clicked(void)
{
    CANmessage msg;

    msg.candata.sid = static_cast<uint16_t>(ui->ID->value());
    msg.candata.dlc = static_cast<uint16_t>(ui->DLC->value());

    msg.candata.data[0] = static_cast<uint16_t>(ui->word1_2->value());
    msg.candata.data[1] = static_cast<uint16_t>(ui->word2_2->value());
    msg.candata.data[2] = static_cast<uint16_t>(ui->word3_2->value());
    msg.candata.data[3] = static_cast<uint16_t>(ui->word4_2->value());

    qint64 log_diff_ms = time.msecsTo(time.currentDateTime());
    msg.timestamp = static_cast<uint32_t>(log_diff_ms);

    #ifdef DBG_SERIAL_SEND
        fprintf(stderr, "[%s : %d] DBG_SERIAL_SEND: Message constructed: %u %u %u %u %u %u.\n",
            __FILE__, __LINE__, msg.sid, msg.dlc, msg.data[0], msg.data[1], msg.data[2], msg.data[3]);
    #endif

    emit send_to_CAN(msg);

    return;
}

/**********************************************************************
 * Name:	_log_changed
 * Args:	bool log_status
 * Return:	-
 * Desc:	Opens/closes file for logging, sets/unsets a logging flag
 *			and sets / unsets the flush timer.
 **********************************************************************/
void Console::_log_changed(bool log_status)
{
	if(log_status){
		time = time.currentDateTime();

        /* If the filename is empty it is set to the current time */
        if (ui->logfile->text().isEmpty()) {
            _logfile.setFileName(QDateTime::currentDateTime().toString());
        }
        else {
            _logfile.setFileName(ui->logfile->text());
        }

		if(_logfile.open(QIODevice::Append | QIODevice::Text)){

			_logstream.setDevice(&_logfile);

			// set session delimiter (if file exists, it gets appended)
			_logstream << time.toString(QString("#########################################\n"));
			_logstream << time.toString(QString("# dddd, dd MMMM yyyy\n"));
			_logstream << time.toString(QString("# hh:mm:ss\n"));
			_logstream << time.toString(QString("#########################################\n"));

            _log_timer.start( LOG_FLUSH_timestamp );	// restart timer

			// Only changes IF file is successfully opened!
			_logging = log_status;

		}else{
			// Quick way of resetting the button
			// Is there a better way??
			// Should be race condition free regarding user input
			ui->logbutton->setCheckable(false);
			ui->logbutton->setCheckable(true);
		}


	}else{
		_logging = log_status;

		// Not testing returns, but there's probably very little to do with the result anyway
		_log_timer.stop();
		_logstream.flush();
		_logfile.close();
	}


	return;
}


/**********************************************************************
 * Name:	_flush_log
 * Args:	-
 * Return:	-
 * Desc:	Flushes the log stream to the file and resets the flush
 *			timer.
 **********************************************************************/
void Console::_flush_log(void){

	_logstream.flush();

    _log_timer.start( LOG_FLUSH_timestamp );	// restart timer

	return;
}

void Console::on_pushButton_debugger_clicked()
{
//    printer.show();
//    printer.activateWindow();
//    printer.raise();
}

void Console::on_pushButton_5_clicked()
{
    this->no_output ^= 1;
    if (this->no_output) {
        ui->pushButton_5->setText("Continue");
    }
    else {
        ui->pushButton_5->setText("Stop");
    }
}

void Console::on_show_ids_clicked()
{
    id_list.show();
    id_list.activateWindow();
    id_list.raise();
    id_list.update();
    id_list.toTop();
}

void Console::set_id_value(int id)
{
    ui->ID->setValue(id);
}

void Console::on_AddMessageFilterButton_clicked()
{
    static unsigned ids = 0;
    FilterWidget *filter = new FilterWidget;
    ui->filterList_4->addWidget(filter);
    filter_vector.append(filter);
    connect(filter->ui->enabled, &QCheckBox::stateChanged, this, [this, filter](const int state_changed){filter->active = state_changed ? 1:0;});
    connect(filter->ui->delete_2, &QPushButton::clicked, this, [this, filter](){this->filter_vector.remove(this->filter_vector.indexOf(filter));delete filter;});
    ids++;
}
void Console::on_make_id_clicked()
{
    make_id.show();
    make_id.activateWindow();
    make_id.raise();
}

void Console::on_var_type_currentIndexChanged(const QString &arg1)
{
    var_type = arg1;
}

void Console::update_LED(){
    ui->CAN_LED->turnRed();
    return;
}
