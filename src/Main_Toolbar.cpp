#include "Main_Toolbar.hpp"

MainToolbar::MainToolbar(MainWindow *parent)
    : QMainWindow(parent)
{
    toolbar = new QToolBar;
    mainwindow = parent;

    // COM
    COM = new QAction(this);
    COM->setObjectName("actionToggleCOM");
    COM->setIcon(QIcon(":/CustomIcons/CustomIcons/link.png"));
    COM->setToolTip("Toggle Comunication Selector");
    COM->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_C));
    COM->setCheckable(true);
    COM->setChecked(true);
    toolbar->addAction(COM);
    connect(COM,&QAction::triggered,
            this,&MainToolbar::toggleCOM);

    // Separator
    toolbar->addSeparator();

    // Console
    temp = new QAction(this);
    temp->setObjectName("ConDock");
    temp->setIcon(QIcon(":/CustomIcons/CustomIcons/terminal.png"));
    temp->setToolTip("Create Console Widget");
    temp->setText("ConDock");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // General
    temp = new QAction(this);
    temp->setObjectName("GenDock");
    temp->setIcon(QIcon(":/CustomIcons/CustomIcons/car.png"));
    temp->setToolTip("Create General Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_G));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // AMS
    temp = new QAction(this);
    temp->setObjectName("AMSDock");
    temp->setIcon(QIcon(":/CustomIcons/CustomIcons/battery.png"));
    temp->setText("AMS");
    temp->setToolTip("Create AMS Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_A));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Motors
    temp = new QAction(this);
    temp->setObjectName("InvDock");
    temp->setIcon(QIcon(":/CustomIcons/CustomIcons/engine.png"));
    temp->setText("Motors");
    temp->setToolTip("Create Motors Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_M));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Sensors
    QIcon *tempicon = new QIcon(":/CustomIcons/CustomIcons/suspension.png");
    temp = new QAction(this);
    temp->setObjectName("SenDock");
    temp->setIcon(*tempicon);
    temp->setText("Sensors");
    temp->setToolTip("Create Sensor Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Plot
    tempicon = new QIcon(":/CustomIcons/CustomIcons/line-graphic.png");
    temp = new QAction(this);
    temp->setObjectName("PloDock");
    temp->setIcon(*tempicon);
    temp->setText("Plot");
    temp->setToolTip("Create Plot Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_P));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Isabelle
    tempicon = new QIcon(":/CustomIcons/CustomIcons/voltmeter.png");
    temp = new QAction(this);
    temp->setObjectName("IsaDock");
    temp->setIcon(*tempicon);
    temp->setText("Isabelle");
    temp->setToolTip("Create Isabelle Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_I));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    tempicon = new QIcon(":/CustomIcons/CustomIcons/cooling.png");
    temp = new QAction(this);
    temp->setObjectName("ccu");
    temp->setIcon(*tempicon);
    temp->setText("CCU");
    temp->setToolTip("Create CCU Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // TestBench
    tempicon = new QIcon(":/CustomIcons/CustomIcons/laboratory-bench.png");
    temp = new QAction(this);
    temp->setObjectName("MTBDock");
    temp->setIcon(*tempicon);
    temp->setText("Test Bench");
    temp->setToolTip("Create Motor Test Bench");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_M + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // DevStatus
    tempicon = new QIcon(":/CustomIcons/CustomIcons/checklist.png");
    temp = new QAction(this);
    temp->setObjectName("DevDock");
    temp->setIcon(*tempicon);
    temp->setText("Devices");
    temp->setToolTip("Create Device Status Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));


    // Separator
    toolbar->addSeparator();

    // Save Layout
    tempicon = new QIcon(":/CustomIcons/CustomIcons/layout.png");
    temp = new QAction(this);
    temp->setObjectName("saveLayout");
    temp->setIcon(*tempicon);
    temp->setText("Save Layout");
    temp->setToolTip("Save Layout");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Delete Layout
    tempicon = new QIcon(":/CustomIcons/CustomIcons/garbage.png");
    temp = new QAction(this);
    temp->setObjectName("deleteLayout");
    temp->setIcon(*tempicon);
    temp->setText("Delete Layout");
    temp->setToolTip("Delete Layout");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));



    // LayoutCombobox
    LayoutCombo = new QComboBox;
    toolbar->addWidget(LayoutCombo);
    LayoutCombo->setMinimumWidth(170);

    toolbar->setObjectName("MainToolBar");
    parent->addToolBar(toolbar);
    if (QSysInfo::productType() == "osx")
        parent->setUnifiedTitleAndToolBarOnMac(true);

    // Separator
    toolbar->addSeparator();

    // Memory View
    tempicon = new QIcon(":/CustomIcons/CustomIcons/database.png");
    temp = new QAction(this);
    temp->setObjectName("MVDock");
    temp->setIcon(*tempicon);
    temp->setText("Memory View");
    temp->setToolTip("Memory View");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_M + Qt::Key_V));

    //Versions
    tempicon = new QIcon(":/CustomIcons/CustomIcons/gitlab_blue.png");
    temp = new QAction(this);
    temp->setObjectName("Versions");
    temp->setIcon(*tempicon);
    temp->setText("Versions");
    temp->setToolTip("Versions");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D + Qt::Key_B));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    //DRS_cal
    tempicon = new QIcon(":/CustomIcons/CustomIcons/aero_blue.png");
    temp = new QAction(this);
    temp->setObjectName("DRS_cal");
    temp->setIcon(*tempicon);
    temp->setText("DRS");
    temp->setToolTip("DRS");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D + Qt::Key_B));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    // Log Console
    tempicon = new QIcon(":/CustomIcons/CustomIcons/logging_transparent.png");
    temp = new QAction(this);
    temp->setObjectName("logconsole");
    temp->setIcon(*tempicon);
    temp->setText("Log");
    temp->setToolTip("Log");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_D));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    tempicon = new QIcon(":/CustomIcons/CustomIcons/arm.png");
    temp = new QAction(this);
    temp->setObjectName("ArmDock");
    temp->setIcon(*tempicon);
    temp->setText("ARM");
    temp->setToolTip("Create ARM Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));

    tempicon = new QIcon(":/CustomIcons/CustomIcons/garbage.png");
    temp = new QAction(this);
    temp->setObjectName("LapDock");
    temp->setIcon(*tempicon);
    temp->setText("Lap");
    temp->setToolTip("Create Lap Widget");
    temp->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S + Qt::Key_T));
    toolbar->addAction(temp);
    connect(temp,SIGNAL(triggered()),this,SLOT(widgetAction()));


}

void MainToolbar::widgetAction()
{
    QString type = QObject::sender()->objectName();
    if (type == "saveLayout") {
        emit saveLayout();
    } else if (type == "deleteLayout") {
        emit deleteLayout();
    } else {
        emit openUiWindow(type);
    }

}

void MainToolbar::toggleCOM()
{
    if (mainwindow->centralWidget()->isVisible()) {
        mainwindow->centralWidget()->hide();
        COM->setChecked(false);
    } else {
        mainwindow->centralWidget()->show();
        COM->setChecked(true);
    }

}
