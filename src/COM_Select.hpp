#ifndef COMSELECT_H
#define COMSELECT_H

#include <QFileDialog>
#include <QWidget>
#include <QListWidgetItem>
#include <QString>

#include "ui_COM_Select.h"

#include "UiWindow.hpp"
#include "COM_Manager.hpp"
#include "Sitrep.hpp"

class COMSelect : public UiWindow
{
    Q_OBJECT

public:
    explicit COMSelect(QWidget *parent = nullptr, Comsguy *parentcom = nullptr);
    ~COMSelect();
    Ui::COMSelect *ui;

public slots:

private:
    Comsguy *ComsManager;
        //int TableCurrentRowCount;
        //QTableWidgetItem *TempItem;

private slots:
    void add_line(void);
    //void generateTableLines(void);

};

#endif // COMSELECT_H
