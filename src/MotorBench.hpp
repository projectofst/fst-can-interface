#ifndef TESTBENCH_H
#define TESTBENCH_H

#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "UiWindow.hpp"

namespace Ui {
class TestBench;
}

class TestBench : public UiWindow
{
    Q_OBJECT

public:
    explicit TestBench(QWidget *parent = nullptr);
    ~TestBench() override;

private slots:
    void process_CAN_message(CANmessage) override;

    void on_load_file_clicked();

    void on_send_file_clicked();

    void on_send_values_clicked();

    void on_speed_1_copyAvailable(bool);

    void on_motor_1_clicked();

    void on_gen_1_clicked();

private:
    Ui::TestBench *ui;

    int** parameters;
    QString file_name_1;

signals:
    void send_to_CAN(CANmessage msg) override;
};

#endif // TESTBENCH_H
