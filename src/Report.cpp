#include <QString>
#include <QCloseEvent>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QDebug>

#include "Report.hpp"
#include "ui_Report.h"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"
#include "can-ids/table.h"
#include "report_indiv_filter.hpp"

enum trap_types_enum {
    HARD_TRAP_OSCILATOR_FAIL,
    HARD_TRAP_ADDRESS_ERROR,
    HARD_TRAP_STACK_ERROR,
    HARD_TRAP_MATH_ERROR,
    CUSTOM_TRAP_PARSE_ERROR,
};

const char* const trap_types[] = { "OSCILLATOR FAIL", "ADDRESS ERROR", "STACK ERROR", "MATH ERROR", "PARSE ERROR"};

const char* const reason_list[] = {
    "DASH_BUTTON",
    "OVERVOLTAGE",
    "UNDERVOLTAGE",
    "OVERCURRENT_IN",
    "OVERCURRENT_OUT",
    "OVERTEMPERATURE",
    "UNDERTEMPERATURE",
    "MIN_CELL_LTC",
    "IMD",
    "FAKE_ERROR",
    "OTHER",
    "NO_ERROR",
};

Log::Log(QWidget *parent) : UiWindow(parent), ui(new Ui::Log){
    ui->setupUi(this);
    send_message = 1;
    WriteString("Welcome to FST CAN Interface log", BLACK);
    filters = new Report_filters;
    init_filters();
}

Log::~Log()
{
    delete ui;
}

void Log::UpdateTimer(){
    //timestamp
    QDateTime time=time.currentDateTime();

    if(IMD_status==0 && IMD_statusreset == 0){
        ui->LogConsole->setTextColor(Qt::darkGreen);
        ui->LogConsole->append(QString("%1 -- DCU -- IMD NOK and MEM DETECTED").arg(time.toString(QString("yyyy-MM-dd hh:mm:ss.zzz"))));

        IMD_statusreset = 1;
    }else{
        IMD_status = 0;
    }
}



/**********************************************************************
 * Name:    initflags
 * Args:    -
 * Return:  -
 * Desc:    Flag init default state.
 **********************************************************************/
/*void Log::initflags(void){
    _status_flags_mask_master[0]	= 0;
    _status_flags_mask_master[1]	= 0;
    _op_mode_master[0]				= 0;
    _op_mode_master[1]				= 0;
    _ts_on 							= 0;
    _ams_error						= 0;
    _air_feed						= 0;
}*/

void Log::WriteString(QString string, unsigned int colour){

    //timestamp
    QDateTime time=time.currentDateTime();

    switch(colour){
    case RED:   ui->LogConsole->setTextColor(Qt::red);
        break;

    case GREEN: ui->LogConsole->setTextColor(Qt::darkGreen);
        break;
    case BLUE: ui->LogConsole->setTextColor(Qt::blue);
        break;
    default:    ui->LogConsole->setTextColor(Qt::black);
        break;
    }
    if (send_message)
        ui->LogConsole->append(QString("%1 -- %2").arg(time.toString(QString("yyyy-MM-dd hh:mm:ss.zzz"))).arg(string));
    send_message = 1;
    return;
}

/**********************************************************************
 * Name:    process_CAN_message
 * Args:    CANmessage msg
 * Return:  -
 * Desc:    Process incoming CAN message.
 **********************************************************************/
void Log::process_CAN_message(CANmessage message){
    CANdata msg = message.candata;
    new_log_event(msg);

    return;
}


void Log::new_log_event(CANdata msg){
    char buffer[512];
    unsigned color = 0;



    /*COMMON Messages*/
    if (msg.msg_id == MSG_ID_COMMON_RESET) {
        COMMON_MSG_RESET reset;

        parse_can_common_reset(msg, &reset);

        QString reset_string = " reset, RCON = ";
        sprintf(buffer, nomes_placas[msg.dev_id] + reset_string.toLatin1() + QString::number(reset.RCON, 2).toLatin1());
        color = RED;
        send_message = get_check_status(msg.dev_id, msg.msg_id);
    }
    else if (msg.msg_id == MSG_ID_COMMON_TRAP) {
        COMMON_MSG_TRAP trap;

        parse_can_common_trap(msg, &trap);
        sprintf(buffer, "%s trap %s", nomes_placas[msg.dev_id], trap_types[trap.type]);
        color = BLACK;
    }
    else if (msg.msg_id == MSG_ID_COMMON_PARSE_ERROR) {
        COMMON_MSG_PARSE_ERROR parse;

        parse_can_common_parse_error(msg, &parse);
        sprintf(buffer, "%s tried to parse a message from %s but it was from %s",
                nomes_placas[msg.dev_id], nomes_placas[parse.wrong_dev_id],
                nomes_placas[parse.right_dev_id]);
        color = BLACK;
    }
    else if (msg.msg_id == CMD_ID_COMMON_SET) {
        if((msg.dev_id == msg.data[0]) && (msg.dev_id < 32) && (msg.data[0] < 32)){
            sprintf(buffer, "SET_ACKNOWLEDGED: The parameter %d was changed on %s to %d.", msg.data[1], nomes_placas[msg.dev_id], msg.data[2]);
            color = GREEN;
        }
        else if((msg.dev_id < 32) && (msg.data[0] < 32)) {
            sprintf(buffer, "SET_REQUEST: The %s tried to set the parameter %d on the %s.", nomes_placas[msg.dev_id], msg.data[1], nomes_placas[msg.data[0]]);
            color = RED;
        }
        else {
            sprintf(buffer, "I WAS GONNA SEG FAULT THERE");
            color = BLUE;
        }
        send_message = get_check_status(msg.dev_id, msg.msg_id);
    }
    else if (msg.msg_id == MSG_ID_COMMON_ERROR) {
        sprintf(buffer, "ERROR: %s: %s.", nomes_placas[msg.dev_id], erros[msg.data[0]]);
        color = RED;
        send_message = get_check_status(msg.dev_id, msg.msg_id);
    }
    else if (msg.msg_id == CMD_ID_COMMON_GET){
        if((msg.dev_id == msg.data[0]) && (msg.dev_id < 32) && (msg.data[0] < 32)){
            sprintf(buffer, "GET_ACKNOWLEDGED: %d from the %s is %d.", msg.data[1], nomes_placas[msg.dev_id], msg.data[2]);
            color = GREEN;
        }
        else if((msg.dev_id < 32) && (msg.data[0] < 32)) {
            sprintf(buffer, "GET_REQUEST: %s asked for the %d from %s. Waiting for response...", nomes_placas[msg.dev_id], msg.data[1], nomes_placas[msg.data[0]]);
            color = RED;
        }
        else {
            sprintf(buffer, "I WAS GONNA SEG FAULT THERE");
            color = BLUE;
        }
        send_message = get_check_status(msg.dev_id, msg.msg_id);
    }
    else if (msg.dev_id == DEVICE_ID_IIB){

        if (msg.msg_id == CMD_ID_COMMON_RTD_ON){
            COMMON_MSG_RTD_ON rtd_on;

            parse_can_common_RTD(msg, &rtd_on);

            if (rtd_on.step == RTD_STEP_INV_GO){
                sprintf(buffer, "Ready to Drive Mode is ON!");
            }

            else return;
        }
        else if (msg.msg_id == CMD_ID_COMMON_RTD_OFF){
            COMMON_MSG_RTD_OFF rtd_off;

            parse_can_common_RTD_OFF(msg, &rtd_off);

            if (rtd_off.step == RTD_INV_CONFIRM || rtd_off.step == RTD_INV_OFF)
                sprintf(buffer, "Ready to Drive Mode is OFF.");

        }
        else
            return;
        color = RED;
    }

    else if (msg.dev_id == DEVICE_ID_DASH){
        if (msg.msg_id == CMD_ID_COMMON_RTD_ON){
            COMMON_MSG_RTD_ON rtd_on;

            parse_can_common_RTD(msg, &rtd_on);

            if (rtd_on.step == RTD_STEP_DASH_BUTTON){
                sprintf(buffer, "RTD Button Pressed. (ON)");
            }

            else return;
        }
        else if (msg.msg_id == CMD_ID_COMMON_RTD_OFF){
            COMMON_MSG_RTD_OFF rtd_off;

            parse_can_common_RTD_OFF(msg, &rtd_off);

            if (rtd_off.step == RTD_DASH_OFF )
                sprintf(buffer, "RTD Button Pressed. (OFF)");

        }

        else if (msg.msg_id == CMD_ID_COMMON_TS){
            COMMON_MSG_TS ts;

            parse_can_common_TS(msg,&ts);

            if(ts.step==TS_DASH_BUTTON)
                sprintf(buffer, "TS Button Pressed.");

            else return;


        }
        else
            return;
        color = RED;
    }



    /*Master Messages*/
    else if (msg.dev_id == DEVICE_ID_BMS_MASTER){

        if (msg.msg_id == CMD_ID_COMMON_TS) {
            sprintf(buffer, "TS turned ON!");
        }
        else if (msg.msg_id == MSG_ID_MASTER_TS_STATE){
            MASTER_MSG_TS ts;

            parse_can_message_master_ts(msg, &ts);
            sprintf(buffer, "TS turned OFF, reason: %s.", reason_list[ts.reason]);
        }

        else
            return;
        color = RED;


    }
    else {
        return;
    }

    WriteString(buffer, color);
}

/**********************************************************************
 * Name:    on_clearlog_clicked
 * Args:    -
 * Return:  -
 * Desc:    Clear the console output.
 **********************************************************************/
void Log::on_clearlog_clicked(void){
    ui->LogConsole->clear();
}

void Log::on_savebutton_clicked()
{
    QFile outfile;
    outfile.setFileName("log.txt");
    outfile.open(QIODevice::Append | QIODevice::Text);
    QTextStream out(&outfile);
    out << ui->LogConsole->toPlainText() << endl;

    return;
}

void Log::on_StatusLog_clicked()
{
    /*_CarStatus.show();
    _CarStatus.raise();
    _CarStatus.activateWindow();*/


    return;
}

void Log::init_filters(){

    for(int k = 0; k < 32; k++){
        if((nomes_placas[k][0] < '0') || (nomes_placas[k][0] > '9')){
            filters->add_device(nomes_placas[k]);
        }
    }
    return;
}

void Log::on_filters_button_clicked()
{
    filters->show();
}

int Log::get_check_status(int dev_id, int msg_id){
    Report_indiv_filter* filter = nullptr;
    int not_found = 1;

    if(!filters->size()) return not_found; /*If there are no filters applied,send message by default*/

    for (int k = 0; k < filters->size(); k++){ /*This is not very well done*/
        if(dev_id < 32){
            if(filters->at(k)->get_device_name() == nomes_placas[dev_id]){
                filter = filters->at(k);
                not_found = 0;
            }
        }
    }

    if (not_found) /*If there are filters applied, and it isnt found, we dont send the message*/
        return 0;

    if (filter != nullptr){ /*Check the status of the boxes*/
        if(msg_id == CMD_ID_COMMON_GET){
            return filter->get_get_status();
        }
        else if (msg_id == CMD_ID_COMMON_SET){
            return filter->get_set_status();
        }
        else if (msg_id == MSG_ID_COMMON_ERROR){
            return filter->get_error_status();
        }
        else if (msg_id == MSG_ID_COMMON_RESET) {
            return filter->get_reset_status();
        }
    }
    return 1;
}

//557cd5880
