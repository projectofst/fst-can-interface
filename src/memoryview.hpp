#ifndef MEMORYVIEW_HPP
#define MEMORYVIEW_HPP

#include <QWidget>

#include "UiWindow.hpp"

#include "can-ids/CAN_IDs.h"

namespace Ui {
class MemoryView;
}

class MemoryView : public UiWindow
{
    Q_OBJECT

public:
    explicit MemoryView(QWidget *parent = nullptr);
    ~MemoryView();

private slots:
    void on_update_clicked();
    void update(void);

public slots:
    void process_CAN_message(CANmessage message);
private:
    QTimer *timer;
    Ui::MemoryView *ui;
    unsigned *count;
    unsigned *_msg_id;

signals:
    void send_to_CAN(CANmessage) override;
};

#endif // MEMORYVIEW_HPP
