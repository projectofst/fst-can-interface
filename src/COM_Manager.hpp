#ifndef COMSGUY_HPP
#define COMSGUY_HPP

#include <QObject>
#include <QDialog>
#include <QTextStream>
#include <QFile>
#include <QFileDialog>
#include <QString>
#include <QMap>
#include "Main_Window.hpp"

#include "COM_LogGen.hpp"
#include "COM.hpp"
#include "COM_SerialPort.hpp"
#include "COM_LogPlay.hpp"
#include "COM_LogRec.hpp"
#include "COM_LAN.hpp"
#include "COM_Interface.hpp"
#include "COM_Kvaser.hpp"
#include "Console.hpp"
#include "Printer.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/INTERFACE_CAN.h"

#include <qmessagebox.h>

typedef enum _PortType {
    Close = 0,
    Serial = 1,
    WifiUdp = 2,
    LogRecorder = 3,
    LogGenera = 4,
    LogPlayer = 5,
    Kvaser = 6,
} PortType;

class Comsguy : public QObject
{
    Q_OBJECT

public:
    explicit Comsguy(QObject *parent = nullptr);
    ~Comsguy(void);
    QMap<QString,COM*> ComList;
    QMap<QString,COM*> ComLogList;

public slots:
    void connectCOM(QString,QString,QString,QString,bool);
    void disconnectCOM(QString,QString);
    void connectMainWin(MainWindow *);

private:
    int MsgCounter;
    COM *SourceCOM;
    COM *DestinCOM;
    LogRec *LoggerCOM;
    LogPlay *LogPlayCOM;
    LogGenerator *LogGenCOM;
    SerialPort *SerialCOM;

};

#endif // COMSGUY_HPP
