

#ifndef CALLOUT_H
#define CALLOUT_H

#include <QtCharts/QChartGlobal>
#include <QtWidgets/QGraphicsItem>
#include <QtWidgets/QGraphicsObject>
#include <QtGui/QFont>
#include "Plotting.hpp"

class QGraphicsSceneMouseEvent;

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

QT_CHARTS_USE_NAMESPACE

class Callout : public QGraphicsObject
{
public:
    Callout(QChart *parent);
    void destroy();
    void setText(const QString &text);
    void setAnchor(QPointF point);
    void updateGeometry();
    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,QWidget *widget);
    QRectF m_rect;
    QPointF m_anchor;
    void set_plot_id(int plot_id);
    int  get_plot_id();
    void set_position(int position);
    int  get_pos();

protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

private:
    QString m_text;
    QRectF m_textRect;
    QFont m_font;
    QChart *m_chart;
    int plot;
    int pos;
};

#endif // CALLOUT_H
