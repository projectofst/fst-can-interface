 #include "COM_Manager.hpp"

Comsguy::Comsguy(QObject *parent) : QObject(parent)
{
    ComList["Serial"] = new SerialPort;
    ComList.value("Serial")->setObjectName("Serial");
    ComLogList["Serial"] = new LogRec;
    ComLogList.value("Serial")->setObjectName("Serial");

    ComList["Player"] = new LogPlay;
    ComList.value("Player")->setObjectName("Play");
    ComLogList["Player"] = new LogRec;
    ComLogList.value("Player")->setObjectName("Player");

    ComList["Generator"] = new LogGenerator;
    ComList.value("Generator")->setObjectName("Gen");
    ComLogList["Generator"] = new LogRec;
    ComLogList.value("Generator")->setObjectName("Generator");

    ComList["Network"] = new LAN;
    ComList.value("Network")->setObjectName("LAN");
    ComLogList["Network"] = new LogRec;
    ComLogList.value("Network")->setObjectName("Nsetwork");

    ComList["Network2"] = new LAN;
    ComList.value("Network2")->setObjectName("LAN");
    ComLogList["Network2"] = new LogRec;
    ComLogList.value("Network2")->setObjectName("Network2");

    ComList["Kvaser"] = new COM_Kvaser;
    ComList.value("Kvaser")->setObjectName("Kvaser");
    ComLogList["Kvaser"] = new LogRec;
    ComLogList.value("Kvaser")->setObjectName("Kvaser");

}

Comsguy::~Comsguy() {
    return;
}

void Comsguy::connectCOM(QString targetone, QString argone,
                         QString targettwo, QString argtwo,
                         bool log)
{
    COM *One = ComList.value(targetone);
    COM *Two = ComList.value(targettwo);
    COM *OneLog = ComLogList.value(targetone);
    COM *TwoLog = ComLogList.value(targettwo);

    if (One->status() <= 0) {
        One->open(argone);
    }
    if (Two->status() <= 0) {
        Two->open(argtwo);
    }

    connect(One,SIGNAL(new_messages(CANmessage)),
            Two,SLOT(send(CANmessage)));
    connect(Two,SIGNAL(new_messages(CANmessage)),
            One,SLOT(send(CANmessage)));

    if (OneLog->status() <= 0 && log) {
        OneLog->open("");
        connect(One,SIGNAL(new_messages(CANmessage)),
                OneLog,SLOT(send(CANmessage)));
    }
}

void Comsguy::disconnectCOM(QString targetone,QString targettwo)
{
    COM *One = ComList.value(targetone);
    COM *Two = ComList.value(targettwo);
    COM *OneLog = ComLogList.value(targetone);

    disconnect(One,SIGNAL(new_messages(CANmessage)),
               Two,SLOT(send(CANmessage)));
    disconnect(Two,SIGNAL(new_messages(CANmessage)),
               One,SLOT(send(CANmessage)));

    if (OneLog->status() == 1) {
        OneLog->close();
        disconnect(One,SIGNAL(new_messages(CANmessage)),
                   OneLog,SLOT(send(CANmessage)));
    }

    One->close();
    Two->close();
}

void Comsguy::connectMainWin(MainWindow *MainWindow)
{
    int index = 0;
    foreach (QString interFace,ComList.keys()) {
        if (interFace.section("@",1,1).toInt() > index)
            index = interFace.section("@",1,1).toInt();
    }

    Interface *temp = new Interface;
    ComList[QString("Interface@").append(QString::number(index))] = temp;
    ComLogList[QString("Interface@").append(QString::number(index))] = new LogRec;
    MainWindow->interfaceCom = temp;
}
