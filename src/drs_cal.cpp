#include "drs_cal.hpp"
#include "ui_drs_cal.h"
#include <QtDebug>


drs_cal::drs_cal(QWidget *parent) :
    ui(new Ui::drs_cal)
{
    ui->setupUi(this);
    reset_values();
    min = 0;
    max = 0;
}

drs_cal::~drs_cal()
{
    delete ui;
}

void drs_cal::send_DRS_can_msg(int parameter, int value_to_send){ /*op = 1 -> SET, op = 0 -> Calibration*/
    if(ui->DRS_number->currentText() != ""){
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = CMD_ID_COMMON_SET;
        msg.candata.dlc = 8;
        msg.candata.data[0] = DEVICE_ID_DCU;
        msg.candata.data[1] = uint16_t (parameter);
        msg.candata.data[2] = uint16_t (value_to_send);
        ui->DRS_number->currentText() == "DRS 1" ?  msg.candata.data[3] = DRS_1 : msg.candata.data[3] = DRS_2;
        send_to_CAN(msg);
    }
    return;
}

void drs_cal::reset_values(){
    ui->value->setText("0");
}

void drs_cal::on_send_clicked()
{
    int parameter;

    parameter = 0;

    if(ui->option->currentText() == "Calibration"){
        if(ui->limit->currentText() == "Min")
            send_DRS_can_msg(P_DRS_MIN_ANGLE_CALI, ui->value->text().toInt());
        else if (ui->limit->currentText() == "Max")
            send_DRS_can_msg(P_DRS_MAX_ANGLE_CALI, ui->value->text().toInt());
    }
    else if(ui->option->currentText() == "Set"){
        if(ui->limit->currentText() == "Min")
            send_DRS_can_msg(P_DRS_MIN_ANGLE_FINAL, ui->value->text().toInt());
        else if (ui->limit->currentText() == "Max")
            send_DRS_can_msg(P_DRS_MAX_ANGLE_FINAL, ui->value->text().toInt());
    }
    return;
}

void drs_cal::on_inc_clicked()
{
    int current_value;

    current_value = ui->value->text().toInt();
    current_value++;
    ui->value->setText(QString::number(current_value));
    if(ui->option->currentText() == "Calibration"){
        if(ui->limit->currentText() == "Min"){
            send_DRS_can_msg(P_DRS_MIN_ANGLE_CALI, current_value);
            ui->Curr_min->setText(QString::number(current_value));
        }
        else if (ui->limit->currentText() == "Max") {
            send_DRS_can_msg(P_DRS_MAX_ANGLE_CALI, current_value);
            ui->Curr_Max->setText(QString::number(current_value));
        }
    }
    return;
}

void drs_cal::on_dec_clicked()
{
    int current_value;

    current_value = ui->value->text().toInt();
    current_value--;
    ui->value->setText(QString::number(current_value));
    if(ui->option->currentText() == "Calibration"){
        if(ui->limit->currentText() == "Min"){
            send_DRS_can_msg(P_DRS_MIN_ANGLE_CALI, current_value);
            ui->Curr_min->setText(QString::number(current_value));
        }
        else if (ui->limit->currentText() == "Max") {
            send_DRS_can_msg(P_DRS_MAX_ANGLE_CALI, current_value);
            ui->Curr_Max->setText(QString::number(current_value));
        }
    }
    return;
}

void drs_cal::on_DRS_number_currentIndexChanged(const QString &arg1)
{
    if(arg1 == ""){
        ui->send->setEnabled(0);
    }
    else{
        ui->send->setEnabled(1);
    }
    return;
}
