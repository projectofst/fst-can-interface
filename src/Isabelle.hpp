#ifndef ISA_H
#define ISA_H

#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "Console.hpp"
#include "COM_SerialPort.hpp"
#include "UiWindow.hpp"
#include "isa_config.hpp"
#include "LEDwidget.hpp"

namespace Ui {
class ISA;
}

class ISA : public UiWindow
{
    Q_OBJECT

public:
    explicit ISA(QWidget *parent = nullptr);
    ~ISA() override;

private slots:

    void on_set_threshold_button_clicked();

    void process_CAN_message(CANmessage) override;
    int64_t calculate_maxormin(CANdata message);
    int binary_check(int binary);

    void on_run_button_clicked();

    void on_stop_button_clicked();

    void on_reset_button_clicked();

    void send_to_isa(int message_sid, int DB0, int DB1, int DB2, int DB3, int DB4, int DB5, int DB6, int DB7);

    void on_config_button_clicked();

    void test();
    void on_store_button_clicked();

signals:
    void send_to_CAN(CANmessage msg) override;

private:
    Ui::ISA *ui;
    int positive_threshold;
    int negative_threshold;
    int current_current;
    int v1_current;
    int v2_current;
    int v3_current;
    int temp_current;
    int power_current;
    int curr_count_current;
    int energy_count_current;
    int temp_min;
    int power_min;
    int curr_count_min;
    int energy_count_min;
    int cont;
    QList<int> measurment_erros;
    int resto;
    int error_number;
    int num_current;
    int stop_mode;
    isa_config *isa_config_dialog;

};

#endif // ISA_H
