#ifndef VERSIONS_HPP
#define VERSIONS_HPP

#include <QWidget>
#include "UiWindow.hpp"
#include "pcb_version.hpp"
namespace Ui {
class versions;
}


class versions : public UiWindow
{
    Q_OBJECT

public:
    explicit versions(QWidget *parent = nullptr);
    ~versions();
    Ui::versions *ui;

private slots:
    void read_versions();
    void process_CAN_message(CANmessage message);
    void request_version();
    void init_devices();
    //uint64_t cast_versions(int dev_id);

    void on_refresh_button_clicked();


   void process_new_version(CANmessage message);

   int device_in_table(QString device);

   void on_load_clicked();

private:
    QVector<pcb_version * > all_pcbs;
    QTimer *timer1;
    QVector<uint16_t> devices;
    int number_of_devices;
    uint16_t request_byte;
    int timer_status;
};

#endif // VERSIONS_HPP
