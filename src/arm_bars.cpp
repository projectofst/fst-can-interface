#include "arm_bars.hpp"
#include "ui_arm_bars.h"

arm_bars::arm_bars(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::arm_bars)
{
    ui->setupUi(this);
    set_new_minimum(0);
    set_new_maximum(21);
}

arm_bars::~arm_bars()
{
    delete ui;

}
void arm_bars::set_new_minimum(double value){
    minimum = value;
    ui->min_val->setText(QString::number(value));
    ui->bar->setMaximum(minimum);
    return;
}

void arm_bars::set_new_maximum(double value){
    maximum = value;
    ui->max_val->setText(QString::number(value));
    if (value < 0){
        ui->bar->setMaximum(-maximum);
    }
    else {
        ui->bar->setMaximum(maximum);
    }

    return;
}
void arm_bars::update_value(double new_value){
    if(new_value < 0)
        ui->bar->setValue(-new_value);
    else {
        ui->bar->setValue(new_value);
    }
    return;
}

int arm_bars::get_limit(){
    //return limit;;
    return 0;
}
int arm_bars::get_current_val(){
    return current_value;
}
