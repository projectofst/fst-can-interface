#include "COM_Interface.hpp"

Interface::Interface()
{
    StatusInt = 0;

    OptionList.append("(no options)");
}

bool Interface::open(QString)
{
    StatusInt = 1;
    return true;
}

void Interface::read()
{

}

int Interface::close(void)
{
    return 0;
}

int Interface::send(CANmessage msg)
{
    if (StatusInt == 1)
        emit broadcast_in_message(msg);

    return 1;
}


