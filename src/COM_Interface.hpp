#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "COM.hpp"
#include "can-ids/CAN_IDs.h"
#include <QObject>

#include <QSqlDatabase>
#include <QSqlQuery>

namespace Ui {
    class Interface;
}

class Interface : public COM
{
    Q_OBJECT

public:
    Interface();
    virtual bool open(QString) override;
    virtual int close(void) override;
    virtual int status(void) override {return StatusInt;}
        int StatusInt;
    virtual QStringList options(void) override {return OptionList;}
        QStringList OptionList;
public slots:
    virtual void read(void) override;
    virtual int send(CANmessage) override;

signals:
    void new_messages(CANmessage);
    void broadcast_in_message(CANmessage);
};

#endif // COM_INTERFACE_HPP
