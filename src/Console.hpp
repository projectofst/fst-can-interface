/**********************************************************************
 *	 FST CAN tools --- interface
 *
 *	 Console widget header
 *	 _______________________________________________________________
 *
 *	 Copyright 2014 Bruno Santos <brunomanuelsantos@tecnico.ulisboa.pt>
 *
 *	 This program is free software; you can redistribute it and/or
 *	 modify it under the terms of the GNU General Public License
 *	 as published by the Free Software Foundation; version 2 of the
 *	 License only.
 *
 *	 This program is distributed in the hope that it will be useful,
 *	 but WITHOUT ANY WARRANTY; without even the implied warranty of
 *	 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *	 GNU General Public License for more details.
 *
 *	 You should have received a copy of the GNU General Public License
 *	 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 **********************************************************************/

#ifndef CONSOLE_HPP
#define CONSOLE_HPP

#include <QWidget>
#include <QTimer>
#include <QDateTime>
#include <QFile>
#include <QTextStream>
#include <QMutex>
#include <queue>
#include <QVector>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>

#include "COM_SerialPort.hpp"
#include "can-ids/CAN_IDs.h"
#include "Printer.hpp"
#include "filterwidget.hpp"
#include "Console_idlist.hpp"
#include "Console_makeid.hpp"
#include "Plotting.hpp"

#include "UiWindow.hpp"

#define MAX_LOG_LINES_CONSOLE		100	// Number of messages to hold in window
#define LOG_FLUSH_timestamp	5000	// Time between flushes

namespace Ui {
	class Console;
}

class Console : public UiWindow
{
	Q_OBJECT

public:
		explicit Console(QWidget *parent = nullptr);
        ~Console(void) override;
        bool _linked;

private slots:
        void process_CAN_message(CANmessage msg) override;
        void process_print_CAN_raw_message(QByteArray);
		void on_consoleclear_clicked(void);
		void on_sendbutton_clicked(void);

		void _log_changed(bool log_status);
		void _flush_log(void);

        void on_pushButton_debugger_clicked();

        void on_pushButton_5_clicked();

        void on_show_ids_clicked();
		void send_start_log();

        void set_id_value(int id);

        void on_AddMessageFilterButton_clicked();

        void on_make_id_clicked();

        void on_var_type_currentIndexChanged(const QString &arg1);

        void update();

        void update_LED();

private:
        Ui::Console *ui;
        std::queue<CANmessage> _messsage_queue;
		void calculate_filters();
		QTimer _log_timer;
		bool _logging;
		QMutex _logging_mutex;
        void log_to_file(CANmessage msg);
        bool filter_message(CANmessage msg);
        void write_to_console(CANmessage msg);
		QFile _logfile;
		QTextStream _logstream;
		int _sockfd;
		QMutex _queueLck;
        QVector<FilterWidget *> filter_vector;
        int no_output;
        IDList id_list;
        MakeID make_id;
        QDateTime time;
        QTimer *tester_timer;
        QTextStream *in;
        QString var_type;
        QQueue<CANmessage> messages;
        QTimer *LED_timer;

signals:
    void send_to_CAN(CANmessage) override;

};

#endif
