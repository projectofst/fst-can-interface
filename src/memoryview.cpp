#include <QTextEdit>
#include <QString>
#include <QTimer>
#include <QLabel>
#include <QFormLayout>

#include <stdint.h>

#include "memoryview.hpp"
#include "ui_memoryview.h"

MemoryView::MemoryView(QWidget *parent) :
    UiWindow(parent),
    ui(new Ui::MemoryView)
{
    ui->setupUi(this);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    count = (unsigned *) malloc(sizeof(unsigned));
    *count = 0;

    _msg_id = (unsigned *) malloc(sizeof(unsigned));
    *_msg_id = 0;

    for (int i=0; i<128; i++) {
        QString position = QString::number(i);
        QLabel *label = new QLabel;

        QString value = QString::number(0);
        label->setText(value);
        ui->memory->addRow(position, label);
    }
}

MemoryView::~MemoryView()
{
    delete ui;
}

void MemoryView::update(void) {
    if (*count == 128) {
        *count = 0;
        timer->stop();
    }

    CANdata msg;
    CANmessage message;
    msg.dev_id = DEVICE_ID_INTERFACE;
    msg.msg_id = *_msg_id;
    msg.data[0] = *count;
    *count++;

    message.candata = msg;

    emit send_to_CAN(message);
}

void MemoryView::process_CAN_message(CANmessage message) {
    CANdata msg = message.candata;
    if (!msg.dev_id == 0xF || !msg.msg_id == 33) {
         return;
    }

    QString position = QString::number(msg.data[0]);
    QLabel *position_label = new QLabel;
    position_label->setText(position);
    QLabel *label = new QLabel;
    QString value = QString::number(msg.data[1]);
    label->setText(value);
    ui->memory->setWidget(msg.data[0], QFormLayout::LabelRole, position_label);
    ui->memory->setWidget(msg.data[0], QFormLayout::FieldRole, label);

}

void MemoryView::on_update_clicked()
{
    QString msg_id = ui->msg_id->toPlainText();

    *_msg_id = msg_id.toInt();

    *count = 0;
    timer->start(100);
}
