#include "DevStatus.hpp"
#include "ui_DevStatus.h"

DevStatus::DevStatus(QWidget *parent,QObject *picasso) :
    UiWindow(parent),
    ui(new Ui::DevStatus)
{
    ui->setupUi(this);

    // Create Gradient Colors
    red = QLinearGradient(100,0,0,100);
        red.setColorAt(0,QColor(100,0,0));
        red.setColorAt(1,QColor(150,0,0));
    green = QLinearGradient(100,0,0,100);
        green.setColorAt(0,QColor(0,100,0));
        green.setColorAt(1,QColor(0,150,0));
    yellow = QLinearGradient(100,0,0,100);
        yellow.setColorAt(0,QColor(100,100,0));
        yellow.setColorAt(1,QColor(150,150,0));

    lastK = 0;
    currentK = 1;
    createDevs();

    QTimer *verifyStatus = new QTimer;
    verifyStatus->setInterval(5000);
    verifyStatus->start();
    connect(verifyStatus,SIGNAL(timeout()),this,SLOT(silentWarning()));
}

DevStatus::~DevStatus(void)
{
   delete ui;
}

void DevStatus::process_CAN_message(CANmessage Msg)
{
    Id = Msg.candata.dev_id;
    if (!DevButtons.contains(Id)) {
        qDebug("Not Listed ID!!!");
    } else {
        QPalette temppal = DevButtons[Id]->palette();
        temppal.setBrush(QPalette::Button,green);
        DevButtons[Id]->setPalette(temppal);
        DevTimers[Id]->start(3000);
        DevStates[Id] = 1;
    }
}

void DevStatus::setButtonOrder()
{
    QSettings devstatorder(QString("DeviceStatusOrder").prepend("set/"),QSettings::IniFormat);
    QStringList buttonorder = devstatorder.value("IdList").toStringList();

}

QList<int> DevStatus::saveButtonOrder()
{
    QStringList *list = new QStringList;
    for (int i = 0;i<ui->listWidget->count();i++)
        list->append(ui->listWidget->item(i)->text());
    QSettings devstatorder(QString("DeviceStatusOrder").prepend("set/"),QSettings::IniFormat);
    devstatorder.beginGroup("Default");
    devstatorder.setValue("IdList",QVariant(*list));
    devstatorder.endGroup();
    devstatorder.sync();
    setButtonOrder();
}

void DevStatus::resize(int Width)
{
    layoutDevs(((Width-80)/100));
}

void DevStatus::silentWarning()
{
    foreach (QTimer *timer,DevTimers) {
        if (DevStates[DevTimers.key(timer)] == 1) {
            if (!timer->isActive()) {
                QPalette temppal = DevButtons[DevTimers.key(timer)]->palette();
                temppal.setBrush(QPalette::Button,yellow);
                DevButtons[DevTimers.key(timer)]->setPalette(temppal);
            }
        }
    }
}

void DevStatus::createDevs()
{
    for (int i=0;i<32;i++) {
        DevButtons[i] = new QCommandLinkButton(QString(nomes_placas[i]),QString("Description"));
        QPalette temppal = DevButtons[i]->palette();
        temppal.setBrush(QPalette::Button,red);
        DevButtons[i]->setAutoFillBackground(true);
        DevButtons[i]->setPalette(temppal);
        DevButtons[i]->setMinimumSize(QSize(100,50));
        DevButtons[i]->setMaximumSize(QSize(100,50));

        DevTimers[i] = new QTimer;
        DevTimers[i]->setSingleShot(true);

        DevStates[i] = 0;
    }
}

void DevStatus::layoutDevs(int k)
{
    k = k - 1;
    if (lastK != k) {
        QGridLayout *DevicesGrid = new QGridLayout;
        int c = 0;
        int l = 0;
        foreach (QWidget *Button,DevButtons) {
            DevicesGrid->addWidget(Button,l,c);
            c++;
            if (c>k) {
                c = 0;
                l++;
            }
        }
        foreach (QObject *Button,ui->Devs->layout()->children()){
            ui->Devs->layout()->removeWidget(static_cast<QWidget*>(Button));
        }
        delete ui->Devs->layout();
        ui->Devs->setLayout(DevicesGrid);
        lastK = k;
    }
}

void DevStatus::on_SettingsButton_toggled(bool checked)
{
    if (checked) {
        ui->DevStatusStack->setCurrentIndex(1);
    } else {
        ui->DevStatusStack->setCurrentIndex(0);
    }
}
