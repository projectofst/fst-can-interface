#ifndef AMSDETAILS_H
#define AMSDETAILS_H

#include <QWidget>
#include <QVector>

#include "AMS_Stack_Details.hpp"
#include "AMS_Stack_Summary.hpp"
#include "CAN_IDs.h"

namespace Ui {
class AMSDetails;
}

class AMSDetails : public QWidget
{
    Q_OBJECT

public:
    explicit AMSDetails(QWidget *parent = 0);
    ~AMSDetails();
    QVector<StackDetails *> stacks;
    Ui::AMSDetails *ui;
    void clear(void);
    void updateCell(int stack, int cell, double voltage, double temperature);
    void updateTempFault(int stack, int cell, bool fault);
    void updateVoltageFault(int stack, int cell, bool fault);
    void updateDischargeFault(int stack, int cell, bool fault);
    void updateDischargeState(int stack, int cell, bool fault);



private slots:

signals:

private:

};

#endif // AMSDETAILS_H
