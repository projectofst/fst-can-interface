#include <QLabel>
#include <QWindow>

#include "Main_About.hpp"
#include "ui_Main_About.h"

About::About(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::About)
{
    ui->setupUi(this);

    //printf("%d\n", VERSION);

    QString version = QString("Version: %1").arg(GIT_CURRENT_SHA1);
    this->setWindowTitle("About");
    this->resize(10, 10);
    ui->version->setText(version);
}

About::~About()
{
    delete ui;
}
