#ifndef FILTERWIDGET_H
#define FILTERWIDGET_H

#include <QWidget>

namespace Ui {
class FilterWidget;
}

class FilterWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FilterWidget(QWidget *parent = nullptr);
    ~FilterWidget();
    Ui::FilterWidget *ui;
    bool active;

private slots:

    void on_delete_2_clicked();

    void on_enabled_stateChanged(int);

private:
};

#endif // FILTERWIDGET_H
