#include "LEDwidget.hpp"

LEDwidget::LEDwidget(QWidget *parent)
	: QWidget(parent)
{
	turnOff();
	return;
}

LEDwidget::~LEDwidget(void)
{
	return;
}

void LEDwidget::setColor(QColor new_color)
{
	if (color != new_color) {
		color = new_color;
		update();
	}
}

void LEDwidget::paintEvent(QPaintEvent *)
{
	painter.begin(this);
	painter.setRenderHint(QPainter::Antialiasing);
	painter.setBrush(color);
	painter.drawEllipse(0, 0, width(), height());
	painter.end();

	return;
}
