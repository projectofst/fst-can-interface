#include <QCheckBox>
#include "filterwidget.hpp"
#include "ui_filterwidget.h"

FilterWidget::FilterWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FilterWidget)
{
    ui->setupUi(this);
    ui->enabled->setCheckState(Qt::Checked);
    this->active = true;
}

FilterWidget::~FilterWidget()
{
    delete ui;
}

void FilterWidget::on_delete_2_clicked()
{
}

void FilterWidget::on_enabled_stateChanged(int)
{
}
