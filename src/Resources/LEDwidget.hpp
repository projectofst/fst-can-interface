#ifndef LEDWIDGET_HPP
#define LEDWIDGET_HPP

#include <QWidget>
#include <QPainter>
#include <QColor>

class LEDwidget : public QWidget
{
	Q_OBJECT

public:
	LEDwidget(QWidget *parent = 0);
	~LEDwidget(void);

	void setColor(QColor new_color);

	QColor getColor() { return color; }

	void turnGreen()	{ setColor(Qt::green);		}
	void turnRed()		{ setColor(Qt::red);		}
	void turnBlue()		{ setColor(Qt::blue);		}
	void turnYellow()	{ setColor(Qt::yellow);		}
	void turnCyan()		{ setColor(Qt::cyan);		}
	void turnOff()		{ setColor(Qt::lightGray);	}

private:
	QColor color;
	QPainter painter;

	void paintEvent(QPaintEvent *);
};

#endif // LEDWIDGET_HPP
