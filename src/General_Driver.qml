import QtQuick 2.2
import QtQuick.Extras 1.4
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4

Rectangle {
    id: item1
    color: "#353637"
    Image {
        id: image
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.top: parent.top
        anchors.left: parent.left
        fillMode: Image.PreserveAspectFit
        scale: 0.3
        anchors.topMargin: 0
        source: "../../../Downloads/Formula-4-1000x666.png"
    }

    Rectangle {
        id: rectangle
        x: 253
        y: 215
        width: 110
        height: 62
        color: "#ff2d2d"
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenterOffset: 0
        anchors.horizontalCenter: parent.horizontalCenter
        opacity: 1

        Text {
            id: text1
            x: 35
            y: 59
            width: 129
            height: 82
            font.bold: true
            anchors.verticalCenterOffset: 0
            anchors.horizontalCenterOffset: 0
            fontSizeMode: Text.Fit
            renderType: Text.QtRendering
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            elide: Text.ElideNone
            font.family: "Arial"
            lineHeight: 0
            anchors.verticalCenter: rectangle.verticalCenter
            anchors.horizontalCenter: rectangle.horizontalCenter
            font.pixelSize: 17
        }
    }

    Gauge {
        id: gauge
        x: 60
        y: 0
        width: 148
        height: 374
        anchors.topMargin: 53
        anchors.top: parent.top
        value: 100

        style: GaugeStyle {
                    valueBar: Rectangle {
                        implicitWidth: 28
                        gradient: Gradient {
                            GradientStop { position: 0.0; color: "red" }
                            GradientStop { position: 1.0; color: "#00B7FF" }
                        }
                    }
        }
    }

    CircularGauge {
        id: circularGauge
        x: 367
        y: 247
    }

}

/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:1;anchors_height:100;anchors_width:100;anchors_x:156;anchors_y:152}
}
 ##^##*/
