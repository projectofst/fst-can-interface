﻿#ifndef PLOTTING_H
#define PLOTTING_H

#include <QWidget>

#include "can-ids/CAN_IDs.h"
#include <QtCharts>
#include <QQueue>
#include <QTimer>
#include "plot_filter_widget.hpp"
#include <QtWidgets/QGraphicsView>
#include <QtCharts/QChartGlobal>
#include "UiWindow.hpp"
#include <QPair>
#ifndef Q_OS_WIN
#include <exptrk.hpp>
#endif





class QGraphicsScene;
class QMouseEvent;
class QResizeEvent;

QT_CHARTS_BEGIN_NAMESPACE
class QChart;
QT_CHARTS_END_NAMESPACE

class Callout;
template <typename T>
T foo(T v)
{
    return std::abs(v + T(2)) / T(3);
}

typedef struct{
    uint64_t time;
    int16_t data;
}Data;

typedef struct {
    uint16_t msg_id: 6;
    uint16_t dev_id: 5;
    uint16_t offset;
    uint16_t length;
    uint16_t plot_id;
}Filter;

typedef struct {
    double low_x;
    double high_x;
    double low_y;
    double high_y;
}Limits;
typedef struct {
    QLineSeries *line_series;
    Filter filter;
    QVector<QPointF> buffer;
    QQueue<Data> dataqueue;
    int32_t colour;
    Limits limits;
    int sum;
    int sample_number;
    uint64_t previous_timestamp;
    uint16_t previous_timestamp_aux;
    double average;
    int parent_1_prev_buffer_size;
    int parent_2_prev_buffer_size;
    QString operation;
    double operation_value;
    QVector<QPair<int, int>> math_parent;
    int parent;
    QVector<int> children;
    double area;
    QList<Callout *> m_callouts;
    bool being_logged;
    int integral_sample;
    int derivative_sample;
    bool hidden;
    QString custom_expression;
}Plot_info;

typedef struct{
    QString device;
    int dev_id;
    QVector<QString> signal; /*Pair of message name and message_id*/
    QVector<int> msg_ids, offset, length;
}device_messages;

#ifndef Q_OS_WIN
    typedef exprtk::symbol_table<double> symbol_table_t;
    typedef exprtk::expression<double>     expression_t;
    typedef exprtk::parser<double>             parser_t;
#endif

namespace Ui {
class plotting;
}

class plotting : public UiWindow
{
    Q_OBJECT

public:
    explicit plotting(QWidget *parent = nullptr);
    QString log_name;
    ~plotting();

public slots:

    void remove_line_series(plot_filter_widget *filter);

    void tooltip(QPointF point, bool state);

private slots:
    void process_CAN_message(CANmessage message);

    void send_to_CAN(CANmessage);

    void on_clear_plot_clicked();

    void on_set_button_clicked();

    /// This method does something
    void on_clear_x_vals_clicked();
    /**
     * @brief This is a function
     * @param point : This is a point
     * @param plot_id : This is the plot_id
     */
    void find_axisScale(QPointF point, int plot_id);

    void adjust_axis();


    void save_can_data(CANmessage message, QVector<int> plot_ids);

    void update_plot();

    void on_hold_plot_button_clicked();

    void on_set_scale_button_clicked();

    QVector<int> find_plot(CANmessage message);

    void set_new_plot();

    bool find_filter(int msg_id, int dev_id, int offset, int length);

    void on_refresh_colours();

    void reset_axis();

    void on_test_but_clicked();

    void calc_average(double value, Plot_info *plot);


    double derivative(Plot_info *parent, Plot_info *plot);

    double integral(Plot_info *parent, Plot_info *plot);

    void keepCallout();

    void on_hold_plot_button_toggled(bool checked);

    void on_values_edit_returnPressed();

    void update_msg_id_combo_box();

    void on_scaling_button_toggled(bool checked);

    void update_callouts_plot_id();

    int find_pair(int parent, int child);

    void process_math_channels(Plot_info *plot);

    void process_simple_math(Plot_info *plot);

    void init_math_channel(int id);

    void calc_next_math_point(int id, int parent_1, int parent_2, int parent_1_cont, int parent_2_cont);

    void find_next_plot_color(plot_filter_widget* filter);

    void save_to_png();

    void save_to_csv();

    void on_apply_adv_math_clicked();

    void on_Apply_simple_math_clicked();

    void on_save_export_button_clicked();


    void get_messages();

    void on_set_plot_messages_clicked();

    int64_t cast_message(CANmessage message);
#ifndef Q_OS_WIN
    bool check_valid_expression(QString expression, int number_of_variables);

    double calc_custom_expression_result(QString in_expression, QVector<double> values);

    void custom_op_line_edit();
#endif
    void on_msg_id_box_currentIndexChanged(int index);

private:
    Ui::plotting *ui;
    int  points_number, hold_plot, first_sample, number_of_plots, plot_found, font_size,
    first_sample_derivative, do_not_plot_derivative, derivative_sample, current_plot, screen_shot_counter, integral_sample;

    double l_x, l_y, h_x, h_y, trigger;
    float total_area;

    QValueAxis *axisX;
    QPointF *new_point;
    QChartView *chartView;
    QChart *plot;
    QValueAxis *axisY;

    QQueue<Data> plot_queue;

    Data data;
    Filter filter;
    Plot_info plot_info;

    QTimer *timer;

    QVector<Plot_info> plot_vec;
    uint16_t plot_number;
    QLineSeries *series;
    QLineSeries *new_line_series;

    QVector<plot_filter_widget *> filter_vector;
    QMap<QString,int> colour_map;
    QString legend;

    QVector<QPointF> empty_vec;
    QVector<QString> colour_vector;

    QGraphicsSimpleTextItem *m_coordX;
    QGraphicsSimpleTextItem *m_coordY;

    Callout *m_tooltip;
    Callout *callout_teste;

    QVector<int> math_plots;
    QVector<device_messages> messages;
};


#endif // PLOTTING_H
