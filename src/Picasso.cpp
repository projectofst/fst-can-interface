#include "Picasso.hpp"

Picasso::Picasso(MainToolbar *toolbar,MainWindow *parent,Comsguy *coms)
{
    mainwindow = parent;
    MainToolBar = toolbar;
    LayoutDir = QDir("set/Layouts");

    connect(MainToolBar, &MainToolbar::openUiWindow,
            this, &Picasso::openDockWidget);
    connect(MainToolBar, &MainToolbar::saveLayout,
            this, &Picasso::saveLayout);
    connect(MainToolBar, &MainToolbar::deleteLayout,
            this, &Picasso::closeLayout);
    connect(MainToolBar->LayoutCombo, QOverload<const QString &>::of(&QComboBox::activated),
            this, &Picasso::loadLayout);
    updateToolbar();

    test = new SQL_CANID;
}

Picasso::~Picasso(void)
{

}

void Picasso::saveLayout(void)
{
    // Layout file creation
    QStringList LayoutList = LayoutDir.entryList(QStringList("*.ini"), QDir::NoFilter, QDir::NoSort);
    LayoutList.replaceInStrings(".ini","");
    int i = 1;
    QString init = "Layout";
    foreach (QString LayoutName,LayoutList) {
        QString is = QString::number(i);
        QString TempName = init;
        TempName.append(is);
        if (0==LayoutName.compare(TempName))
            i = i + 1;
    }
    QString is = QString::number(i);
    LayoutName = init.append(is.append(".ini"));
    QSettings layout(LayoutName.prepend("set/Layouts/"),QSettings::IniFormat);

    // Add DockWidget Data
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    foreach (QDockWidget* dock, ListOfChildren) {
        layout.beginGroup(dock->objectName());
        layout.setValue("Geometry",dock->geometry());
        layout.endGroup();
    }

    // Add TabbedDocks Data
    while (!ListOfChildren.isEmpty()) {
        foreach (QDockWidget* dock, ListOfChildren) {
            QList<QDockWidget*> TabbedDocks = mainwindow->tabifiedDockWidgets(dock);
            if (!TabbedDocks.isEmpty()) {
                foreach (QDockWidget *tabdock, TabbedDocks) {
                    layout.beginGroup(tabdock->objectName());
                    layout.setValue("ParentDock",dock->objectName());
                    layout.endGroup();
                    ListOfChildren.removeOne(tabdock);
                }
            }
            layout.beginGroup(dock->objectName());
            layout.setValue("ParentDock",QVariant());
            layout.endGroup();
            ListOfChildren.removeOne(dock);
            break;
        }
    }

    // Save MainWindowState
    layout.beginGroup("MainWindowState");
    layout.setValue("geometry", mainwindow->saveGeometry());
    layout.setValue("windowState", mainwindow->saveState());
    layout.endGroup();

    // Save
    layout.sync();
    updateToolbar();

}

void Picasso::loadLayout(QString Layout)
{
    closeLayout();

    // Add Docks
    QSettings newlayout(Layout.prepend("set/Layouts/").append(".ini"),QSettings::IniFormat,nullptr);
    foreach (QString group, newlayout.childGroups()) {
        if (group!="MainWindowState") {
            openDockWidget(group.mid(1,7));
        }
    }

    // Tabbify respective
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    ListOfDocks.clear();
    foreach (QDockWidget *dock, ListOfChildren)
        ListOfDocks.append(dock->objectName());
    foreach (QString group, newlayout.childGroups()) {
        newlayout.beginGroup(group);
        if (newlayout.value("ParentDock").isValid()) {
            mainwindow->tabifyDockWidget(ListOfChildren.at(ListOfDocks.indexOf(newlayout.value("ParentDock").toString(),0)),
                                         ListOfChildren.at(ListOfDocks.indexOf(group)));
        }
        newlayout.endGroup();
    }

    // Load MainWindowState
    newlayout.beginGroup("MainWindowState");
    mainwindow->restoreGeometry(newlayout.value("geometry").toByteArray());
    mainwindow->restoreState(newlayout.value("windowState").toByteArray());
    newlayout.endGroup();
}

void Picasso::closeLayout(void)
{
    ListOfChildren = mainwindow->findChildren<QDockWidget *>();
    foreach (QObject* object, ListOfChildren)
        delete object;
}

void Picasso::openDockWidget(QString type)
{
    QMetaObject EnumList = Picasso::staticMetaObject;
    Types EnumValue = static_cast<Types>(EnumList.enumerator(
                                             EnumList.indexOfEnumerator(
                                                 "Types")).keyToValue(type.toLocal8Bit().data()));

    switch (EnumValue) {
    case GenDock: {
        dockwidget = new QDockWidget(tr("General"), mainwindow);
        dockwidget ->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new General();
        break;
    }
    case ConDock: {
        dockwidget = new QDockWidget(tr("Console"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new Console;
        break;
    }
    case AMSDock: {
        dockwidget = new QDockWidget(tr("AMS"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new AMS;
        break;
    }
    case InvDock: {
        dockwidget = new QDockWidget(tr("Motor Controllers"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new Inverters;
        break;
    }
    case SenDock: {
        dockwidget = new QDockWidget(tr("Sensors"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new Sensors;
        break;
    }
    case IsaDock: {
        dockwidget = new QDockWidget(tr("Isabelle"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new ISA;
        break;
    }
    case PloDock: {
        dockwidget = new QDockWidget(tr("Plot"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new plotting;
        break;
    }
    case MTBDock: {
        dockwidget = new QDockWidget(tr("Motor Test Bench"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new TestBench;
        break;
    }
    case DevDock: {
        dockwidget = new QDockWidget(tr("Device Status"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new DevStatus;
        dockwidget->installEventFilter(mainwindow);
        connect(mainwindow,SIGNAL(resizeDev(int)),widget,SLOT(resize(int)));
        break;
    }
    case DriDock: {
        dockwidget = new QDockWidget(tr("Driver Inputs"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new DriverInputs;
        break;
    }
    case BatDock: {
        dockwidget = new QDockWidget(tr("Battery Stats"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new BatteryT;
        dockwidget->installEventFilter(mainwindow);
        connect(mainwindow,SIGNAL(resizeBat(int)),widget,SLOT(resize(int)));
        break;
    }
    case ShuDock: {
        dockwidget = new QDockWidget(tr("Shutdown Circuit"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        QQuickWidget *temp = new QQuickWidget(QUrl("file:///Users/diogopereira/Documents/fst-can-interface/src/Shutdown.qml"),dockwidget);
        temp->setResizeMode(QQuickWidget::SizeRootObjectToView);
        dockwidget->setWidget(static_cast<QWidget*>(temp));
        break;
    }
    case CDBDock: {
        dockwidget = new QDockWidget(tr("Shutdown Circuit"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new SQL_CANID_Select(nullptr,test);
        break;
    }
    case MVDock: {
        dockwidget = new QDockWidget(tr("Memory View"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new MemoryView;
        break;
    }
    case Versions:{
        dockwidget = new QDockWidget(tr("Versions"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new versions;
        break;
    }
    case DRS_cal:{
        dockwidget = new QDockWidget(tr("DRS_cal"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new drs_cal;
        break;
    }
    case logconsole:{
        dockwidget = new QDockWidget(tr("logconsole"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new Log;
        break;
    }
    case ArmDock:{
        dockwidget = new QDockWidget(tr("Arm"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new arm;
        break;
    }
    case ccu:{
        dockwidget = new QDockWidget(tr("CCU"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new CCU;
        break;
    }
    case LapDock:{
        dockwidget = new QDockWidget(tr("Lap"), mainwindow);
        dockwidget->setAllowedAreas(Qt::AllDockWidgetAreas);
        mainwindow->addDockWidget(Qt::TopDockWidgetArea, dockwidget);
        widget = new Lap_time;
        break;
    }
    }
    dockwidget->setAttribute(Qt::WA_DeleteOnClose);
    if (EnumValue!=ShuDock) {
        dockwidget->setWidget(dynamic_cast<QWidget *> (widget));
        updateObjectName(dockwidget,type.prepend("[").append("]"));
        connect(mainwindow->interfaceCom, &Interface::broadcast_in_message,
                widget, &UiWindow::process_CAN_message);
        connect(widget, SIGNAL(send_to_CAN(CANmessage)),
                mainwindow->interfaceCom, SIGNAL(new_messages(CANmessage)));
    }
}

void Picasso::updateObjectName(QObject *TargetObj, QString ObjType)
{
    repeated = 0;
    QList<QObject*> ListOfChildren = mainwindow->children();
    foreach (QObject* object, ListOfChildren) {
        if (object->objectName().startsWith(ObjType))
            repeated = repeated + 1;
    }
    FinalName.sprintf("%s - %d",ObjType.toLocal8Bit().data(),repeated+1);
    TargetObj->setObjectName(FinalName);
}

void Picasso::updateToolbar(void)
{
    MainToolBar->LayoutCombo->clear();
    QStringList LayoutList = LayoutDir.entryList(QStringList("*.ini"), QDir::NoFilter, QDir::NoSort);
    LayoutList.replaceInStrings(".ini","");
    MainToolBar->LayoutCombo->addItems(LayoutList);
}
