#include "Inverters.hpp"
#include "ui_Inverters.h"
#include <stdio.h>

#include <QVBoxLayout>
#include <QtCharts>
#include <QLineSeries>
#include <QChartView>
#include <QTableView>
#include <QThread>

#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/IIB_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/ARM_CAN.h"
#include "Resources/LEDwidget.hpp"

#include "iib_motor_control.hpp"
#include "ui_iib_motor_control.h"

using namespace QtCharts;

//static int current_mode = MODE_TRACTION_LIMIT;


const char *MODES[] = {
    "ARM",
    "DEFAULT",
    "TEST BENCH",
    "ERROR"
};

Inverters::Inverters(QWidget *parent) : UiWindow(parent) , ui(new Ui::Inverters)
{
    ui->setupUi(this);
    specific = "";
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Inverters::timer_clear_inverters);
    timer->start(2000);

    clear_inv_flag = 0;
    Inv_cleared = 0;
    regen_on = 0;

    manager = new state_manager();

    m_sSettingsFile = QDir::currentPath() +"/set/states/" + NAME + "_"+ specific.toUpper() + "_settings.ini";

}

Inverters::~Inverters()
{
    delete ui;
}

void Inverters::process_CAN_message(CANmessage message) {
    CANdata msg = message.candata;
    clear_inv_flag = 0;
    Inv_cleared = 0;
    if(msg.dev_id==DEVICE_ID_IIB){
        switch(msg.msg_id){
        case MSG_ID_IIB_MOTOR_INFO:{
            IIB_MOTOR_info motor;
            parse_IIB_motor_info_message(msg.data, &motor);
            int16_t motor_temp, motor_speed, motor_torque,motor_current;
            motor_temp = motor.temp_motor;
            motor_speed = motor.actual_speed;
            motor_torque = motor.torque;
            motor_current = motor.current;

            //Motor depending on node:
            switch(motor.motor_num){
            case 0:{
                ui->inverters_widget->ui->MotorTemp_FL->setText(QString::number(motor_temp,'f',1));
                ui->inverters_widget->ui->Torque_FL->setText(QString::number(motor_torque,'f',1));
                ui->inverters_widget->ui->Speed_FL->setText(QString::number(motor_speed,'f',1));
                ui->inverters_widget->ui->Current_FL->setText(QString::number(motor_current,'f',1));
                return;
            }
            case 1:{
                ui->inverters_widget->ui->MotorTemp_FR->setText(QString::number(motor_temp,'f',1));
                ui->inverters_widget->ui->Torque_FR->setText(QString::number(motor_torque,'f',1));
                ui->inverters_widget->ui->Speed_FR->setText(QString::number(motor_speed,'f',1));
                ui->inverters_widget->ui->Current_FR->setText(QString::number(motor_current,'f',1));
                return;
            }
            case 2:{
                ui->inverters_widget->ui->MotorTemp_RL->setText(QString::number(motor_temp,'f',1));
                ui->inverters_widget->ui->Torque_RL->setText(QString::number(motor_torque,'f',1));
                ui->inverters_widget->ui->Speed_RL->setText(QString::number(motor_speed,'f',1));
                ui->inverters_widget->ui->Current_RL->setText(QString::number(motor_current,'f',1));
                return;
            }
            case 3:{
                ui->inverters_widget->ui->MotorTemp_RR->setText(QString::number(motor_temp,'f',1));
                ui->inverters_widget->ui->Torque_RR->setText(QString::number(motor_torque,'f',1));
                ui->inverters_widget->ui->Speed_RR->setText(QString::number(motor_speed,'f',1));
                ui->inverters_widget->ui->Current_RR->setText(QString::number(motor_current,'f',1));
                return;
            }
            }
        }
        case MSG_ID_IIB_INV_INFO:{
            IIB_INV_info inv;
            parse_IIB_inv_info_message(msg.data, &inv);
            unsigned BE_1 = inv.BE_1;
            unsigned BE_2 = inv.BE_2;
            unsigned inverte_active = inv.inverter_active;
            uint16_t temp_IGBT = inv.temp_IGBT;
            uint16_t temp_inverter = inv.temp_inverter;
            uint16_t error_info = inv.error_info;
            switch (inv.i){
            case 0:{
                ui->inverters_widget->ui->IGBT_FL->setText(QString::number(temp_IGBT / 10.0,'f',1));
                ui->inverters_widget->ui->InvTemp_FL->setText(QString::number(temp_inverter / 10.0,'f',1));
                ui->inverters_widget->ui->Error_FL->setText(QString::number(error_info));
                if(BE_1)
                    ui->IN_1_BE_1->turnGreen();
                else
                    ui->IN_1_BE_1->turnOff();
                if(BE_2)
                    ui->IN_1_BE_2->turnGreen();
                else
                    ui->IN_1_BE_2->turnOff();
                if(inverte_active)
                    ui->iib_inv_1_led->turnGreen();
                else
                    ui->iib_inv_1_led->turnOff();

                if (inv.temperature_status.inverter_max_passed){
                    ui->inverters_widget->ui->FL_INV_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.inverter_min_passed){
                    ui->inverters_widget->ui->FL_INV_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FL_INV_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.IGBT_max_passed){
                    ui->inverters_widget->ui->FL_IGBT_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.IGBT_min_passed){
                    ui->inverters_widget->ui->FL_IGBT_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FL_IGBT_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->FL_MT_LED->turnRed();
                }
                else if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->FL_MT_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FL_MT_LED->turnGreen();
                }
                return;
            }
            case 1:{
                ui->inverters_widget->ui->InvTemp_FR->setText(QString::number(temp_inverter / 10.0,'f',1));
                ui->inverters_widget->ui->IGBT_FR->setText(QString::number(temp_IGBT / 10.0,'f',1));
                ui->inverters_widget->ui->Error_FR->setText(QString::number(error_info));
                if(BE_1)
                    ui->IN_2_BE_1->turnGreen();
                else
                    ui->IN_2_BE_1->turnOff();
                if(BE_2)
                    ui->IN_2_BE_2->turnGreen();
                else
                    ui->IN_2_BE_2->turnOff();
                if(inverte_active)
                    ui->iib_inv_2_led->turnGreen();
                else
                    ui->iib_inv_2_led->turnOff();
                if (inv.temperature_status.inverter_max_passed){
                    ui->inverters_widget->ui->FR_INV_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.inverter_min_passed){
                    ui->inverters_widget->ui->FR_INV_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FR_INV_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.IGBT_max_passed){
                    ui->inverters_widget->ui->FR_IGBT_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.IGBT_min_passed){
                    ui->inverters_widget->ui->FR_IGBT_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FR_IGBT_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->FR_MOTOR_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->FR_MOTOR_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->FR_MOTOR_TEMP_LED->turnGreen();
                }
                return;
            }
            case 2:{
                ui->inverters_widget->ui->InvTemp_RL->setText(QString::number(temp_inverter/ 10.0,'f',1));
                ui->inverters_widget->ui->IGBT_RL->setText(QString::number(temp_IGBT/ 10.0,'f',1));
                ui->inverters_widget->ui->Error_RL->setText(QString::number(error_info));
                if(BE_1)
                    ui->IN_3_BE_1->turnGreen();
                else
                    ui->IN_3_BE_1->turnOff();
                if(BE_2)
                    ui->IN_3_BE_2->turnGreen();
                else
                    ui->IN_3_BE_2->turnOff();
                if(inverte_active)
                    ui->iib_inv_3_led->turnGreen();
                else
                    ui->iib_inv_3_led->turnOff();
                if (inv.temperature_status.inverter_max_passed){
                    ui->inverters_widget->ui->RL_INV_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.inverter_min_passed){
                    ui->inverters_widget->ui->RL_INV_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RL_INV_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.IGBT_max_passed){
                    ui->inverters_widget->ui->RL_IGBT_LED->turnRed();
                }
                else if (inv.temperature_status.IGBT_min_passed){
                    ui->inverters_widget->ui->RL_IGBT_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RL_IGBT_LED->turnGreen();
                }

                if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->RL_MOTOR_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->RL_MOTOR_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RL_MOTOR_TEMP_LED->turnGreen();
                }
                return;
            }
            case 3:{

                ui->inverters_widget->ui->InvTemp_RR->setText(QString::number(temp_inverter/ 10.0,'f',1));
                ui->inverters_widget->ui->IGBT_RR->setText(QString::number(temp_IGBT/ 10.0,'f',1));
                ui->inverters_widget->ui->Error_RR->setText(QString::number(error_info));
                if(BE_1)
                    ui->IN_4_BE_1->turnGreen();
                else
                    ui->IN_4_BE_1->turnOff();
                if(BE_2)
                    ui->IN_4_BE_2->turnGreen();
                else
                    ui->IN_4_BE_2->turnOff();
                if(inverte_active)
                    ui->iib_inv_4_led->turnGreen();
                else
                    ui->iib_inv_4_led->turnOff();
                if (inv.temperature_status.inverter_max_passed){
                    ui->inverters_widget->ui->RR_INV_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.inverter_min_passed){
                    ui->inverters_widget->ui->RR_INV_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RR_INV_TEMP_LED->turnGreen();
                }
                if (inv.temperature_status.IGBT_max_passed){
                    ui->inverters_widget->ui->RR_IGBT_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.IGBT_min_passed){
                    ui->inverters_widget->ui->RR_IGBT_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RR_IGBT_TEMP_LED->turnGreen();
                }

                if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->RR_MOTOR_TEMP_LED->turnRed();
                }
                else if (inv.temperature_status.motor_max_passed){
                    ui->inverters_widget->ui->RR_MOTOR_TEMP_LED->turnYellow();
                }
                else {
                    ui->inverters_widget->ui->RR_MOTOR_TEMP_LED->turnGreen();
                }
                return;
            }
            }
        }
        case MSG_ID_IIB_LIMITS:{
            IIB_limits limits;
            parse_IIB_limits_message(msg.data , &limits);
            uint16_t max_op_torque, max_op_power, max_safety_torque, max_safety_power;
            max_op_power = limits.max_operation_power;
            max_op_torque = limits.max_operation_torque;
            max_safety_power = limits.max_safety_power;
            max_safety_torque = limits.max_safety_torque;
            switch(limits.motor_num){
            case 0:{
                ui->inverters_widget->ui->FL_MAX_OP_POWER->setText(QString::number(max_op_power,'f',1));
                ui->FL_info_power->setText(QString::number(max_op_power,'f',1));
                ui->inverters_widget->ui->FL_MAX_OP_TORQUE->setText(QString::number(max_op_torque,'f',1));
                ui->FL_info_torque->setText(QString::number(max_op_torque,'f',1));
                ui->inverters_widget->ui->FL_MAX_SAF_TORQUE->setText(QString::number(max_safety_torque,'f',1));
                ui->inverters_widget->ui->FL_MAX_SAF_POWER->setText(QString::number(max_safety_power * 1000,'f',1));
                break;
            }
            case 1:{
                ui->inverters_widget->ui->FR_MAX_OP_POWER->setText(QString::number(max_op_power,'f',1));
                ui->FR_info_power->setText(QString::number(max_op_power,'f',1));
                ui->inverters_widget->ui->FR_MAX_OP_TORQUE->setText(QString::number(max_op_torque,'f',1));
                ui->FR_info_torque->setText(QString::number(max_op_torque,'f',1));
                ui->inverters_widget->ui->FR_MAX_SAF_TORQUE->setText(QString::number(max_safety_torque,'f',1));
                ui->inverters_widget->ui->FR_MAX_SAF_POWER->setText(QString::number(max_safety_power * 1000,'f',1));
                break;
            }
            case 2:{
                ui->inverters_widget->ui->RL_MAX_OP_POWER->setText(QString::number(max_op_power,'f',1));
                ui->RL_info_power->setText(QString::number(max_op_power,'f',1));
                ui->inverters_widget->ui->RL_MAX_OP_TORQUE->setText(QString::number(max_op_torque,'f',1));
                ui->RL_info_torque->setText(QString::number(max_op_torque,'f',1));
                ui->inverters_widget->ui->RL_MAX_SAF_TORQUE->setText(QString::number(max_safety_torque,'f',1));
                ui->inverters_widget->ui->RL_MAX_SAF_POWER->setText(QString::number(max_safety_power * 1000,'f',1));
                break;
            }
            case 3:{
                ui->inverters_widget->ui->RR_MAX_OP_POWER->setText(QString::number(max_op_power,'f',1));
                ui->RR_info_power->setText(QString::number(max_op_power,'f',1));
                ui->inverters_widget->ui->RR_MAX_OP_TORQUE->setText(QString::number(max_op_torque,'f',1));
                ui->RR_info_torque->setText(QString::number(max_op_torque,'f',1));
                ui->inverters_widget->ui->RR_MAX_SAF_TORQUE->setText(QString::number(max_safety_torque,'f',1));
                ui->inverters_widget->ui->RR_MAX_SAF_POWER->setText(QString::number(max_safety_power * 1000,'f',1));
                break;
            }
            }
            return;
        }
        case MSG_ID_IIB_CAR_INFO:{
            IIB_CAR_info info;
            parse_IIB_car_info_message(msg.data, &info);
            ui->TE_PERCENTAGE->setText(QString::number(info.TE_percentage));
            ui->BMS_voltage->setText(QString::number(info.BMS_voltage));

            ui->mode->setText(MODES[msg.data[2]]);
            ui->mode_info->setText(MODES[msg.data[2]]);

            if(info.status.TE_OK == 1){
                ui->TE_OK_LED->turnGreen();
            }
            else {
                ui->TE_OK_LED->turnOff();
            }
            if(info.status.ARM_OK == 1){
                ui->ARM_OK_LED->turnGreen();
            }
            else {
                ui->ARM_OK_LED->turnOff();
            }
            if(info.status.ARM_OK == 1){
                ui->ARM_OK_LED->turnGreen();
            }
            else {
                ui->ARM_OK_LED->turnOff();
            }
            if(info.status.SDC1 == 1){
                ui->SDC_1_LED->turnGreen();
            }
            else {
                ui->SDC_1_LED->turnOff();
            }
            if(info.status.SDC2 == 1){
                ui->SDC_2_LED->turnGreen();
            }
            else {
                ui->SDC_2_LED->turnOff();
            }
            if(info.status.CAR_RTD == 1){
                ui->CAR_RTD_LED->turnGreen();
            }
            else {
                ui->CAR_RTD_LED->turnOff();
            }
            if(info.status.INV_HV == 1){
                ui->INV_HV_LED->turnGreen();
            }
            else {
                ui->INV_HV_LED->turnOff();
            }
            return;
        }
        case MSG_ID_IIB_INFO:{
            IIB_info iib_info;
            parse_IIB_iib_info_message(msg.data, &iib_info);
            ui->air_temp->setText(QString::number(iib_info.air_temp));
            ui->fan_speed->setText(QString::number(iib_info.max_rpm));
            ui->rpm_info->setText(QString::number(iib_info.max_rpm));
            ui->iib_temp->setText(QString::number(iib_info.temp_2));
            if (iib_info.fan_on == 1){
                ui->fan_on_LED->turnGreen();
            }
            else {
                ui->fan_on_LED->turnOff();
            }
            if(iib_info.INV_RTD == 1){
                ui->iib_inv_rtd_led->turnGreen();
            }
            else {
                ui->iib_inv_rtd_led->turnOff();
            }
            if (iib_info.INV_ON == 1){
                ui->iib_inv_on_led->turnGreen();
            }
            else {
                ui->iib_inv_on_led->turnOff();
            }
            if (iib_info.CU_power_A == 1){
                ui->EFA_LED->turnGreen();
            }
            else {
                ui->EFA_LED->turnOff();
            }
            if (iib_info.CU_power_B == 1){
                ui->EFB_LED->turnGreen();
            }
            else {
                ui->EFB_LED->turnOff();
            }
            if (iib_info.REGEN_ON == 1){
                ui->REGEN_LED->turnGreen();
                regen_on = 1;
            }
            else{
                ui->REGEN_LED->turnOff();
                regen_on = 0;
            }
            return;
        }
        }
    }


    if(msg.dev_id==DEVICE_ID_ARM){
       /*
        switch(msg.msg_id){

        case MSG_ID_ARM_LIMS :
            ARM_lims lims;
            parse_ARM_lims_message(msg.data, &lims);

            if (current_mode == MODE_DIFFERENTIAL || current_mode == MODE_TRACTION_LIMIT){
                ui->current_lim_rpm->setText(QString::number(lims.lim_rpm));
                ui->current_lim_torque_r->setText(QString::number(lims.lim_torque_r));
                ui->current_lim_torque_f->setText(QString::number(lims.lim_torque_f));
            }
            return;

        case MSG_ID_ARM_CONTROL_INFO:
            ARM_control_info control_info;

            parse_ARM_control_info_message(msg.data, &control_info);

            ui->current_gain->setText(QString::number(control_info.gain));
            ui->current_tl->setText(QString::number(control_info.tl));

            current_mode = control_info.mode;

            if (current_mode == MODE_TRACTION_LIMIT)
                ui->current_mode->setText("TL");
            else if (current_mode == MODE_DIFFERENTIAL)
                ui->current_mode->setText("DIFF");
            else if (current_mode == MODE_NONE)
                ui->current_mode->setText("NO");
            else
                ui->current_mode->setText("??");

            return;
        }
        */
    }
}



void Inverters::on_pushButton_5_clicked()
{
    ui->inverters_widget->ui->Error_FL->setText(" ");
    ui->inverters_widget->ui->Error_FR->setText(" ");
    ui->inverters_widget->ui->Error_RL->setText(" ");
    ui->inverters_widget->ui->Error_RR->setText(" ");

}

void Inverters::timer_clear_inverters(){
    if (clear_inv_flag && !Inv_cleared){
        clear();
        clear_inv_flag = 0;
        Inv_cleared = 1;
    }
    else
        clear_inv_flag = 1;
    return;
}

void Inverters::clear(){
    ui->inverters_widget->clear();
    ui->iib_inv_1_led->turnOff();
    ui->iib_inv_2_led->turnOff();
    ui->iib_inv_3_led->turnOff();
    ui->iib_inv_4_led->turnOff();
    ui->EFA_LED->turnOff();
    ui->EFB_LED->turnOff();
    ui->IN_1_BE_1->turnOff();
    ui->IN_1_BE_2->turnOff();
    ui->IN_2_BE_1->turnOff();
    ui->IN_2_BE_2->turnOff();
    ui->IN_3_BE_1->turnOff();
    ui->IN_3_BE_2->turnOff();
    ui->IN_4_BE_1->turnOff();
    ui->IN_4_BE_2->turnOff();
    ui->iib_temp->clear();
    ui->REGEN_LED->turnOff();
    ui->SDC_1_LED->turnOff();
    ui->SDC_2_LED->turnOff();
    ui->TE_OK_LED->turnOff();
    ui->fan_speed->clear();
    ui->ARM_OK_LED->turnOff();
    ui->INV_HV_LED->turnOff();
    ui->fan_on_LED->turnOff();
    ui->BMS_voltage->clear();
    ui->CAR_RTD_LED->turnOff();
    ui->TE_PERCENTAGE->clear();
    ui->air_temp->clear();
    ui->iib_inv_rtd_led->turnOff();

}

void Inverters::on_rtd_button_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = DEVICE_ID_IIB;
    msg.candata.dlc = 8;
    msg.candata.data[0] = 2;
    msg.candata.data[1] = msg.candata.data[2] = msg.candata.data[3] = 0;
    send_to_CAN(msg);
    return;
}


void Inverters::on_set_mode_clicked()
{
    if(ui->modes_dropdown->currentIndex() != 0){
        CANmessage msg;
        msg.candata.dev_id = DEVICE_ID_INTERFACE;
        msg.candata.msg_id = 27;
        msg.candata.dlc = 8;
        msg.candata.data[0] = DEVICE_ID_IIB;
        msg.candata.data[1] = 15;
        msg.candata.data[2] = uint16_t (ui->modes_dropdown->currentIndex() - 1);
        msg.candata.data[3] = 0;
        send_to_CAN(msg);
    }
    return;
}

void Inverters::on_specific_val_textEdited(const QString &arg1)
{
    double current_number = arg1.toDouble();
    check_spec_val(current_number);
    return;
}

void Inverters::on_parameter_currentIndexChanged(int index)
{
    check_spec_val(ui->specific_val->text().toDouble());
}


void Inverters::check_spec_val(double val){
    double current_number = val;
    if(ui->parameter->currentText() == "POWER"){
        if(current_number> 35000){
            ui->specific_val->setText("35000");
        }
    }
    else if (ui->parameter->currentText() == "TORQUE") {
        if(current_number > 21000){
            ui->specific_val->setText("21000");
        }
    }

    return;
}

void Inverters::on_set_new_pow_tor_clicked()
{
    int component = 0;
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[2] = uint16_t (ui->specific_val->text().toInt());
    msg.candata.data[3] = 0;
    if(ui->parameter->currentIndex() == 0){
        return;
    }

    if((ui->component->currentIndex() > 0) && (ui->component->currentIndex() < 5)){
        if(ui->parameter->currentText() == "POWER")
            component = ui->component->currentIndex() + 8;
        else if (ui->parameter->currentText() == "TORQUE")
            component = ui->component->currentIndex() + 4;
        else if (ui->parameter->currentText() == "REGEN_PWR")
            component = ui->component->currentIndex() + 21;
        else if (ui->parameter->currentText() == "Regen Torque") {
            component = ui->component->currentIndex() + 16;
        }
        msg.candata.data[1] = uint16_t (component);
        emit send_to_CAN(msg);
        return;
    }
    else{
        if(ui->component->currentText() == "REAR"){
            if(ui->parameter->currentText() == "TORQUE"){
                msg.candata.data[1] = P_IIB_RL_MAX_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_TORQUE_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "POWER") {
                msg.candata.data[1] = P_IIB_RL_MAX_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_POWER_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "REGEN_PWR") {
				msg.candata.data[1] = P_IIB_RL_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);

				msg.candata.data[1] = P_IIB_RR_MAX_REGEN_POWER_OP;
				emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "Regen Torque") {
                msg.candata.data[1] = P_IIB_RL_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);
            }

        }
        else if(ui->component->currentText() == "FRONT"){
            if(ui->parameter->currentText() == "TORQUE"){
                msg.candata.data[1] = P_IIB_FL_MAX_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_TORQUE_OP;
            emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "POWER") {
                msg.candata.data[1] = P_IIB_FL_MAX_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_POWER_OP;
                emit send_to_CAN(msg);
            }

            else if (ui->parameter->currentText() == "REGEN_PWR") {
                msg.candata.data[1] = P_IIB_FL_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "Regen Torque") {
                msg.candata.data[1] = P_IIB_FL_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);
            }
        }
        else if(ui->component->currentText() == "ALL"){
            if(ui->parameter->currentText() == "TORQUE"){
               msg.candata.data[1] = P_IIB_FL_MAX_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RL_MAX_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_TORQUE_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "POWER") {
                msg.candata.data[1] = P_IIB_FL_MAX_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RL_MAX_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_POWER_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "REGEN_PWR") {
                msg.candata.data[1] = P_IIB_FL_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RL_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_REGEN_POWER_OP;
                emit send_to_CAN(msg);
            }
            else if (ui->parameter->currentText() == "Regen Torque"){
                msg.candata.data[1] = P_IIB_FL_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_FR_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RL_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);

                msg.candata.data[1] = P_IIB_RR_MAX_REGEN_TORQUE_OP;
                emit send_to_CAN(msg);
            }
        }
    }
    return;
}

void Inverters::on_Set_motor_speed_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[1] = DEVICE_ID_IIB;
    msg.candata.data[2] = uint16_t (ui->motor_speed->text().toInt());
    msg.candata.data[3] = 0;
    emit send_to_CAN(msg);
}

void Inverters::on_motor_speed_textChanged(const QString &arg1)
{
    int current_number = arg1.toInt();
    if(current_number > 20000){
        ui->motor_speed->setText("20000");
    }
    else if (current_number < 0) {
        ui->motor_speed->setText("0");
    }
    return;
}

void Inverters::on_set_regen_clicked()
{
    CANmessage msg;
    QRgb red = QRgb (0xFF0000);
    QRgb green = QRgb (0x00FF00);
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[1] = P_IIB_REGEN;
    msg.candata.data[2] = regen_on;
    msg.candata.data[3] = 0;
    emit send_to_CAN(msg);
}

void Inverters::on_debug_but_toggled(bool checked)
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[1] = P_IIB_DEBUG_MODE;
    msg.candata.data[2] = checked;
    msg.candata.data[3] = 0;
    if (checked){
        ui->debug_info->setText("ON");
    }
    else {
        ui->debug_info->setText("OFF");
    }
    return;
}

void Inverters::on_set_all_iib_clicked()
{
    if(ui->motor_speed->text() != ""){
        on_Set_motor_speed_clicked();
    }
    if (ui->specific_val->text() != "") {
        on_set_new_pow_tor_clicked();
    }
    if(ui->modes_dropdown->currentText() != ""){
        on_set_mode_clicked();
    }
    if (ui->torque_front_input->text() != "") {
        on_set_torque_front_clicked();
    }
    if (ui->torque_rear_input->text() != "") {
        on_set_torque_rear_clicked();
    }
    return;
}

void Inverters::on_set_torque_front_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[1] = P_IIB_FL_MAX_TORQUE_OP;
    msg.candata.data[2] = uint16_t (ui->torque_front_input->text().toDouble());
    msg.candata.data[3] = 0;
    emit send_to_CAN(msg);
    msg.candata.data[1] = P_IIB_FR_MAX_TORQUE_OP;
    emit send_to_CAN(msg);
}

void Inverters::on_set_torque_rear_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 8;
    msg.candata.data[0] = DEVICE_ID_IIB;
    msg.candata.data[1] = P_IIB_RL_MAX_TORQUE_OP;
    msg.candata.data[2] = uint16_t (ui->torque_rear_input->text().toDouble());
    msg.candata.data[3] = 0;
    emit send_to_CAN(msg);
    msg.candata.data[1] = P_IIB_RR_MAX_TORQUE_OP;
    emit send_to_CAN(msg);
}

void Inverters::on_save_button_clicked()
{
    CANmessage msg;
    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_COMMON_SET;
    msg.candata.dlc = 2;
    msg.candata.data[0] = DEVICE_ID_IIB;
    emit send_to_CAN(msg);
}


void Inverters::save_settings(){
    manager->add("Component",ui->component->currentIndex());
    manager->add("Modes", ui->modes_dropdown->currentIndex());
    manager->add("Parameter", ui->parameter->currentIndex());
    manager->add("Motor Speed", ui->motor_speed->text().toInt());
    manager->add("Torque Rear", ui->torque_rear_input->text().toInt());
    manager->add("Torque Front", ui->torque_front_input->text().toInt());
    manager->add("Power_Torque", ui->set_new_pow_tor->text().toInt());
    manager->save(NAME, specific);
}

void Inverters::load_settings(){
    QSettings settings(m_sSettingsFile, QSettings::NativeFormat);
    ui->component->setCurrentIndex(settings.value("Component", "").toInt());
    ui->modes_dropdown->setCurrentIndex(settings.value("Modes", "").toInt());
    ui->parameter->setCurrentIndex(settings.value("Parameter", "").toInt());

    ui->specific_val->setText((settings.value("Power_Torque", "")).toString());
    ui->torque_front_input->setText((settings.value("Torque Front", "")).toString());
    ui->torque_rear_input->setText((settings.value("Torque Rear", "")).toString());
    ui->motor_speed->setText((settings.value("Motor Speed", "")).toString());

}

void Inverters::on_save_clicked()
{
    save_settings();
}

void Inverters::on_load_clicked()
{
    load_settings();
}

