#ifndef ID_LIST_H
#define ID_LIST_H

#include <QDialog>

namespace Ui {
class IDList;
}

class IDList : public QDialog
{
    Q_OBJECT

public:
    explicit IDList(QWidget *parent = 0);
    ~IDList();
    void toTop();
    void update();

private:
    Ui::IDList *ui;
    void changeEvent(QEvent *event);


signals:


};

#endif
