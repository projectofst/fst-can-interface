#include "src/Console_idlist.hpp"
#include "ui_Console_idlist.h"

#include <QDebug>
#include <QTextEdit>
#include <QFile>
#include <QMessageBox>
#include <QScrollArea>
#include <QScrollBar>


IDList::IDList(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::IDList)
{
    ui->setupUi(this);


    this->setWindowTitle("CAN IDs List");
    this->setFixedHeight(700);
    this->setFixedWidth(600);
    QFile file(":/ids/table.txt");
    if(!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(this, "Fatal error", "table.txt" + file.errorString());
        exit(1);
    }

    QTextStream in(&file);

    while(!in.atEnd()) {
        QString line = in.readLine();
        ui->can_list->append(line);
    }

    file.close();


}

void IDList::changeEvent(QEvent *event)
{
	return;
}

IDList::~IDList()
{
    delete ui;
}

void IDList::toTop() {
    this->ui->can_list->verticalScrollBar()->setValue(0);
}

void IDList::update() {
    ui->can_list->clear();
    QFile file(":/ids/table.txt");
        if(!file.open(QIODevice::ReadOnly)) {
            QMessageBox::critical(this, "Fatal error", "can-ids/table.txt: " + file.errorString());
            exit(1);
        }

        QTextStream in(&file);

        while(!in.atEnd()) {
            QString line = in.readLine();
            ui->can_list->append(line);
        }

        file.close();
}


