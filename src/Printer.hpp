#ifndef PRINTER_H
#define PRINTER_H


#include <QWidget>

namespace Ui {
    class Printer;
}


class Printer : public QWidget
{
    Q_OBJECT

public:
    explicit Printer(QWidget *parent = nullptr);
    ~Printer();
    void append_string(QString string);




private slots:

    void on_pushButton_clicked();

    void on_button_send_clicked();

private:
    Ui::Printer *ui;

    // TODO: pls no typedef struct

signals:
    void send_serial(QString string);

};


#endif // PRINTER_H
