#ifndef SENSORS_H
#define SENSORS_H

#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "can-ids/Devices/WATER_TEMP_CAN.h"

#include "UiWindow.hpp"


namespace Ui {
class Sensors;
}

class Sensors : public UiWindow
{
    Q_OBJECT

public:
    explicit Sensors(QWidget *parent = nullptr);
    ~Sensors() override;
    void updateUI(void);
    //void updateSupotsUI(SUPOT_MSG_SIG supots_values, WT_Data water_temp);

private slots:

    void process_CAN_message(CANmessage msg) override;


private:
    Ui::Sensors *ui;

signals:
    void send_to_CAN(CANmessage) override;

};



#endif // SENSORS_H
