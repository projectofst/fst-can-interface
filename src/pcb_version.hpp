#ifndef PCB_VERSION_HPP
#define PCB_VERSION_HPP

#include <QWidget>

namespace Ui {
class pcb_version;
}

class pcb_version : public QWidget
{
    Q_OBJECT

public:
    explicit pcb_version(QWidget *parent = nullptr);
    ~pcb_version();
    Ui::pcb_version *ui;
    QString get_name();
    uint64_t get_current_version();
    QString get_expected_version();
    void set_name(QString new_name);
    void set_current_version(uint64_t version, int byte);
    void set_expected_version(QString version);
    uint64_t cast_versions();
    void destroy();
private:
    QString name;
    QVector<uint64_t> current_version;
    QString expected_version;
};

#endif // PCB_VERSION_HPP
