#ifndef DRS_CAL_HPP
#define DRS_CAL_HPP

#include <QWidget>
#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"

#define DRS_1 1
#define DRS_2 2
#define P_DRS_MIN_ANGLE_CALI    0
#define P_DRS_MAX_ANGLE_CALI    1
#define P_DRS_MIN_ANGLE_FINAL   2
#define P_DRS_MAX_ANGLE_FINAL   3
namespace Ui {
class drs_cal;
}

class drs_cal : public UiWindow
{
    Q_OBJECT

public:
    explicit drs_cal(QWidget *parent = nullptr);
    ~drs_cal();

private slots:

    void send_DRS_can_msg(int parameter, int value_to_send);

    void reset_values();

    void on_send_clicked();

    void on_inc_clicked();

    void on_dec_clicked();

    void on_DRS_number_currentIndexChanged(const QString &arg1);

private:
    Ui::drs_cal *ui;
    int min;
    int max;
    int curr_value;
signals:
    void send_to_CAN(CANmessage msg);

};

#endif // DRS_CAL_HPP
