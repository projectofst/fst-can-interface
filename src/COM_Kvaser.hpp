#ifndef COM_KVASER_H
#define COM_KVASER_H

#include <QObject>
#include <QSerialPort>
#include <QTcpSocket>
#include <QTcpServer>
#include <QtNetwork>
#include <QNetworkSession>
#include <QTimer>
#include <QFile>
#include <QMessageBox>
#include <QTextStream>
#include <queue>
#include <thread>
#include <mutex>
#include <QMutex>
#include <QUdpSocket>
#include <QCanBus>

#include "COM.hpp"

#include "can-ids/CAN_IDs.h"

namespace Ui {
    class COM_Kvaser;
}

class COM_Kvaser : public COM
{
    Q_OBJECT

public:
    COM_Kvaser();
    int Status;
    QStringList options(void);
    bool open(QString);
    int close(void);

    CANmessage pop(void);
    int status(void);

public slots:
    virtual void read(void);
    int send(CANmessage);

private:
    QMap<QString,COM*> Interfaces;
    QMap<QString,COM*> Loggers;
    QCanBusDevice *device1;
    QCanBusDevice *device2;
    QQueue<CANmessage> *fifo;
    
signals:
    void new_messages(CANmessage);
    void broadcast_in_message(CANmessage);
};

#endif // COM_H
