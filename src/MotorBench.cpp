#include "MotorBench.hpp"
#include "ui_MotorBench.h"

#include <QTextEdit>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>

#define MAX_SPEED_RPM 21000 //rpm
#define MAX_TORQUE 21 //N/m

TestBench::TestBench(QWidget *parent) : UiWindow(parent) , ui(new Ui::TestBench)
{
    ui->setupUi(this);
    parameters = (int**)malloc(4*sizeof(int*));

    for(int i=0; i<4; i++){
        parameters[i]= (int*)malloc(3*sizeof(int));
    }

    ui->real_speed_1->setText(QString :: number(0));
    ui->real_t_p_1->setText(QString :: number(0));
    ui->real_t_n_1->setText(QString :: number(0));
    ui->real_speed_2->setText(QString :: number(0));
    ui->real_t_p_2->setText(QString :: number(0));
    ui->real_t_n_2->setText(QString :: number(0));
    ui->real_speed_3->setText(QString :: number(0));
    ui->real_t_p_3->setText(QString :: number(0));
    ui->real_t_n_3->setText(QString :: number(0));
    ui->real_speed_4->setText(QString :: number(0));
    ui->real_t_p_4->setText(QString :: number(0));
    ui->real_t_n_4->setText(QString :: number(0));

}

TestBench::~TestBench()
{
    delete ui;
}

void TestBench::process_CAN_message(CANmessage) {}

void TestBench::on_load_file_clicked()
{

    printf("load file\n");

    file_name_1 = QFileDialog::getOpenFileName(this, "Select a .txt file", "C://");

    if(file_name_1.contains(".txt", Qt::CaseSensitive)){
       ui->messages->setText("File open succefully!\n");
       ui->file_name->setText(file_name_1);
    }else{
        ui->messages->setText("File is not .txt form!");
        file_name_1 = "0";
        printf("nao tem txt\n");
    }

    return;
}

void TestBench::on_send_file_clicked()
{
    printf("send file\n");

    QFile file(file_name_1);

    int x = QString::compare(file_name_1, "0", Qt::CaseSensitive);

    if(!file.open(QIODevice::WriteOnly) || x==0){
        QMessageBox::information(nullptr, "error", file.errorString());
        ui->messages->setText("Could not run file\n");
    }

    QTextStream i(&file);

    while(!i.atEnd()){

        //

    }

    file.close();

    return;
}

void TestBench::on_send_values_clicked()
{


    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 3; j++){

            parameters[i][j] = 0;
        }
    }

    printf("send values\n");

    parameters[0][0] = ui->speed_1->toPlainText().toInt();
    parameters[0][1] = ui->torque_p_1->toPlainText().toInt();
    parameters[0][2] = ui->torque_n_1->toPlainText().toInt();
    parameters[1][0] = ui->speed_2->toPlainText().toInt();
    parameters[1][1] = ui->torque_p_2->toPlainText().toInt();
    parameters[1][2] = ui->torque_n_2->toPlainText().toInt();
    parameters[2][0] = ui->speed_3->toPlainText().toInt();
    parameters[2][1] = ui->torque_p_3->toPlainText().toInt();
    parameters[2][2] = ui->torque_n_3->toPlainText().toInt();
    parameters[3][0] = ui->speed_4->toPlainText().toInt();
    parameters[3][1] = ui->torque_p_4->toPlainText().toInt();
    parameters[3][2] = ui->torque_n_4->toPlainText().toInt();

    //Ver se é gen ou motor

    if(ui->motor_1->isChecked()){
        parameters[0][2] = 0;
        parameters[0][0] = MAX_SPEED_RPM;
    }
    if(ui->gen_1->isChecked()){
        parameters[0][1] = 0;
        parameters[0][2] = MAX_TORQUE;
    }

    if(ui->motor_2->isChecked()){
        parameters[1][2] = 0;
        parameters[1][0] = MAX_SPEED_RPM;
    }
    if(ui->gen_2->isChecked()){
        parameters[1][1] = 0;
        parameters[1][2] = MAX_TORQUE;
    }

    if(ui->motor_3->isChecked()){
        parameters[2][2] = 0;
        parameters[2][0] = MAX_SPEED_RPM;
    }
    if(ui->gen_3->isChecked()){
        parameters[2][1] = 0;
        parameters[2][2] = MAX_TORQUE;
    }

    if(ui->motor_4->isChecked()){
        parameters[3][2] = 0;
        parameters[3][0] = MAX_SPEED_RPM;
    }
    if(ui->gen_4->isChecked()){
        parameters[3][1] = 0;
        parameters[3][2] = MAX_TORQUE;
    }


    for(int i = 0; i < 4; i++){
        for(int j = 0; j < 3; j++){

            printf("%d ", parameters[i][j]);
        }
        printf("\n");
    }


    if(ui->motor_1->isChecked() || ui->motor_2->isChecked() || ui->motor_3->isChecked() || ui->motor_4->isChecked()){

        if(ui->gen_1->isChecked() || ui->gen_2->isChecked() || ui->gen_3->isChecked() || ui->gen_4->isChecked()){

        }else{
           ui->messages->setText("Please select a generator\n");
           return;
        }
    }

    if(ui->gen_1->isChecked() || ui->gen_2->isChecked() || ui->gen_3->isChecked() || ui->gen_4->isChecked()){

        if(ui->motor_1->isChecked() || ui->motor_2->isChecked() || ui->motor_3->isChecked() || ui->motor_4->isChecked()){

        }else{
            ui->messages->setText("Please select a motor\n");
            return;
        }
    }


    if(ui->motor_1->isChecked() && ui->gen_1->isChecked()){
        ui->messages->setText("ERROR 1! Can't choose the same amk as Motor and Generator\n");
        return;
    }
    if(ui->motor_2->isChecked() && ui->gen_2->isChecked()){
        ui->messages->setText("ERROR 2! Can't choose the same amk as Motor and Generator\n");
        return;
    }
    if(ui->motor_3->isChecked() && ui->gen_3->isChecked()){
        ui->messages->setText("ERROR 3! Can't choose the same amk as Motor and Generator\n");
        return;
    }
    if(ui->motor_4->isChecked() && ui->gen_4->isChecked()){
        ui->messages->setText("ERROR 4! Can't choose the same amk as Motor and Generator\n");
        return;
    }


    ui->real_speed_1->setText(QString :: number(parameters[0][0]));
    ui->real_t_p_1->setText(QString :: number(parameters[0][1]));
    ui->real_t_n_1->setText(QString :: number(parameters[0][2]));
    ui->real_speed_2->setText(QString :: number(parameters[1][0]));
    ui->real_t_p_2->setText(QString :: number(parameters[1][1]));
    ui->real_t_n_2->setText(QString :: number(parameters[1][2]));
    ui->real_speed_3->setText(QString :: number(parameters[2][0]));
    ui->real_t_p_3->setText(QString :: number(parameters[2][1]));
    ui->real_t_n_3->setText(QString :: number(parameters[2][2]));
    ui->real_speed_4->setText(QString :: number(parameters[3][0]));
    ui->real_t_p_4->setText(QString :: number(parameters[3][1]));
    ui->real_t_n_4->setText(QString :: number(parameters[3][1]));



    CANmessage msg;

    //msg.msg_id = MSG_ID_ARM_SPEED;
    msg.candata.dev_id = DEVICE_ID_ARM;

    //speeds to arm

    msg.candata.data[0] = static_cast<uint16_t>(parameters[0][0]);
    msg.candata.data[1] = static_cast<uint16_t>(parameters[1][0]);
    msg.candata.data[2] = static_cast<uint16_t>(parameters[2][0]);
    msg.candata.data[3] = static_cast<uint16_t>(parameters[3][0]);

    emit send_to_CAN(msg);

    msg.candata.msg_id = 38;

    //positive torque to ARM

    int i;

    for(i = 0; i < 4; i++){

        if(parameters[i][1] < 0){
            msg.candata.data[i] = 0;
        }else{
            msg.candata.data[i] = static_cast<uint16_t>(parameters[i][1]);
        }
    }

    emit send_to_CAN(msg);


    //negative torque to ARM

    for(i = 0; i < 4; i++){

        if(parameters[i][2] > 0){
            msg.candata.data[i] = 0;
        }else{
            msg.candata.data[i] = static_cast<uint16_t>(parameters[i][2]);
        }
    }

    emit send_to_CAN(msg);

    return;
}

void TestBench::on_speed_1_copyAvailable(bool)
{

}

void TestBench::on_motor_1_clicked()
{
    printf("Motor 1\n");

    return;
}

void TestBench::on_gen_1_clicked()
{
    printf("Generator 1\n");

    return;

}
