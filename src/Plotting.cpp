﻿#include "Plotting.hpp"
#include "ui_Plotting.h"
#include "QTime"
#include <QtCharts>
#include <QtCharts/QChartView>
#include <QDir>
#include <QFileDialog>
#include "QPair"
#include "Resources/LEDwidget.hpp"
#include "Resources/plot_filter_widget.hpp"
#include "ui_plot_filter_widget.h"
#include "Plot_Callout.hpp"
#include <QtGui/QResizeEvent>
#include <QtWidgets/QGraphicsScene>
#include <QtCharts/QChart>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtWidgets/QGraphicsTextItem>
#include <QtGui/QMouseEvent>
#include <QtWidgets/QGraphicsItem>
#include <QRubberBand>
#include <QPrinter>
#include <QtMath>
#include <stdio.h>

QT_CHARTS_USE_NAMESPACE
plotting::plotting(QWidget *parent)
    : UiWindow(parent) ,
      ui(new Ui::plotting)
{
    ui->setupUi(this);

    get_messages();
    connect(ui->dev_id_box, &QComboBox::currentTextChanged, this, &plotting::update_msg_id_combo_box);
    ui->ChildrenLayout->setHidden(true);
    ui->SettingsLayout->setHidden(true);

    series = new QLineSeries;
    // Create chart, add data, hide legend, and add axis
    plot = new QChart();
    plot->addSeries(series);

    // Create axis
    axisX = new QValueAxis;
    axisY = new QValueAxis;
    axisY->setRange(0,10);


    plot->setAxisX(axisX, series);
    plot->setAxisY(axisY, series);

    // Used to display the chart
    ui->Plot->setChart(plot);
    ui->Plot->setRenderHint(QPainter::Antialiasing);

    m_coordX = new QGraphicsSimpleTextItem(plot);

    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update_plot()));
    timer->start(100);

    //initiating variables
    first_sample = 0;
    plot_number = 0;
    number_of_plots = 0;
    hold_plot = 0;
    h_x = h_y = 0;
    l_y = l_x = pow(2, 16);
    points_number = 0;

    //Color codes for the plots
    colour_map["Red"] = 0xFF0000;
    colour_map["Blue"] = 0x0000FF;
    colour_map["Green"] = 0x00FF00;
    colour_map["Black"] = 0x00000;
    colour_map["Pink"] = 0xFFC0CB;
    colour_map["Hot Pink"] = 0xFF69B4;
    colour_map["Yellow"] = 0xFFFF00;
    colour_map["Orange"] = 0xFF8000;
    colour_map["Cyan"] =  0x00FFFF;
    colour_map["Grey"] = 0x808080;

    //Shit so that clicks on plot work
    plot->setAcceptHoverEvents(true);
    ui->Plot->setMouseTracking(true);
    trigger= 0;

    //initialize the first callout to NULL so we can check later
    m_tooltip = nullptr;
    screen_shot_counter = 0;

    //Connects so that the method on the 4th place happens when you change the object on the first position
    connect(ui->dev_id_box, &QComboBox::currentTextChanged, this, &plotting::update_msg_id_combo_box);
#ifndef Q_OS_WIN
    connect(ui->adv_math_operation_dropdown, &QComboBox::currentTextChanged, this, &plotting::custom_op_line_edit);
#endif
}


void plotting::process_CAN_message(CANmessage message){
    QVector<int> plot_id = find_plot(message);
    //Only processes anything if it fins a match
    if (!plot_id.isEmpty()){
        save_can_data(message, plot_id);
    }
}

void plotting::send_to_CAN(CANmessage msg) {

}
#ifndef Q_OS_WIN
void plotting::custom_op_line_edit(){
    if(ui->adv_math_operation_dropdown->currentText() == "Custom Expression"){
        ui->math_custom_op_line_edit->setEnabled(true);
    }
    else {
        ui->math_custom_op_line_edit->setEnabled(false);
    }
}
#endif

plotting::~plotting()
{
    delete ui;
}

/**********************************************************************
     * Name:	on_clear_plot_clicked
     * Args:	-
     * Return:	-
     * Desc:	Clears all the samples from all the active plots.
     **********************************************************************/
void plotting::on_clear_plot_clicked()
{
    // Clears every plot
    for (int k = 0; k < plot_vec.size(); k++){
        //Reset cumulative parameters
        plot_vec[k].parent_1_prev_buffer_size = 0;
        plot_vec[k].parent_2_prev_buffer_size = 0;
        QPointF last = plot_vec[k].buffer.last();
        plot_vec[k].limits.low_x = plot_vec[k].limits.high_x = last.x();
        plot_vec[k].limits.low_y = plot_vec[k].limits.high_y = last.y();
        plot_vec[k].buffer.clear();
        plot_vec[k].integral_sample = 0;
        plot_vec[k].derivative_sample = 0;
        plot_vec[k].area = 0;
        plot_vec[k].sample_number = 0;
        plot_vec[k].average = 0;
        plot_vec[k].sum = 0;
        plot_vec[k].previous_timestamp = 0;
    }
    ui->message_output->setPlainText("");
    first_sample = 0;
    reset_axis();


}

/**********************************************************************
     * Name:	on_set_button_clicked
     * Args:	-
     * Return:	-
     * Desc:	Adjusts the numbber of samples on screen to the value introduced
     *          by the user.
     **********************************************************************/
void plotting::on_set_button_clicked()
{
    points_number = ui->values_edit->text().toDouble();
    ui->values_edit->setText(" ");
    reset_axis();
}

/**********************************************************************
     * Name:	on_clear_x_vals_clicked
     * Args:	-
     * Return:	-
     * Desc:	Resets the number of samples on the screen.
     **********************************************************************/
void plotting::on_clear_x_vals_clicked()
{
    points_number = 0;
}

/**********************************************************************
     * Name:	find_axisScale
     * Args:	QPointF, int
     * Return:	-
     * Desc:    Finds the values for used for axis adjustment.
     **********************************************************************/
void plotting::find_axisScale(QPointF point, int plot_id){
    plot_filter_widget *filter;
    filter = filter_vector[plot_id];
    if(point.y() < l_y)
        l_y = point.y();
    if(point.x() > h_x)
        h_x = point.x();
    if(point.y() > h_y)
        h_y = point.y();
    if (point.x() < l_x)
        l_x = point.x();
    if (point.x() < plot_vec[plot_id].limits.low_x)
        plot_vec[plot_id].limits.low_x = point.x();
    if (point.y() < plot_vec[plot_id].limits.low_y){
        plot_vec[plot_id].limits.low_y = point.y();
        char set_min[64];
        sprintf(set_min, "%.2f", plot_vec[plot_id].limits.low_y);
        filter->ui->write_min->setText(set_min);
    }
    if (point.x() > plot_vec[plot_id].limits.high_x)
        plot_vec[plot_id].limits.high_x = point.x();
    if (point.y() > plot_vec[plot_id].limits.high_y){
        plot_vec[plot_id].limits.high_y = point.y();
        char set_max[64];
        sprintf(set_max, "%.2f", plot_vec[plot_id].limits.high_y);
        filter->ui->write_max->setText(set_max);
    }
}

/**********************************************************************
     * Name:	adjust_axis
     * Args:	-
     * Return:	-
     * Desc:	Adjusts the axis to fit the plot.
     **********************************************************************/
void plotting::adjust_axis(){
    double curr_h_x, curr_h_y, curr_l_x, curr_l_y;
    curr_h_x = curr_h_y = 10;
    curr_l_x = curr_l_y = -10;
    bool found_active_plot = false;
    //We only want te axis to adjust to the plots that are currently being shown on screen
    for (int k = 0; k < plot_vec.size(); k++){
        if(filter_vector[k]->active){
            if (!found_active_plot){
                //If plot[k] is the first active one, its values are set as the highest/lowest
                curr_h_x = plot_vec[k].limits.high_x;
                curr_l_x = plot_vec[k].limits.low_x;
                curr_l_y = plot_vec[k].limits.low_y;
                curr_h_y = plot_vec[k].limits.high_y;
                found_active_plot = true; //change this to true,because an active plot was found
            }
            else if (found_active_plot){
                if (plot_vec[k].limits.high_x > curr_h_x){ // For every subsequent active plot, compare with the previous highest/lowest values. update accordingly
                    curr_h_x = plot_vec[k].limits.high_x;
                }
                else if (plot_vec[k].limits.high_y > curr_h_y){
                    curr_h_y= plot_vec[k].limits.high_y;
                }
                else if (plot_vec[k].limits.low_x < curr_l_x){
                    curr_l_x = plot_vec[k].limits.low_x;
                }
                else if (plot_vec[k].limits.low_y < curr_l_y){
                    curr_l_y = plot_vec[k].limits.low_y;
                }
            }
        }
    }

    //Update the axis according to the value found previously
    if (points_number != 0){
        axisX->setRange(curr_h_x-points_number, curr_h_x+5);
        axisY->setRange(curr_l_y-5, curr_h_y+5);
    }
    else{
        axisX->setRange(curr_l_x-10, curr_h_x+10);
        axisY->setRange(curr_l_y-10, curr_h_y+10);
    }
    return;
}

int plotting::find_pair(int parent_id, int child_id){
    //Goes through every plot to check if plot_vec[child_id] is a child of any plot
    for (int k = 0; k < plot_vec[child_id].math_parent.size(); k++){
        if (plot_vec[child_id].math_parent[k].first == parent_id || plot_vec[child_id].math_parent[k].second == parent_id){
            return k;
        }
    }
    return -1;
}

int64_t plotting::cast_message(CANmessage message){
    //Concatenates the 4 data bytes of a CANmessage data to a single 64 bit number, for processing purposes.
    auto data = message.candata.data;
    int64_t full_message = (int64_t(data[3]) << 48) + (int64_t(data[2]) << 32) + (data[1] << 16) + data[0];
    return full_message;
}
/**********************************************************************
     * Name:	save_can_data
     * Args:	CANmessage
     * Return:	-
     * Desc:    Saves the content of a can Messages, inserting it to the a queue
     *          specific to each plot
     **********************************************************************/
void plotting::save_can_data(CANmessage message, QVector<int> plot_ids)
{
    uint16_t inc_timestamp;
    for (int k = 0; k < plot_ids.size(); k++)
    {
        k = plot_ids[k];
        int64_t full_message_data = cast_message(message);
        //Data sent to the dataqueue is determined by the user with the offset and the length fields bound to each plot
        data.data = (full_message_data >> plot_vec[k].filter.offset) & ((1 << plot_vec[k].filter.length) - 1);
        message.timestamp = uint16_t(message.timestamp);

        inc_timestamp = (message.timestamp - plot_vec[k].previous_timestamp_aux) % 0xFFFF;

        qDebug() << "timestamp: "<< uint16_t(message.timestamp);
        qDebug() << "prev_timestamp_aux: " << plot_vec[k].previous_timestamp_aux;
        qDebug() << "inc_timestamp: " << inc_timestamp;


        plot_vec[k].previous_timestamp_aux = uint16_t(message.timestamp);

        data.time = plot_vec[k].previous_timestamp + inc_timestamp;

        plot_vec[k].previous_timestamp = data.time;
        if (plot_vec[k].operation == "None")
            plot_vec[plot_vec[k].filter.plot_id].dataqueue.enqueue(data);

        for (int j = 0; j < plot_vec[k].children.size(); j++){
            //Checks for children and  its type and acts accordingly
            if (plot_vec[plot_vec[k].children[j]].operation.contains("Math"))
                process_math_channels(&(plot_vec[k]));
            else{
                process_simple_math(&(plot_vec[plot_vec[k].children[j]]));
            }
        }
        if (first_sample == 0){
            l_x = data.time;
            l_y = data.data;
            h_x = 0;
            h_y = 0;
            reset_axis();
            first_sample = 1;
        }
    }
}

void plotting::process_math_channels(Plot_info *plot)
{
    //Cycle goes through every child of the given plot, to see if those children have children of their own.
    for (int j = 0; j < plot->children.size(); j++){
        if (plot_vec[plot->children[j]].operation.contains("Math")){
            if (!plot_vec[plot->children[j]].children.isEmpty() && (plot_vec[plot->children[j]].operation.contains("Math")))
                process_math_channels(&(plot_vec[plot->children[j]]));
            int parent_1 = plot_vec[plot->children[j]].math_parent.first().first;
            int parent_2 = plot_vec[plot->children[j]].math_parent.first().second;
            int parent_1_cont = plot_vec[plot->children[j]].parent_1_prev_buffer_size;
            int parent_2_cont = plot_vec[plot->children[j]].parent_2_prev_buffer_size;
            calc_next_math_point(plot->children[j], parent_1, parent_2, parent_1_cont, parent_2_cont);
        }
        else
            process_simple_math(&(plot_vec[plot->children[j]]));
    }
}
#ifndef Q_OS_WIN
double plotting::calc_custom_expression_result(QString in_expression, QVector<double> values){

    std::string expression =  in_expression.toUtf8().constData(); //QString to std::string
    symbol_table_t symbol_table;
    expression_t   exp;
    parser_t       parser;
    QVector<std::string> variables = {"x", "y", "z", "w", "v"};
    QVector<double> variable_values;
    for (int k = 0; k < values.size(); k++){
        symbol_table.add_variable(variables[k], values[k]); //Giving every variable a value
    }

    exp.register_symbol_table(symbol_table);
    parser.compile(expression, exp);

    double result = exp.value();

    return result;
}
#endif
void plotting::init_math_channel(int id){
    //If the plot_vec[id] has a parent, it comes through here. This method goes through every point of the parent plot and applies the given operation to that point
    if (plot_vec[id].operation.contains("Math")){
        //If the operation involves two parent plots, the operation is slightly more complicated...
        int parent_1 = plot_vec[id].math_parent.first().first;
        int parent_2 = plot_vec[id].math_parent.first().second; //First, we need to define the parent plots.
        if (!plot_vec[parent_1].buffer.isEmpty() && !plot_vec[parent_2].buffer.isEmpty()){ //You can only init a math plot if both have points
            int smaller_plot = parent_2; //Find smaller and larger plot, so that you know where to start adding the plots.
            int larger_plot =  parent_1;
            if (plot_vec[parent_1].buffer.first().x() > plot_vec[parent_2].buffer.first().x()){
                smaller_plot = parent_1;
                larger_plot = parent_2;
            }
            else {
                smaller_plot = parent_2;
                larger_plot = parent_1;
            }
            bool stop = true;
            int cont = 0;
            while (stop){
                if (plot_vec[larger_plot].buffer[cont].x() > plot_vec[smaller_plot].buffer.first().x()){
                    stop = false; //Go through the larger plot until one of its samples has an x value greater thanthe first point of the smaller plot. This is your starting position for the larger plot. For the smaller plot it is 0,obviously
                }
                else {
                    cont ++;
                }
            }
            int parent_1_cont;
            int parent_2_cont;
            if (larger_plot == parent_1){
                parent_1_cont = cont; //Set the starting point
                parent_2_cont = 0;
            }
            else {
                parent_2_cont = cont;
                parent_1_cont = 0;
            }
            calc_next_math_point(id, parent_1, parent_2, parent_1_cont, parent_2_cont);
        }
    }
    else{
        //For simple math, a simple for loop will do the trick. Simply just go through the parent's buffer and apply the specific operation
        double value_to_plot = 0;
        int parent = plot_vec[id].parent;
        QVector<double> custom_values;
        if (plot_vec[parent].buffer.size() > 2){
            plot_vec[id].integral_sample = 3;
        }
        if (plot_vec[parent].buffer.size() > 7){
            plot_vec[id].derivative_sample = 8;
        }
        for (int i = 0; i < plot_vec[parent].buffer.size(); i++){
            if (plot_vec[id].operation == "Addition"){
                value_to_plot = plot_vec[parent].buffer[i].y() + plot_vec[id].operation_value;
            }
            else if (plot_vec[id].operation == "Subtraction"){
                value_to_plot = plot_vec[parent].buffer[i].y() - plot_vec[id].operation_value;
            }
            else if (plot_vec[id].operation == "Multiplication"){
                value_to_plot = plot_vec[parent].buffer[i].y() * plot_vec[id].operation_value;
            }
            else if (plot_vec[id].operation == "Division"){
                value_to_plot = plot_vec[parent].buffer[i].y() / plot_vec[id].operation_value;
            }
            else if(plot_vec[id].operation == "Derivative"){
                if ((plot_vec[parent].buffer.size() > 7) && (plot_vec[id].derivative_sample < plot_vec[parent].buffer.size()-3)){
                    value_to_plot = derivative(&(plot_vec[parent]), &(plot_vec[id]));
                }
            }
            else if((plot_vec[id].operation == "Integral") && (plot_vec[id].integral_sample > 1) && (plot_vec[id].integral_sample < plot_vec[parent].buffer.size())){
                value_to_plot = integral(&(plot_vec[parent]), &(plot_vec[id]));
            }
            else if(plot_vec[id].operation == "Ln"){
                value_to_plot = qLn(plot_vec[parent].buffer[i].y());
            }
            else if(plot_vec[id].operation == "sqrt"){
                value_to_plot = qSqrt(plot_vec[parent].buffer[i].y());
            }
            else if(plot_vec[id].operation == "pow"){
                value_to_plot = qPow(plot_vec[parent].buffer[i].y(), plot_vec[id].operation_value);
            }
#ifndef Q_OS_WIN
            else if(plot_vec[id].operation == "Custom Expression"){
                QString expression = plot_vec[id].custom_expression;
                custom_values = {plot_vec[parent].buffer[i].y()};
                value_to_plot = calc_custom_expression_result(expression,custom_values);
            }
#endif
            //Append to the plot's buffer and ajudst the axis
            plot_vec[id].buffer.append(QPointF(plot_vec[parent].buffer[i].x(), value_to_plot));

            plot_vec[id].parent_1_prev_buffer_size = plot_vec[parent].buffer.size() - 1;
            plot_vec[id].parent_2_prev_buffer_size = plot_vec[parent].buffer.size() - 1;
            find_axisScale(QPointF(plot_vec[parent].buffer[i].x(), value_to_plot), id);
            adjust_axis();
        }
    }
    plot_vec[id].line_series->replace(plot_vec[id].buffer);
}

void plotting::calc_next_math_point(int id, int parent_1, int parent_2, int parent_1_cont, int parent_2_cont){
    double value_to_plot = 0;
    double x = 0;
    double y = 0;
    double b = 0;
    double m = 0;
    int flag = 0;
    QVector<double> custom_values;
    while ( (parent_1_cont + 1 < plot_vec[parent_1].buffer.size()) && (parent_2_cont + 1 < plot_vec[parent_2].buffer.size())){
        if (plot_vec[parent_1].buffer[parent_1_cont].x() < plot_vec[parent_2].buffer[parent_2_cont].x()){
            x = plot_vec[parent_2].buffer[parent_2_cont].x();
            m = ((plot_vec[parent_1].buffer[parent_1_cont + 1].y() - plot_vec[parent_1].buffer[parent_1_cont].y()) / (plot_vec[parent_1].buffer[parent_1_cont+1].x() - plot_vec[parent_1].buffer[parent_1_cont].x()));
            b = plot_vec[parent_1].buffer[parent_1_cont].y() - m * plot_vec[parent_1].buffer[parent_1_cont].x();
            y = (m * x) + b;
            parent_1_cont ++;
            flag = 2;
        }
        else if (plot_vec[parent_1].buffer[parent_1_cont].x() > plot_vec[parent_2].buffer[parent_2_cont].x()){
            x = plot_vec[parent_1].buffer[parent_1_cont].x();
            m = ((plot_vec[parent_2].buffer[parent_2_cont + 1].y() - plot_vec[parent_2].buffer[parent_2_cont].y()) / (plot_vec[parent_2].buffer[parent_2_cont+1].x() - plot_vec[parent_2].buffer[parent_2_cont].x()));
            b = plot_vec[parent_2].buffer[parent_2_cont].y() - (m * plot_vec[parent_2].buffer[parent_2_cont].x());
            y = ((m * x) + b);
            parent_2_cont ++;
            flag = 1;
        }
        else {
            x = plot_vec[parent_1].buffer[parent_1_cont].x();
            y = plot_vec[parent_1].buffer[parent_1_cont].y();
            parent_1_cont ++;
            parent_2_cont ++;
            flag = 0;
        }

        int chosen_parent = 0;
        int chosen_parent_cont = 0;

        if (flag == 1){
            chosen_parent = parent_1;
            chosen_parent_cont = parent_1_cont;
        }
        else if ((flag == 2) || (flag == 0)){
            chosen_parent = parent_2;
            chosen_parent_cont = parent_2_cont;
        }
        if (plot_vec[id].operation == "Math+")
            value_to_plot =  y + plot_vec[chosen_parent].buffer[chosen_parent_cont].y();
        else if (plot_vec[id].operation == "Math-"){
            if (flag == 1)
                value_to_plot =  plot_vec[chosen_parent].buffer[chosen_parent_cont].y() - y;
            else
                value_to_plot =  y - plot_vec[chosen_parent].buffer[chosen_parent_cont].y();
        }
        else if (plot_vec[id].operation == "Mathx")
            value_to_plot =  y * plot_vec[chosen_parent].buffer[chosen_parent_cont].y();
        else if ((plot_vec[id].operation == "Math/")){
            if (flag == 1)
                value_to_plot =  plot_vec[chosen_parent].buffer[chosen_parent_cont].y() / y;
            else
                value_to_plot =  y / plot_vec[chosen_parent].buffer[chosen_parent_cont].y();
        }
        else if(plot_vec[id].operation == "Addition")
            value_to_plot = y + plot_vec[id].operation_value;

        else if(plot_vec[id].operation == "Subtraction")
            value_to_plot = y - plot_vec[id].operation_value;

        else if(plot_vec[id].operation == "Division")
            value_to_plot = y / plot_vec[id].operation_value;

        else if(plot_vec[id].operation == "Multiplication")
            value_to_plot = y * plot_vec[id].operation_value;

        else if(plot_vec[id].operation == "MathXY"){
            if (flag == 1){
                x = plot_vec[parent_1].buffer[parent_1_cont].y();
                value_to_plot = y;
            }
            else if (flag == 2){
                x = y;
                value_to_plot = plot_vec[parent_2].buffer[parent_2_cont].y();
            }
            else {
                x = plot_vec[parent_1].buffer[parent_1_cont].y();
                value_to_plot = plot_vec[parent_2].buffer[parent_2_cont].y();
            }
        }

        else if(plot_vec[id].operation == "Derivative"){
            if ((plot_vec[parent_1].buffer.size() > 7) && (plot_vec[id].derivative_sample < plot_vec[parent_1].buffer.size()-3)){
                x = plot_vec[parent_1].buffer[parent_1_cont].x();
                value_to_plot = derivative(&(plot_vec[parent_1]), &(plot_vec[id]));
            }
        }
        else if (plot_vec[id].operation == "Integral") {
            if((plot_vec[id].integral_sample >= 2) && (plot_vec[id].integral_sample < plot_vec[parent_1].buffer.size())){
                value_to_plot = integral(&(plot_vec[parent_1]), &(plot_vec[id]));
            }
            else if (plot_vec[parent_1].buffer.size() > 1) {
                plot_vec[id].integral_sample = 2;
            }
        }
        else if(plot_vec[id].operation == "Ln"){
            value_to_plot = qLn(y);
        }
        else if(plot_vec[id].operation == "sqrt"){
            value_to_plot =  qSqrt(y);
        }
        else if(plot_vec[id].operation == "pow"){
            value_to_plot =  qPow(y, plot_vec[id].operation_value);
        }
#ifndef Q_OS_WIN
        else if(plot_vec[id].operation == "MathCustom Expression" || plot_vec[id].operation == "Custom Expression"){
            QString expression = plot_vec[id].custom_expression;
            if (plot_vec[id].parent != -1)
                custom_values = {y};
            else {
                if (flag == 1)
                    custom_values = {plot_vec[parent_1].buffer[parent_1_cont-1].y(), y};
                else if(flag == 2)
                    custom_values = {y, plot_vec[parent_2].buffer[parent_2_cont-1].y()};
                else if(flag == 0){
                    custom_values = {y, plot_vec[parent_2].buffer[parent_2_cont-1].y()};
                }
            }
            value_to_plot = calc_custom_expression_result(expression, custom_values);
        }
#endif
        plot_vec[id].buffer.append(QPointF(x, value_to_plot));

        calc_average(value_to_plot, &(plot_vec[id]));
        if (hold_plot == 0){
            find_axisScale(QPointF(x, value_to_plot), id);
            if (filter_vector[id]->active)
                adjust_axis();
        }
        plot_vec[id].parent_1_prev_buffer_size = plot_vec[parent_1].buffer.size() - 1;
        plot_vec[id].parent_2_prev_buffer_size = plot_vec[parent_2].buffer.size() - 1;
    }

    return;
}
void plotting::process_simple_math(Plot_info *plot){
    for(int i = 0; i < plot->children.size(); i ++)
    {
        if (plot_vec[plot->children[i]].operation.contains("Math")){
            process_math_channels(&(plot_vec[plot->filter.plot_id]));
        }
        else{
            process_simple_math(&(plot_vec[plot->children[i]]));
        }
    }
    if (plot->parent != -1){
        int parent_1 = plot->parent;
        int parent_2 = parent_1;
        int parent_1_cont = plot->parent_1_prev_buffer_size;
        int parent_2_cont = parent_1_cont;
        calc_next_math_point(plot->filter.plot_id, parent_1, parent_2, parent_1_cont, parent_2_cont);
    }
}





/**********************************************************************
     * Name:	update_plot
     * Args:	-
     * Return:	-
     * Desc:    Updates all the active plots by dequeueing the information stashed
     *          on their queues.
     **********************************************************************/
void plotting::update_plot(){
    for (int k = 0; k < plot_vec.size(); k++)
    {
        while(!plot_vec[k].dataqueue.isEmpty())
        {
            Data data = plot_vec[k].dataqueue.dequeue();
            auto operation = plot_vec[k].operation;
            double value_to_plot = data.data;
            plot_vec[k].buffer.append(QPointF(data.time, value_to_plot));
            find_axisScale(QPointF(data.time, value_to_plot), k);
            calc_average(value_to_plot, &plot_vec[k]);
            if (hold_plot == 0)
            {
                if(filter_vector[k]->active)
                    adjust_axis();
            }
        }

        plot_filter_widget *filter_aux;
        for (int k = 0; (k < plot_vec.size()); k++){
            filter_aux = this->filter_vector.at(k);
            if (filter_aux->active){
                plot_vec[k].line_series->replace(plot_vec[k].buffer);
                for (int i = 0; i < plot_vec[k].m_callouts.size(); i++){
                    plot_vec[k].m_callouts[i]->show();
                }
            }
            else
            {
                plot_vec[k].line_series->replace(empty_vec);
                for (int i = 0; i < plot_vec[k].m_callouts.size(); i++)
                {
                    plot_vec[k].m_callouts[i]->hide();
                }
            }
        }
    }
}

void plotting::on_hold_plot_button_clicked(){
    if (ui->hold_plot_button->isChecked()){
        hold_plot = 1;
    }
    else {
        hold_plot = 0;
    }
}

void plotting::on_set_scale_button_clicked(){
    if (hold_plot == 1)
    {
        if (ui->min_scale_edit->text() != "")
            axisX->setRange(ui->min_scale_edit->text().toDouble(), ui->max_scale_edit->text().toDouble());
        if (ui->min_scale_edit_y->text() != "")
            axisY->setRange(ui->min_scale_edit_y->text().toDouble(), ui->max_scale_edit_y->text().toDouble());
        ui->max_scale_edit->setText("");
        ui->min_scale_edit->setText("");
        ui->max_scale_edit_y->setText("");
        ui->min_scale_edit_y->setText("");
    }
}

/**********************************************************************
     * Name:	find_plot
     * Args:	CANmessage
     * Return:	bool
     * Desc:    Returns boolean value of the existance of a plot that has matching
     *          parameters with the receieved CANMessage
     **********************************************************************/
QVector<int> plotting::find_plot(CANmessage message){
    QVector<int> plots_found;
    plots_found.empty();
    for (int k = 0; (k < plot_vec.size()); k++)
    {
        if ((plot_vec[k].filter.msg_id == message.candata.msg_id) && (plot_vec[k].filter.dev_id == message.candata.dev_id))
        {
            plots_found << k;
        }
    }
    return plots_found;
}

/**********************************************************************
     * Name:	find_filter
     * Args:	int, int, int
     * Return:	bool
     * Desc:
     **********************************************************************/
bool plotting::find_filter(int dev_id, int msg_id, int offset, int length){
    for (int k = 0; (k < plot_vec.size()); k++){
        if ((plot_vec[k].filter.msg_id == msg_id) && (plot_vec[k].filter.dev_id == dev_id) && (plot_vec[k].filter.offset == offset) && (plot_vec[k].filter.length == length)){
            return true;
        }
    }
    return false;
}

/**********************************************************************
     * Name:	set_new_plot
     * Args:	-
     * Return:	-
     * Desc:    Adds a new plot.
     **********************************************************************/
void plotting::set_new_plot(){
    /*Plot needs to be a seperate class*/
    plot_filter_widget *filter = new plot_filter_widget;
    filter_vector.append(filter);
    ui->filter_list_2->addWidget(filter);
    plot_info.filter.plot_id = number_of_plots;
    number_of_plots++;
    new_line_series = new QLineSeries;
    QPen pen((QRgb)colour_map["Black"]);
    pen.setWidth(3);
    new_line_series->setPen(pen);
    plot_info.line_series = new_line_series;
    plot->addSeries(plot_info.line_series);
    plot->setAxisX(axisX, plot_info.line_series);
    plot->setAxisY(axisY, plot_info.line_series);
    plot_number++;
    plot_info.limits.low_x = plot_info.limits.low_y = pow(2,16);
    plot_info.limits.high_x = plot_info.limits.high_y = 0;
    plot_info.parent = -1;
    plot_info.colour = -1;
    plot_info.previous_timestamp = 0;
    plot_info.previous_timestamp_aux = 0;
    plot_info.sample_number = 0;
    plot_info.operation = "None";
    plot_info.sum = 0;
    plot_info.parent_1_prev_buffer_size = 0;
    plot_info.parent_2_prev_buffer_size = 0;
    plot_info.m_callouts.empty();
    plot_info.integral_sample = 0;
    connect(plot_info.line_series, &QLineSeries::hovered, this, &plotting::tooltip);
    connect(plot_info.line_series, &QLineSeries::clicked, this, &plotting::keepCallout);
    plot_vec.append(plot_info);
    if (plot_vec.size() == 1)
        series->deleteLater();
    connect(filter->ui->delete_but, &QPushButton::clicked, this, [this, filter](){this->remove_line_series(filter);this->filter_vector.remove(this->filter_vector.indexOf(filter));delete filter;});
    connect(filter->ui->colour_menu, &QComboBox::currentTextChanged, this, [this](){this->on_refresh_colours();});
    connect(filter->ui->show_plot_check_box, &QCheckBox::stateChanged, this, [filter,this](const int state_changed){filter->active = state_changed ? 1:0; this->reset_axis();});
    find_next_plot_color(filter);
}
void plotting::find_next_plot_color(plot_filter_widget* filter){
    QMapIterator<QString, int> i(colour_map);
    QVector<QString> all_colors;
    for (int k = 0; k < colour_map.size(); k++){
        i.next();
        all_colors << i.key();
    }
    for (int k = 0; k < all_colors.size(); k++){
        if (!colour_vector.contains(all_colors[k])){
            filter->ui->colour_menu->setCurrentIndex(filter->ui->colour_menu->findText(all_colors[k]));
        }
    }
    on_refresh_colours();
}

/**********************************************************************
     * Name:	remove_line_series
     * Args:	plot_filter_widget
     * Return:	-
     * Desc:    Removes the selected plot.
     **********************************************************************/
void plotting::remove_line_series(plot_filter_widget *filter){
    if(plot_vec.size() == 1)
    {
        series = new QLineSeries;
        plot->setAxisX(axisX, series);
        plot->setAxisY(axisY, series);
        first_sample = 0;
        reset_axis();
    }
    auto plot_to_delete = plot_vec[filter_vector.indexOf(filter)];
    for(int k = 0; k < plot_to_delete.m_callouts.size(); k++)
    {
        auto callout = plot_to_delete.m_callouts.at(k);
        callout->destroy();
        plot_to_delete.m_callouts.removeAt(k);
        k = k - 1;
    }

    for (int k = 0; k < plot_vec.size(); k++)
    {
        for(int j = 0; j < plot_vec[k].children.size(); j++)
        {
            if(plot_vec[k].children[j] == plot_to_delete.filter.plot_id)
            {
                plot_vec[k].children.removeAt(j);
                j--;
            }
        }
    }
    for (int k = 0; k < plot_to_delete.children.size(); k++)
    {
        if (!(plot_vec[plot_to_delete.children[k]].math_parent.isEmpty()))
        {
            for (int j = 0; j < plot_vec[plot_to_delete.children[k]].math_parent.size(); j ++){
                if (plot_to_delete.filter.plot_id == plot_vec[plot_to_delete.children[k]].math_parent[j].first){
                    plot_vec[plot_vec[plot_to_delete.children[k]].math_parent[j].second].children.removeAt(plot_vec[plot_vec[plot_to_delete.children[k]].math_parent[j].second].children.indexOf(plot_vec[plot_to_delete.children[k]].filter.plot_id)); /*This is a mess*/
                    plot_vec[plot_to_delete.children[k]].math_parent.removeAt(j);
                    j --;
                }
                else if (plot_to_delete.filter.plot_id == plot_vec[plot_to_delete.children[k]].math_parent[j].second){
                    plot_vec[plot_vec[plot_to_delete.children[k]].math_parent[j].first].children.removeAt(plot_vec[plot_vec[plot_to_delete.children[k]].math_parent[j].first].children.indexOf(plot_vec[plot_to_delete.children[k]].filter.plot_id)); /*This is a mess*/
                    plot_vec[plot_to_delete.children[k]].math_parent.removeAt(j);
                    j --;
                }
            }
        }
    }
    auto colour = plot_to_delete.colour;
    if (ui->plot_box->findText(colour_map.key(colour)) != -1)
    {
        ui->plot_box->removeItem(ui->plot_box->findText(colour_map.key(colour)));
        ui->first_plot_dropdown->removeItem(ui->first_plot_dropdown->findText(colour_map.key(colour)));
        ui->second_plot_dropdown->removeItem(ui->second_plot_dropdown->findText(colour_map.key(colour)));
    }
    if (plot_to_delete.colour != -1)
    {
        colour_vector.remove(filter_vector.indexOf(filter));
    }
    plot->removeSeries(plot_to_delete.line_series);
    plot_vec.remove(filter_vector.indexOf(filter));
    for (int k = filter_vector.indexOf(filter); k < plot_vec.size(); k++)
    {
        plot_vec[k].filter.plot_id = plot_vec[k].filter.plot_id - 1;
        if (k == plot_vec[k].parent)
            plot_vec[k].parent = -1;

    }
    for (int k = 0; k < plot_vec.size(); k++){
        for (int j = 0; j < plot_vec[k].children.size(); j ++){
            if (plot_vec[k].children[j] > filter_vector.indexOf(filter))
                plot_vec[k].children[j] --;
        }
    }
    filter_vector[filter_vector.indexOf(filter)]->deleteLater();
    number_of_plots = number_of_plots - 1;
    reset_axis();
    update_callouts_plot_id();
}


/**********************************************************************
     * Name:	on_refresh_colours
     * Args:	-
     * Return:	-
     * Desc:    Updates the colours of every plot
     **********************************************************************/
void plotting::on_refresh_colours(){
    plot_filter_widget *filter_aux;
    for(int k = 0; (k < this->filter_vector.size()); k++){
        filter_aux = this->filter_vector.at(k);
        if(filter_aux->ui->colour_menu->currentText() != ""){
            auto colour = filter_aux->ui->colour_menu->currentText();
            if (colour_vector.indexOf(colour) == -1){
                plot_vec[k].colour = colour_map[colour];
                QPen pen(colour_map[colour]);
                pen.setWidth(3);
                plot_vec[k].line_series->setPen(pen);
            }
        }
    }
    colour_vector.clear();
    ui->plot_box->clear();
    ui->first_plot_dropdown->clear();
    ui->second_plot_dropdown->clear();
    for(int k = 0; (k < this->filter_vector.size()); k++){
        filter_aux = this->filter_vector.at(k);
        if (filter_aux->ui->colour_menu->currentText() != "" && colour_vector.indexOf(filter_aux->ui->colour_menu->currentText()) == -1){
            colour_vector << filter_aux->ui->colour_menu->currentText();
            ui->plot_box->addItem(filter_aux->ui->colour_menu->currentText());
            ui->first_plot_dropdown->addItem(filter_aux->ui->colour_menu->currentText());
            ui->second_plot_dropdown->addItem(filter_aux->ui->colour_menu->currentText());
        }
        else{
            filter_aux->ui->colour_menu->setCurrentIndex(0);
        }

    }
}

/**********************************************************************
     * Name:	reset_axis
     * Args:	-
     * Return:	-
     * Desc:    Readjusts the scale to fit the existing plots
     **********************************************************************/
void plotting::reset_axis(){
    if (plot_vec.size() != 0){
        if (filter_vector[0]->active){
            l_x = plot_vec[0].limits.low_x;
            l_y = plot_vec[0].limits.low_y;
            h_x = plot_vec[0].limits.high_x;
            h_y = plot_vec[0].limits.high_y;
        }
        for (int plot_number = 0; plot_number < plot_vec.size(); plot_number++)
        {
            if (filter_vector[plot_number]->active){
                if (plot_vec[plot_number].limits.low_x < l_x)
                    l_x = plot_vec[plot_number].limits.low_x;
                if (plot_vec[plot_number].limits.low_y < l_y)
                    l_y = plot_vec[plot_number].limits.low_y;
                if (plot_vec[plot_number].limits.high_x > h_x)
                    h_x = plot_vec[plot_number].limits.high_x;
                if (plot_vec[plot_number].limits.high_y > h_y)
                    h_y = plot_vec[plot_number].limits.high_y;
            }
        }
    }
    else
    {
        l_x = -10;
        h_x = 10;
        l_y = -5;
        h_y = 5;
    }
    adjust_axis();
}

/**********************************************************************
     * Name:	calc_average
     * Args:	int value, int plot_id
     * Return:	-
     * Desc:    Calculates the average of the values on a specific (plot_id) plot
     **********************************************************************/
void plotting::calc_average(double value, Plot_info* plot)
{
    plot->sum += value;
    plot->sample_number += 1;
    plot->average = plot->sum / plot->sample_number;
    plot_filter_widget *filter;
    filter = filter_vector[plot->filter.plot_id];
    char average[64];
    sprintf(average, "%0.2f", double (plot->average));
    filter->ui->write_avg->setText(average);
}

void plotting::on_test_but_clicked()
{

}

/**********************************************************************
    >>>>>>> develop
     * Name:	derivative
     * Args:	int parent, int sample
     * Return:	float
     * Desc:    Calculates derivative
     **********************************************************************/
double plotting::derivative(Plot_info *parent, Plot_info *plot){
    int sample = plot->derivative_sample;
    double f1 = parent->buffer[sample+1].y();
    double f2 = parent->buffer[sample+2].y();
    double f3 = parent->buffer[sample+3].y();
    double fm1 = parent->buffer[sample-1].y();
    double fm2 = parent->buffer[sample-2].y();
    double fm3 = parent->buffer[sample-3].y();
    double h = ( parent->buffer[sample+3].x() - parent->buffer[sample-3].x()) / 6;
    plot->derivative_sample ++;
    return (0.75 * (f1 - fm1) +  0.15 * (fm2-f2) +  0.0166 * (f3-fm3)) / h ;
}

/**********************************************************************
     * Name:	derivative
     * Args:	int parent, int sample
     * Return:	float
     * Desc:    Calculates integral
     **********************************************************************/
double plotting::integral(Plot_info *parent, Plot_info *plot){
    int sample = plot->integral_sample;
    double dif_x = parent->buffer[sample-2].x() - parent->buffer[sample-1].x();
    double soma_bases = parent->buffer[sample-2].y() + parent->buffer[sample-1].y();
    double area = qAbs((dif_x * soma_bases)/2);
    plot->area = plot->area + area;
    plot->integral_sample ++;
    return plot->area;
}

void plotting::on_hold_plot_button_toggled(bool checked)
{
    if (checked) {
        hold_plot = 1;
    } else {
        hold_plot = 0;
        ui->scaling_button->setChecked(false);
        h_x = data.time;
        adjust_axis();
    }
}

void plotting::on_values_edit_returnPressed()
{
    QRegExp re("\\d*");  // a digit (\d), zero or more times (*)
    if (!re.exactMatch(ui->values_edit->text()))
        ui->values_edit->clear();

    if (ui->values_edit->text() != "")
        points_number = ui->values_edit->text().toInt();

    else
        points_number = 0;

    reset_axis();
}

void plotting::on_scaling_button_toggled(bool checked)
{
    if (checked) {
        ui->Plot->setRubberBand(QChartView::RectangleRubberBand);
    } else {
        ui->Plot->setRubberBand(QChartView::NoRubberBand);
    }
}

void plotting::keepCallout()
{
    QLineSeries* serie = qobject_cast<QLineSeries *>(sender());
    for (int k = 0; k < plot_vec.size(); k++){
        if (plot_vec[k].line_series == serie){
            plot_vec[k].m_callouts << m_tooltip;
            m_tooltip->set_position(plot_vec[k].m_callouts.size());
            m_tooltip->set_plot_id(plot_vec[k].filter.plot_id);
            m_tooltip = new Callout(plot);
        }
    }

}

void plotting::update_callouts_plot_id()
{

    for(int k = 0; k <  plot_vec.size(); k++)
    {
        int plot_id = plot_vec[k].filter.plot_id;
        for (int i = 0; i < plot_vec[k].m_callouts.size(); i++)
        {
            Callout *callout = plot_vec[k].m_callouts.at(i);
            callout->set_plot_id(plot_id);
        }
    }
}



void plotting::tooltip(QPointF point, bool state)
{

    if (m_tooltip == nullptr){
        m_tooltip = new Callout(plot);
    }

    if (state) {
        m_tooltip->setText(QString("X: %1 \nY: %2 ").arg(point.x()).arg(point.y()));
        m_tooltip->setAnchor(point);
        m_tooltip->setZValue(11);
        m_tooltip->updateGeometry();
        m_tooltip->show();
    }
    else {
        m_tooltip->hide();
    }
}

void plotting::save_to_png()
{
    QString filePath = QFileDialog::getSaveFileName(this,
                                                    tr("Save"), QDir::currentPath(), tr("*.png"));
    QFile file(filePath);
    file.open(QIODevice::Append | QIODevice::Text);
    QPixmap image = ui->Plot->grab();
    bool saved = image.save(filePath, "PNG");
    QString save_msg;
    if (saved)
        save_msg = "Screenshot succesfully saved at: " + filePath;
    else
        save_msg = "Something went wrong, was not able to save the screenshot";
    ui->message_output->appendPlainText(save_msg);
}


void plotting::save_to_csv()
{
    if(plot_vec.size() != 0){
        QString filePath = QFileDialog::getSaveFileName(this,
                                                        tr("Save"), QDir::currentPath(), tr("CSV files (*.csv)"));
        QList<QString> file_path_list;
        QString actual_file_path;
        for (int k = 0 ; k < plot_vec.size(); k++) {
            file_path_list = filePath.split(".");
            if (file_path_list.size() == 2){
                actual_file_path = file_path_list[0] + "(" + (colour_map.key(plot_vec[k].colour)) + ", "
                        + QString::number(plot_vec[k].filter.dev_id) + ","
                        + QString::number(plot_vec[k].filter.msg_id) + ","
                        + QString::number(plot_vec[k].filter.offset) + ","
                        + QString::number(plot_vec[k].filter.length) + ")."
                        + file_path_list[1];

                QFile file(actual_file_path);
                file.open(QIODevice::Append | QIODevice::Text);
                QTextStream stream(&file);
                for (int i = 0; i < plot_vec[k].buffer.size(); ++i){
                    stream << plot_vec[k].buffer[i].x() << "," << plot_vec[k].buffer[i].y() << "\n";
                }
                stream.flush();
                file.close();
            }
            else {
                QString error = "Invalid name";
                ui->message_output->appendPlainText(error);
            }
        }
    }
}


void plotting::on_apply_adv_math_clicked()
#ifndef Q_OS_WIN
{
    if (ui->adv_math_operation_dropdown->currentText() == "Custom Expression" && !check_valid_expression(ui->math_custom_op_line_edit->text(), 2)){
        char error_msg[64];
        sprintf(error_msg, "Invalid expression.");
        ui->message_output->appendPlainText(error_msg);
    }
    else{
        plot_filter_widget *filter_aux;
        if ((ui->first_plot_dropdown->currentText() != "") && (ui->adv_math_operation_dropdown->currentText() != "")
                && (ui->second_plot_dropdown->currentText() != "")){

            auto plot_1_color = ui->first_plot_dropdown->currentText();
            auto plot_2_color = ui->second_plot_dropdown->currentText();
            set_new_plot();
            plot_vec.last().filter.offset = -1;
            plot_vec.last().filter.length = -1;
            plot_vec.last().filter.dev_id = -1;
            plot_vec.last().filter.msg_id = -1;
            int parent_1 = -1;
            int parent_2 = -1;
            for (int k = 0; k < plot_vec.size(); k++)
            {
                if (plot_vec[k].colour == colour_map[plot_1_color])
                {
                    parent_1 = k;
                    plot_vec[k].children << plot_vec.size() - 1;
                }
            }
            for(int k = 0; k < plot_vec.size(); k++){
                if (plot_vec[k].colour == colour_map[plot_2_color]){
                    parent_2 = k;
                    plot_vec[k].children << plot_vec.size() - 1;
                }
            }
            plot_vec.last().math_parent << QPair<int, int>(parent_1, parent_2);
            QString op = "Math";
            QString details;
            op.append(ui->adv_math_operation_dropdown->currentText());
            plot_vec.last().operation = op;
            details = filter_vector[parent_1]->ui->colour_menu->currentText() + ", ";
            details += filter_vector[parent_2]->ui->colour_menu->currentText() + ", ";
            if (ui->adv_math_operation_dropdown->currentText() == "Custom Expression"){
                plot_vec.last().custom_expression = ui->math_custom_op_line_edit->text();
                details += plot_vec.last().custom_expression;
            }
            else {
                details += ui->adv_math_operation_dropdown->currentText();
            }

            init_math_channel(plot_vec.last().filter.plot_id);
            filter_aux = this->filter_vector.at(plot_vec.last().filter.plot_id);
            filter_vector.last()->ui->plot_description->setText(details);
        }
    }
    return;
}
#else
{
    plot_filter_widget *filter_aux;
    if ((ui->first_plot_dropdown->currentText() != "") && (ui->adv_math_operation_dropdown->currentText() != "") && (ui->second_plot_dropdown->currentText() != "")){
        auto plot_1_color = ui->first_plot_dropdown->currentText();
        auto plot_2_color = ui->second_plot_dropdown->currentText();
        set_new_plot();
        plot_vec.last().filter.offset = -1;
        plot_vec.last().filter.length = -1;
        plot_vec.last().filter.dev_id = -1;
        plot_vec.last().filter.msg_id = -1;
        int parent_1 = -1;
        int parent_2 = -1;
        for (int k = 0; k < plot_vec.size(); k++)
        {
            if (plot_vec[k].colour == colour_map[plot_1_color])
            {
                parent_1 = k;
                plot_vec[k].children << plot_vec.size() - 1;
            }
        }
        for(int k = 0; k < plot_vec.size(); k++){
            if (plot_vec[k].colour == colour_map[plot_2_color]){
                parent_2 = k;
                plot_vec[k].children << plot_vec.size() - 1;
            }
        }
        plot_vec.last().math_parent << QPair(parent_1, parent_2);
        QString op = "Math";
        QString details;
        op.append(ui->adv_math_operation_dropdown->currentText());
        plot_vec.last().operation = op;
        details = filter_vector[parent_1]->ui->colour_menu->currentText() + ", ";
        details += filter_vector[parent_2]->ui->colour_menu->currentText() + ", ";
        if (ui->adv_math_operation_dropdown->currentText() == "Custom Expression"){
            plot_vec.last().custom_expression = ui->math_custom_op_line_edit->text();
            details += plot_vec.last().custom_expression;
        }
        else {
            details += ui->adv_math_operation_dropdown->currentText();
        }

        init_math_channel(plot_vec.last().filter.plot_id);
        filter_aux = this->filter_vector.at(plot_vec.last().filter.plot_id);
        filter_vector.last()->ui->plot_description->setText(details);
        return;
    }

}
#endif
#ifndef Q_OS_WIN
bool plotting::check_valid_expression(QString in_exp, int number_of_variables){
    std::string expression =  in_exp.toUtf8().constData(); //QString to std::string
    symbol_table_t symbol_table;
    expression_t   exp;
    parser_t       parser;
    QVector<std::string> variables = {"x", "y", "z", "w", "v"};
    double x = 10;
    for (int k = 0; k < number_of_variables; k++){
        symbol_table.add_variable(variables[k], x);
    }
    exp.register_symbol_table(symbol_table);
    parser.compile(expression, exp);

    double result = exp.value();

    if (!(result == result)) //nan expression have the characteristic that comparisons between them are always false, thank you Stack Overflow
        return false;
    else {
        return true;
    }
}
#endif
void plotting::on_Apply_simple_math_clicked()
{
    if ((ui->plot_box->currentText() != "") && (ui->operation_box->currentText() != ""))
    {
#ifndef Q_OS_WIN
        if (ui->operation_box->currentText() == "Custom Expression" && !check_valid_expression(ui->write_op_value->text(),1)){
            char error_msg[64];
            sprintf(error_msg, "Invalid expression.");
            ui->message_output->appendPlainText(error_msg);
        }
        else {
#endif
            auto colour = ui->plot_box->currentText();
            for (int k = 0; k < plot_vec.size(); k++)
            {
                if (plot_vec[k].colour == colour_map[colour])
                {
                    plot_vec[k].children << plot_vec.size();
                    set_new_plot();
                    plot_vec[plot_vec.size()-1].filter.msg_id = plot_vec[k].filter.msg_id;
                    plot_vec[plot_vec.size()-1].filter.dev_id = plot_vec[k].filter.dev_id;
                    plot_vec[plot_vec.size()-1].filter.offset = plot_vec[k].filter.offset;
                    plot_vec[plot_vec.size()-1].filter.length = plot_vec[k].filter.length;
                    plot_vec[plot_vec.size()-1].operation = ui->operation_box->currentText();
                    plot_vec[plot_vec.size()-1].parent = k;
                    QString details;
                    if (ui->write_op_value->text() ==  ""){
                        plot_vec[plot_vec.size()-1].operation_value = 0;
                        details = ui->operation_box->currentText() + ", ";
                    }
#ifndef Q_OS_WIN

                    else if(ui->operation_box->currentText() == "Custom Expression"){
                        plot_vec.last().custom_expression = ui->write_op_value->text();
                        details = plot_vec.last().custom_expression + ", ";
                    }
#endif
                    else{
                        plot_vec[plot_vec.size()-1].operation_value = ui->write_op_value->text().toDouble();
                        details = ui->operation_box->currentText() + ", " + QString::number(plot_vec[plot_vec.size()-1].operation_value) + ", ";
                    }
                    QString data = " x -> " +  colour_map.key(plot_vec[plot_vec.last().parent].colour);
                    details += data + ".";
                    filter_vector.last()->ui->plot_description->setText(details);
                    init_math_channel(plot_vec.last().filter.plot_id);
                }
            }
#ifndef Q_OS_WIN
        }
#endif
    }
    return;
}

void plotting::on_save_export_button_clicked()
{
    if (ui->format_box->currentText() == "Screenshot")
        save_to_png();
    else if (ui->format_box->currentText() == "CSV")
        save_to_csv();
    return;
}

void plotting::get_messages(){
    QString file_path = ":/new/messages/can-ids/FST09e.csv";
    QFile file(file_path);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (opened){
        QString line = file.readLine();
        device_messages current_message;
        QList<QString> parameters;
        int id;
        QString device, signal, current_device;
        parameters = line.split(",");
        device = parameters[0];
        while((device == "VECTOR__INDEPENDENT_SIG_MSG") || (device == "MSG")){
            line = file.readLine();
            parameters = line.split(",");
            device = parameters[0];
        }

        id = parameters[1].toInt();
        signal = parameters[3];
        current_device = device;
        current_message.device = device;
        current_message.signal.append(signal);
        current_message.dev_id = id & 0b0000000000011111;
        current_message.msg_ids.append(id >> 5);
        current_message.offset.append(parameters[4].toInt());
        current_message.length.append(parameters[5].toInt());
        messages.append(current_message);
        ui->dev_id_box->addItem(current_message.device);
        while(!file.atEnd()){
            line = file.readLine();
            parameters = line.split(",");
            if(parameters[0] == current_message.device){
                current_message.signal.append(parameters[3]);
                current_message.msg_ids.append((parameters[1].toInt()) >> 5);
                current_message.offset.append(parameters[4].toInt());
                current_message.length.append(parameters[5].toInt());
            }
            else {
                device_messages new_current_message;
                messages.append(current_message);
                new_current_message.device = parameters[0];
                new_current_message.dev_id = parameters[1].toInt() & 0b0000000000011111;
                new_current_message.msg_ids.append((parameters[1].toInt()) >> 5);
                new_current_message.signal.append(parameters[3]);
                new_current_message.offset.append(parameters[4].toInt());
                new_current_message.length.append(parameters[5].toInt());
                current_device = parameters[0];
                current_message = new_current_message;
                ui->dev_id_box->addItem(current_message.device);
            }
        }
    }
    else{
        char error_msg[126];
        sprintf(error_msg, "Was not able to load the following file: :/new/messages/can-ids/FST09e.csv");
        ui->message_output->appendPlainText(error_msg);
    }
    return;
}

void plotting::update_msg_id_combo_box(){

    QString device = ui->dev_id_box->currentText();
    int index = -1;
    for(int k = 0; k < messages.size(); k++){
        if(messages[k].device == device){
            index = k;
        }
    }
    ui->msg_id_box->clear();
    if (index != -1){
        for(int i = 0; i < messages[index].signal.size(); i++){
            QString msg_id = messages[index].signal[i];
            ui->msg_id_box->addItem(msg_id);
        }
    }
    return;
}

void plotting::on_set_plot_messages_clicked()
{
    QString device = ui->dev_id_box->currentText();
    QString message_id = ui->msg_id_box->currentText();
    QString offset = ui->offset->text();
    QString length = ui->length->text();

    if ((device != "") && (message_id != "") && (length != "") && (offset != "")){
        if ((device.toInt() == 0 && device != "0") && (message_id.toInt() == 0 && message_id != "0")){
            for(int i = 0; i < messages.size(); i++){
                if (messages[i].device == device){
                    for(int k = 0; k < messages[i].signal.size(); k++){
                        if (messages[i].signal[k] == message_id){
                            set_new_plot();
                            plot_vec.last().filter.dev_id = messages[i].dev_id;
                            plot_vec.last().filter.msg_id = messages[i].msg_ids[k];
                            plot_vec.last().filter.length = length.toInt();
                            plot_vec.last().filter.offset = offset.toInt();
                            QString legend = device + ": " + message_id;
                            new_line_series->setName(legend);
                            QString details;
                            details = ui->dev_id_box->currentText();
                            details += ": " + ui->msg_id_box->currentText() + ", ";
                            QString off_len;
                            off_len = offset + " + " + length;
                            details += off_len;
                            filter_vector.last()->ui->plot_description->setText(details);
                            char filter_message[512];
                            sprintf(filter_message, "New filter set with the following parameters: CAN_ID = %d , MSG_ID = %d.", plot_vec.last().filter.dev_id, plot_vec.last().filter.msg_id);
                            ui->message_output->appendPlainText(filter_message);
                            return;
                        }
                    }
                }
            }
        }
        else{  //its a number
            if(device.toInt() == 0 && device != "0"){
                for(int i = 0; i < messages.size(); i++){
                    for(int k = 0; k < messages[i].signal.size(); k++){
                        if (messages[i].device == device){
                            plot_info.filter.dev_id = messages[i].dev_id;
                            plot_info.filter.length = ui->length->text().toInt();
                            plot_info.filter.offset = ui->offset->text().toInt();
                            plot_info.filter.msg_id = ui->msg_id_box->currentText().toInt();
                        }
                    }
                }
            }
            else if ((ui->msg_id_box->currentText() != "") && (ui->dev_id_box->currentText() != "")){
                plot_info.filter.dev_id = uint16_t (ui->dev_id_box->currentText().toInt());
                plot_info.filter.length = uint16_t (ui->length->text().toInt());
                plot_info.filter.offset = uint16_t (ui->offset->text().toInt());
                plot_info.filter.msg_id = uint16_t (ui->msg_id_box->currentText().toInt());
            }
            if ((plot_info.filter.msg_id >= 0) && (plot_info.filter.dev_id >= 0) && (plot_info.filter.offset >= 0 && plot_info.filter.offset < 64) && (plot_info.filter.offset + plot_info.filter.length < 64 )){
                if (!find_filter(plot_info.filter.dev_id, plot_info.filter.msg_id, plot_info.filter.offset, plot_info.filter.length)){
                    set_new_plot();
                    char filter_message[512];
                    sprintf(filter_message, "New filter set with the following parameters: CAN_ID = %d , MSG_ID = %d.", plot_info.filter.dev_id, plot_info.filter.msg_id);
                    ui->message_output->appendPlainText(filter_message);
                    QString details;
                    details = device + ", " + message_id + ", ";
                    QString legend = "Dev id: "+ device + ", Msg_id: " + message_id;
                    new_line_series->setName(legend);
                    QString off_len;
                    off_len = offset + " + " + length;
                    details += off_len;
                    filter_vector.last()->ui->plot_description->setText(details);
                }
            }
            else{
                char string[64];
                sprintf(string, "Not all filters were set/correctly set.");
                ui->message_output->appendPlainText(string);
            }
        }
    }
    return;
}


void plotting::on_msg_id_box_currentIndexChanged(int index)
{
    QString device = ui->dev_id_box->currentText();
    QString signal = ui->msg_id_box->currentText();

    for(int i = 0; i < messages.size(); i++){
        if(messages[i].device == device){
            for(int k = 0; k < messages[i].signal.size(); k++){
                if(messages[i].signal[k] == signal){
                    ui->length->setText(QString::number(messages[i].length[k]));
                    ui->offset->setText(QString::number(messages[i].offset[k]));
                    return;
                }
            }
        }
    }
    return;
}
