#ifndef ARM_BARS_HPP
#define ARM_BARS_HPP

#include <QWidget>

namespace Ui {
class arm_bars;
}

class arm_bars : public QWidget
{
    Q_OBJECT

public:
    explicit arm_bars(QWidget *parent = nullptr);
    ~arm_bars();

    void set_new_maximum(double value);
    void update_value(double value);
    int get_limit();
    int get_current_val();
    void set_new_minimum(double value);
private:
    int maximum;
    int minimum;
    int current_value;
    Ui::arm_bars *ui;
};

#endif // ARM_BARS_HPP
