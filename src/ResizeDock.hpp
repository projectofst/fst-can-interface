#ifndef RESIZEDOCK_HPP
#define RESIZEDOCK_HPP

#include <QObject>
#include <QKeyEvent>
#include "DevStatus.hpp"

class ResizeDock : public QObject
{
    Q_OBJECT

protected:
    bool eventFilter(DevStatus*, QEvent*);
};

#endif // RESIZEDOCK_HPP
