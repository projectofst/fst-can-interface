#include "versions.hpp"
#include "ui_versions.h"
#include <qpalette.h>
#include <QDebug>
#include <QFile>
#include <QTimer>
#include <QFileDialog>
#include <QDir>

#include "pcb_version.hpp"
#include "table.h"

versions::versions(QWidget *parent) :
    ui(new Ui::versions)
{
    ui->setupUi(this);
    number_of_devices = 0;
    request_byte = 0;
    request_byte = 0;

    init_devices(); /* Get all the  possible devices */

    timer1 = new QTimer(this);
    connect(timer1, SIGNAL(timeout()), this, SLOT(request_version()));
    timer_status = 0;
}

versions::~versions()
{
    delete ui;
}

void versions::process_CAN_message(CANmessage message){
    if (message.candata.msg_id == CMD_ID_COMMON_GET){
        process_new_version(message);
    }
}

void versions::request_version(){
    /* We need to cycle through the 4 bytes of each PCB available... */

    if (request_byte == 4 ){ /*If all 4 bytes of a version have been requested,, we go to the next device*/
        request_byte = 0;
        number_of_devices++;
    }

    if (number_of_devices == devices.size()) /* start over */
        number_of_devices = 0;

    if(number_of_devices < devices.size()) {
        CANmessage message;
        message.candata.dev_id = DEVICE_ID_INTERFACE;
        message.candata.msg_id = CMD_ID_COMMON_GET;
        message.candata.dlc = 8;
        message.candata.data[0] = devices[number_of_devices];
        message.candata.data[1] = request_byte;
        message.candata.data[2] = 0;
        message.candata.data[3] = 0;
        request_byte++;
        emit send_to_CAN(message);
    }
    return;
}

void versions::read_versions(){


    QString filePath = QFileDialog::getOpenFileName(this,
                                                    tr("Save"), QDir::currentPath());
    QFile file(filePath);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (opened){
        QString line = file.readLine();
        while (!file.atEnd()){
            QList<QString> parameters = line.split(",");
            QString name = parameters[0];
            pcb_version *new_pcb = new pcb_version;
            QString expected_version = parameters[1];
            new_pcb->set_name(name);
            new_pcb->set_expected_version(expected_version);
            all_pcbs.append(new_pcb);
            ui->list->addWidget(new_pcb);

            line = file.readLine();
        }
    }
    return;
}

void versions::process_new_version(CANmessage message){
    QString nome_placa = nomes_placas[message.candata.dev_id];

    for (int k = 0; k < all_pcbs.size(); k++){
        if(nome_placa == all_pcbs[k]->get_name()){ /* Find the PCB */
            all_pcbs[k]->set_current_version(message.candata.data[2], message.candata.data[1]); /*Set the value of the byte, check pcb_versions.cpp*/
        }
    }
    return;
}

void versions::on_refresh_button_clicked()
{
    if (timer_status == 0) {
        timer1->start(50);
        timer_status = 1;
    }else {
        timer1->stop();
        timer_status=0;
    }
}


void versions::init_devices(){
    QString file_path = ":/ids/table.txt"; /*Get the devices*/
    QFile file(file_path);
    bool opened = file.open(QIODevice::ReadOnly | QIODevice::Text);
    if (opened){
        QString line;
        QList<QString> parameters, name;
        while(!file.atEnd()){
            line = file.readLine();
            parameters = line.split("\t");
            name = line.split("_");
            if(parameters.size() >=3 && (name[0] == "DEVICE"))
                devices.append(parameters[2].toUShort()); /* Parameter 2 is the device id read the file, it will make sense */
        }
    }
}

int versions::device_in_table(QString device){
    for(int k = 0; k < 32; k++){
        if(nomes_placas[k] == device){
            return 1;
        }
    }
    return 0;
}


void versions::on_load_clicked()
{
    /*If we load again, remove existing widgets on list*/
    for (int k = 0; k < all_pcbs.size(); k++){
        auto pcb = all_pcbs.at(k);
        pcb->destroy();
        all_pcbs.removeAt(k);
        k--;
    }
    read_versions();
}
