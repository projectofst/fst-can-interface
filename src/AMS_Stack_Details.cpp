#include <QVBoxLayout>
#include <QVector>

#include "AMS_Stack_Details.hpp"
#include "ui_AMS_Stack_Details.h"
#include "AMS_Cell_Line.hpp"

StackDetails::StackDetails(QWidget *parent, QString title):
    QWidget(parent),
    ui(new Ui::StackDetails)
{
    ui->setupUi(this);
    ui->groupBox->setTitle(title);

    for (int i=0; i<12; i++) {
        CellLine *cl = new CellLine(this, "Cell " + QString::number(i));
        cells.append(cl);
        ui->cellLines->addWidget(cl);
    }
}

StackDetails::~StackDetails()
{
    delete ui;
}

void StackDetails::clear(void) {
    for (int i=0; i<12; i++) {
        cells[i]->clear();
    }
}



