#ifndef BMS_HPP
#define BMS_HPP

#include <QWidget>
#include <QLineEdit>
#include <QMap>
#include <QSpinBox>
#include <QLabel>

#include "UiWindow.hpp"
#include "BMS_Stack.hpp"
#include "can-ids/CAN_IDs.h"

namespace Ui {
class BatteryT;
}

class BatteryT : public UiWindow
{
    Q_OBJECT

public:
    explicit BatteryT(QWidget *parent = nullptr);
    ~BatteryT() override;

public slots:
    void process_CAN_message(CANmessage) override;
    void resize(int);

private:
    Ui::BatteryT *ui;
    void layoutDevs(int);
    void createDevs();
    int currentK;
    int lastK;

    QMap<uint,QWidget*> Stacks;
    QLineEdit *templine;
    QSpinBox *tempspin;

signals:
    void send_to_CAN(CANmessage) override;
    void rebroadcast_CAN_message(CANmessage);
};

#endif // BMS_HPP
