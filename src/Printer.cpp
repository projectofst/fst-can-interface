#include "Printer.hpp"
#include "ui_Printer.h"

#include "COM_SerialPort.hpp"
Printer::Printer(QWidget *parent) : QWidget(parent)
    , ui(new Ui::Printer)
{
    ui->setupUi(this);
}

//QObject::connect(&_SPort, SIGNAL(new_messages(int)), this, SLOT(print_and_broadcast_messages(void)));

Printer::~Printer()
{
    delete ui;
}

void Printer::append_string(QString string ){

    ui->output->appendPlainText(string);
}

void Printer::on_pushButton_clicked()
{
    ui->output->clear();
}

void Printer::on_button_send_clicked()
{
    QString input_string=ui->input_msg->text();

    emit send_serial(input_string);
}
