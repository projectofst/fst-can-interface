#ifndef REPORT_HPP
#define REPORT_HPP

#define N_MASTER 2

#define RED 1
#define GREEN 2
#define BLUE 3
#define BLACK 0
#define ERROR 10

#include <QWidget>
#include <QTimer>
#include <QLabel>
#include "UiWindow.hpp"
//#include "CANmessage.hpp"
//#include "status.hpp"

#include "can-ids/CAN_IDs.h"
#include "report_filters.hpp"
#include "can-ids/erros.h"

namespace Ui {
class Log;
}

class Log : public UiWindow
{
    Q_OBJECT

public:
    explicit Log(QWidget *parent = nullptr);
    ~Log(void);
    Report_filters* filters;
	private slots:
        void process_CAN_message(CANmessage message);
		void on_clearlog_clicked(void);
        void UpdateTimer(void);


        void on_savebutton_clicked();


        void on_StatusLog_clicked();
        void new_log_event(CANdata msg);

        void on_filters_button_clicked();
        void init_filters();
        int get_check_status(int dev_id, int msg_id);

private:
    Ui::Log *ui;
    int _op_mode_master[N_MASTER];
    unsigned int _status_flags_mask_master[N_MASTER];
    unsigned int _ts_on;
    unsigned int _ams_error;
    unsigned int _air_feed;
    unsigned int IMD_status;
    unsigned int IMD_statusreset;
    int send_message;
    //void initflags();
    void WriteString(QString, unsigned int);

    //Status _CarStatus;

signals:
    //void send_to_CAN(CANdata msg);
    /*void detail_voltage(unsigned int cell_n, float voltage);
    void detail_temperature(unsigned int cell_n, float temperature);
    void change_mode(int mode);
    void wire_fault(unsigned int wire_n, bool ok, int type);
    void SetDash(unsigned int AIRm, unsigned int AIRp, unsigned int IMDmem, unsigned int IMDnok, unsigned int AMS);
    void SetDCU(unsigned int OFF1, unsigned int VCC_Siemens, unsigned int VCC_CAN_C, unsigned int temp);
    void SetTE(unsigned int override, unsigned int TPS_plaus, unsigned int TPS_BP_plaus, unsigned int temp);*/

};

#endif // REPORT_HPP
