#include "Main_Window.hpp"
#include "ui_Main_Window.h"

#include "QDebug"

#include "QJsonDocument"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    /* DockWidget Related Changes */
    //ui->centralWidget->hide(); // Hide central widget to allow spawn of DockWidgets
    setDockNestingEnabled(true); // To split into any orientation
    setTabPosition(Qt::AllDockWidgetAreas, QTabWidget::North); // Force tabbed docks to have their tabs at North
    setDocumentMode(true); // Force Tabs to look the same in any OS

    Sitrep *sitrep = new Sitrep();
    connect(sitrep,SIGNAL(update_status(QString)), ui->statusBar, SLOT(showMessage(QString)));

//    QString val;
//    QFile *file = new QFile("src/test.json");
//    file->open(QIODevice::ReadOnly | QIODevice::Text);
//    val = file->readAll();
//    file->close();
//    qDebug()<<val;
//    QJsonDocument d = QJsonDocument::fromJson(val.toUtf8());
}

MainWindow::~MainWindow(void)
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter* pPainter = new QPainter(this);
    pPainter->drawPixmap(rect(), QPixmap(":/Assets/Backgrounds/FSTelemetryBackground.png"));
    delete pPainter;
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::Resize) {
        QResizeEvent *resizeEvent = static_cast<QResizeEvent*>(event);
        if (obj->objectName().startsWith("[DevDock]"))
            emit resizeDev(resizeEvent->size().width());
        if (obj->objectName().startsWith("[BatDock]"))
            emit resizeBat(resizeEvent->size().width());
        return false;
    } else {
        return QWidget::eventFilter(obj, event);
    }
}

void MainWindow::updateStatusBar()
{
    QPixmap *pixmap = new QPixmap;

    // Note that here I don't specify the format to make it try to autodetect it,
    // but you can specify this if you want to.
    pixmap->load(":/Instatroll/IconWallet/png/verified.png");

    QLabel *iconLbl = new QLabel;
    iconLbl->setPixmap(*pixmap);
    ui->statusBar->addWidget(iconLbl);
}

void MainWindow::on_actionAbout_triggered()
{
    About *about = new About();
    about->show();
    about->activateWindow();
    about->raise();
}

void MainWindow::on_actionOpen_Log_triggered()
{
//    LogConverter *log_convert = new LogConverter();
//    log_convert->show();
//    log_convert->activateWindow();
//    log_convert->raise();
}

void MainWindow::on_actionNew_triggered()
{

}

void MainWindow::actionToggleCOM_triggered()
{
//    QDockWidget *Coms = new QDockWidget("Coms",this);
//    Coms->setAllowedAreas(Qt::AllDockWidgetAreas);
//    this->addDockWidget(Qt::TopDockWidgetArea, Coms);
//    QQuickWidget *temp = new QQuickWidget(QUrl("file:///Users/diogopereira/Documents/fst-can-interface/src/COM_Select.qml"),Coms);
//    temp->setResizeMode(QQuickWidget::SizeRootObjectToView);
//    Coms->setWidget(static_cast<QWidget*>(temp));

//    comselect->USBList();

}
