#ifndef INVERTERS_H
#define INVERTERS_H

#include <QWidget>
#include "can-ids/CAN_IDs.h"
#include "UiWindow.hpp"
#include "iib_motor_control.hpp"
#include <QVBoxLayout>
#include <QtCharts>
#include <QLineSeries>
#include <QChartView>
#include <QTableView>
#include "state_manager.hpp"

#define NAME "IIB"
extern const char *MODES[];
extern int PARAMETERS[];
namespace Ui {
class Inverters;
}

class Inverters : public UiWindow
{
    Q_OBJECT

public:
    explicit Inverters(QWidget *parent = nullptr);
    ~Inverters() override;

private slots:
    void process_CAN_message(CANmessage message) override;

    void on_pushButton_5_clicked();

    void timer_clear_inverters();

    void clear();

    void on_rtd_button_clicked();

    void on_set_mode_clicked();

    void on_specific_val_textEdited(const QString &arg1);

    void on_parameter_currentIndexChanged(int index);

    void check_spec_val(double val);

    void on_set_new_pow_tor_clicked();

    void on_Set_motor_speed_clicked();

    void on_motor_speed_textChanged(const QString &arg1);

    void on_set_regen_clicked();

    void on_debug_but_toggled(bool checked);


    void on_set_all_iib_clicked();

    void on_set_torque_front_clicked();

    void on_set_torque_rear_clicked();

    void on_save_button_clicked();

    void save_settings();

    void load_settings();

    void on_save_clicked();

    void on_load_clicked();


private:
    Ui::Inverters *ui;
    int clear_inv_flag;
    int Inv_cleared;
    int regen_on;
    state_manager* manager;
    QString m_sSettingsFile, specific;

signals:
    void send_to_CAN(CANmessage msg) override;
};

#endif // INVERTERS_H
