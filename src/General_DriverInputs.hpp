#ifndef GENERAL_DRIVERINPUTS_HPP
#define GENERAL_DRIVERINPUTS_HPP

#include <QWidget>
#include <QTimer>

#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"
#include "can-ids/table.h"
#include "can-ids/Devices/TE_CAN.h"
#include "can-ids/Devices/DCU_CAN.h"
#include "can-ids/Devices/DASH_CAN.h"
#include "can-ids/Devices/COMMON_CAN.h"
#include "can-ids/Devices/INTERFACE_CAN.h"
#include "can-ids/Devices/STEER_CAN.h"

namespace Ui {
class DriverInputs;
}

class DriverInputs : public UiWindow
{
    Q_OBJECT

public:
    explicit DriverInputs(QWidget *parent = nullptr);
    ~DriverInputs() override;
    void updateUI();

public slots:
    void process_CAN_message(CANmessage) override;

signals:
    void send_to_CAN(CANmessage) override;

private slots:
    void on_horizontalSlider_actionTriggered(int action);

    void on_horizontalSlider_valueChanged(int value);

private:
    Ui::DriverInputs *ui;
    TE_CAN_Data ggen_TE_CAN_Data;
    STEER_MSG_SIG ggen_steer;
    QTimer *updateTimer;
};

#endif // GENERAL_DRIVERINPUTS_HPP
