#include <QCheckBox>
#include <QTimer>
#include <QDebug>
#include <QMap>
#include <QDateTime>
#include <QDate>

#include "ledform.hpp"
#include "General.hpp"
#include "ui_General.h"

#include <stdint.h>
#include <stdbool.h>

#include "can-ids/CAN_IDs.h"
#include "can-ids/table.h"
#include "can-ids/Devices/STEER_CAN.h"
#include "can-ids/Devices/IIB_CAN.h"
#include "can-ids/Devices/BMS_MASTER_CAN.h"

static DCU_CAN_Data ggen_DCU_CAN_Data;
static DASH_CAN_Data ggen_DASH_CAN_Data;
static TE_CAN_Data ggen_TE_CAN_Data;
static COMMON_MSG_RTD_ON ggen_COMMON_RTD_ON_CAN_Data;
static STEER_MSG_SIG ggen_steer;
static IIB_CAN_data ggen_iib;
static MASTER_MSG_Data ggen_bms;


#define PEDAL_MAX_VALUE 10000
#define STEER_MAX_VALUE 10000

General::General(QWidget *parent,SQL_CANID *test) : UiWindow(parent) , ui(new Ui::General)
{
    ui->setupUi(this);

    teste = test;

    // Set bars to maximum ADC value (12bits => 4096)
    // TODO: APPS max was 1000 before
    ui->APPS_BAR->setMaximum(PEDAL_MAX_VALUE);
    ui->APPS_BAR->setMinimum(0);

    ui->BPS_BAR->setMaximum(PEDAL_MAX_VALUE);
    ui->BPS_BAR->setMinimum(0);

    ui->BPEL_BAR->setMaximum(PEDAL_MAX_VALUE);
    ui->BPEL_BAR->setMinimum(0);

    ui->steer_set_left->setAutoFillBackground(true);
    ui->steer_set_middle->setAutoFillBackground(true);
    ui->steer_set_right->setAutoFillBackground(true);

    //disable_brake_thresholds();

    ui->brake_lower->setEnabled(true);
    ui->brake_upper->setEnabled(true);
    ui->dcu_toggle_dcdc_drs->setEnabled(false);
    ui->dcu_toggle_drs_move->setEnabled(false);

    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(50);

    clear_timer = new QTimer(this);
    connect(clear_timer, &QTimer::timeout, this, &General::clear_all);
    clear_timer->start(1500);
    clear_flag = 1;
    connect(&general_details, &GeneralDetails::send_CAN, this, &General::retransmit_can);
}

General::~General()
{
    delete ui;
}

void General::retransmit_can(CANmessage msg) {
    emit send_to_CAN(msg);
    return;
}

void General::disable_brake_thresholds(){
    //ui->brake_lower->setEnabled(false);
    //ui->brake_upper->setEnabled(false);
}

void General::process_CAN_message(CANmessage message)
{
    CANdata msg = message.candata;
    QString module_name = nomes_placas[msg.dev_id];

     clear_flag = 0;
    //COMMON MESSAGE IDs

    if (msg.msg_id==CMD_ID_COMMON_RTD_ON){
        parse_can_common_RTD(msg, &ggen_COMMON_RTD_ON_CAN_Data);
        general_details.updateRTDSequence(ggen_COMMON_RTD_ON_CAN_Data);
    }

    switch (msg.dev_id) {

    case DEVICE_ID_TE:
        parse_can_te(msg, &ggen_TE_CAN_Data);

        //updateTEGeneralUI();
        //general_details.updateTorqueEncoderDetails(&ggen_TE_CAN_Data);
        break;

    case DEVICE_ID_DASH:
        if (msg.msg_id==MSG_ID_DASH_STATUS){
            parse_can_dash(msg, &ggen_DASH_CAN_Data);
            //updateDashGeneralUI();
        }
        break;

    case DEVICE_ID_STEER:
        parse_can_steer(msg, &ggen_steer);
        //updateSteerUI();
        break;
    case DEVICE_ID_IIB:
        parse_can_IIB(msg, &ggen_iib);
        //updateIIBUI();
        break;

    case DEVICE_ID_BMS_MASTER:
        parse_can_message_master(msg, &ggen_bms);
        //updateBMSUI();
        break;

    case DEVICE_ID_DCU:
        parse_can_dcu(msg, &ggen_DCU_CAN_Data);
        //updateDCUGeneralUI();
        break;

    default:
        break;
    }
}

void General::update(){
    if(!clear_flag){
        updateBMSUI();
        updateIIBUI();
        updateSteerUI();
        updateTEGeneralUI();
        updateDCUGeneralUI();
        updateDashGeneralUI();

        general_details.updateTorqueEncoderDetails(&ggen_TE_CAN_Data);
    }
    return;
}


void General::updateDCUGeneralUI(){
    ui->DCU_Current_LV->setNum(ggen_DCU_CAN_Data.current.LV_current);
    ui->DCU_Current_HV->setNum(ggen_DCU_CAN_Data.current.HV_current);
    if (ggen_DCU_CAN_Data.status.BL_sig)
        ui->BrakeLight_LED->turnRed();

    if(!ggen_DCU_CAN_Data.status.Detection_SC_Front_BSPD)
        ui->front_bspd_led->turnRed();

    else ui->front_bspd_led->turnGreen();

    if(!ggen_DCU_CAN_Data.status.Detection_SC_MH_Front)
        ui->mh_front_led->turnRed();

    else ui->mh_front_led->turnGreen();

    if(!ggen_DCU_CAN_Data.status.Detection_SC_BSPD_AMS)
        ui->bspd_ams_led->turnRed();

    else ui->bspd_ams_led->turnGreen();

    if(!ggen_DCU_CAN_Data.status.Detection_SC_Origin_MH)
        ui->origin_mh_led->turnRed();

    else ui->origin_mh_led->turnGreen();



}

void General::updateDashGeneralUI(){

    if (!ggen_DASH_CAN_Data.status.SC==1)
        ui->dash_led->turnRed();

    else ui->dash_led->turnGreen();

    general_details.updateDashLEDs(ggen_DASH_CAN_Data);
}

void General::updateTEGeneralUI(){

    /*if (ggen_TE_CAN_Data.main_message.status.Debug_Mode)
        ui->TE_LED->turnCyan();*/

    ui->APPS_BAR->setValue(ggen_TE_CAN_Data.main_message.APPS);
    ui->BPS_BAR->setValue(ggen_TE_CAN_Data.main_message.BPS_pressure);
    ui->BPEL_BAR->setValue(ggen_TE_CAN_Data.main_message.BPS_electric);

    if(ggen_TE_CAN_Data.main_message.status.SC_Motor_Interlock_Front)
        ui->te_led->turnRed();

    else ui->te_led->turnGreen();
}

void General::updateSteerUI(){

    if(ggen_steer.value > 0){
        ui->steer_right->setValue(-1 * ggen_steer.value);
        ui->steer_left->setValue(0);
    }
    else{
        ui->steer_right->setValue(0);
        ui->steer_left->setValue(ggen_steer.value);
    }

    if(ggen_steer.clear_interface){
        ui->steer_set_left->setStyleSheet("background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)");
        ui->steer_set_middle->setStyleSheet("background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)");
        ui->steer_set_right->setStyleSheet("background-color: rgb(255, 255, 255); color: rgb(0, 0, 0)");
    }
    return;
}

void General::updateIIBUI() {
    if(ggen_iib.car_info.status.SDC1) {
        ui->sdc1_led->turnGreen();
    }
    else {
        ui->sdc1_led->turnRed();
    }

    if(ggen_iib.car_info.status.SDC2) {
        ui->sdc2_led->turnGreen();
    }
    else {
        ui->sdc2_led->turnRed();
    }
}

void General::updateBMSUI() {
    if(ggen_bms.status.SC_AMS_DCU) {
        ui->ams_dcu_led->turnGreen();
    }
    else {
        ui->ams_dcu_led->turnRed();
    }
    if(ggen_bms.status.SC_DCU_IMD) {
        ui->dcu_imd_led->turnGreen();
    }
    else {
        ui->dcu_imd_led->turnRed();
    }
    if(ggen_bms.status.SC_IMD_AMS) {
        ui->imd_ams_led->turnGreen();
    }
    else {
        ui->imd_ams_led->turnRed();
    }
    if(ggen_bms.status.SC_TSMS_Relays) {
        ui->tsms_relays_led->turnGreen();
    }
    else {
        ui->tsms_relays_led->turnRed();
    }
}


void General::dcu_toggle(DCU_TOGGLE code){
    CANmessage msg;

    msg.candata.dev_id=DEVICE_ID_INTERFACE;

    msg.candata.msg_id=CMD_ID_INTERFACE_DCU_TOGGLE;

    msg.candata.dlc=2;

    msg.candata.data[0]=code;
    msg.candata.data[1]=0;
    msg.candata.data[2]=0;
    msg.candata.data[3]=0;

    emit send_to_CAN(msg);

}

void General::on_dcu_toggle_dcdc_drs_clicked()
{
    dcu_toggle(DCU_DCDC_DRS);
}

void General::debug_toggle(DEBUG_TOGGLE code){
    CANmessage msg;

    msg.candata.dev_id=DEVICE_ID_INTERFACE;

    msg.candata.msg_id=CMD_ID_INTERFACE_DEBUG_TOGGLE;

    msg.candata.dlc=2;

    msg.candata.data[0]=code;
    msg.candata.data[1]=0;
    msg.candata.data[2]=0;
    msg.candata.data[3]=0;

    emit send_to_CAN(msg);

}
void General::on_debug_mode_te_clicked()
{
    debug_toggle(DEBUG_TOGGLE_TORQUE_ENCODER);
}

void General::on_debug_mode_dcu_clicked()
{
    debug_toggle(DEBUG_TOGGLE_DCU);
}

void General::on_debug_mode_dash_clicked()
{
    debug_toggle(DEBUG_TOGGLE_DASH);
}


void General::set_pedal_thresholds(INTERFACE_MSG_PEDAL_THRESHOLDS select){

    CANmessage msg;

    msg.candata.dev_id=DEVICE_ID_INTERFACE;
    msg.candata.msg_id=CMD_ID_INTERFACE_SET_PEDAL_THRESHOLD;
    msg.candata.dlc=2;
    msg.candata.data[0]= select;
    msg.candata.data[1]= 0;
    msg.candata.data[2]= 0;
    msg.candata.data[3]= 0;

    emit send_to_CAN(msg);
}


void General::on_accel_lower_clicked()
{
    set_pedal_thresholds(ACCELERATOR_ZERO);
}

void General::on_accel_upper_clicked()
{
    set_pedal_thresholds(ACCELERATOR_MAX);
}

void General::on_brake_lower_clicked()
{
    set_pedal_thresholds(BRAKE_ZERO);
}

void General::on_brake_upper_clicked()
{
    set_pedal_thresholds(BRAKE_MAX);
}

void General::on_clearButton_clicked()
{
    general_details.clear();
    ui->BrakeLight_LED->turnOff();

    ui->APPS_BAR->setValue(0);
    ui->BPS_BAR->setValue(0);
    ui->BPEL_BAR->setValue(0);
}

void General::set_steer_threshold(INTERFACE_MSG_STEER_THRESHOLDS select){

    CANmessage msg;

    msg.candata.dev_id=DEVICE_ID_INTERFACE;
    msg.candata.msg_id=CMD_ID_INTERFACE_SET_STEER_THRESHOLD;
    msg.candata.dlc=2;
    msg.candata.data[0]= select;
    msg.candata.data[1]= 0;
    msg.candata.data[2]= 0;
    msg.candata.data[3]= 0;

    emit send_to_CAN(msg);
}

void General::on_steer_set_left_clicked()
{
    set_steer_threshold(STEER_LEFT);
    ui->steer_set_left->setStyleSheet("background-color: rgb(255, 0, 0); color: rgb(0, 0, 0)");

}

void General::on_steer_set_middle_clicked()
{
    set_steer_threshold(STEER_CENTRE);
    ui->steer_set_middle->setStyleSheet("background-color: rgb(255, 0, 0); color: rgb(0, 0, 0)");
}

void General::on_steer_set_right_clicked()
{
    set_steer_threshold(STEER_RIGHT);
    ui->steer_set_right->setStyleSheet("background-color: rgb(255, 0, 0); color: rgb(0, 0, 0)");
}

void General::on_start_log_button_clicked()
{
    int year = QDate::currentDate().year();
    int month = QDate::currentDate().month();
    int day = QDate::currentDate().day();

    int hour = QTime::currentTime().hour();
    int minute = QTime::currentTime().minute();
    int second = QTime::currentTime().second();

    CANmessage msg;

    msg.candata.dev_id = DEVICE_ID_INTERFACE;
    msg.candata.msg_id = CMD_ID_INTERFACE_START_LOG;

    msg.candata.dlc = 6;

    msg.candata.data[0] = static_cast<uint16_t>(year | day << 11);
    msg.candata.data[1] = static_cast<uint16_t>(month | hour << 4);
    msg.candata.data[2] = static_cast<uint16_t>(minute | second << 6);

    INTERFACE_CAN_Data interface_data;

    parse_can_interface(msg.candata, &interface_data);

    year = interface_data.start_log.year;
    month = interface_data.start_log.month;
    day = interface_data.start_log.day;
    hour = interface_data.start_log.hour;
    minute = interface_data.start_log.minute;
    second = interface_data.start_log.second;

    emit send_to_CAN(msg);

    //printf("Year: %u\nMonth:%u\nDay:%u\nHour:%u\nMinute:%u\nSecond:%u\n", year,month,day,hour,minute,second);

}

void General::on_details_clicked()
{
    general_details.show();
    general_details.activateWindow();
    general_details.raise();
}

void General::on_dcu_toggle_pumps_clicked()
{
    dcu_toggle(DCU_PUMPS1);
    dcu_toggle(DCU_PUMPS2);
}

void General::clear_all(){
    if(clear_flag){
        general_details.clear();
        ui->ams_dcu_led->turnOff();
        ui->bspd_ams_led->turnOff();
        ui->dash_led->turnOff();
        ui->dcu_imd_led->turnOff();
        ui->front_bspd_led->turnOff();
        ui->imd_ams_led->turnOff();
        ui->mh_front_led->turnOff();
        ui->origin_mh_led->turnOff();
        ui->sdc1_led->turnOff();
        ui->sdc2_led->turnOff();

        ui->te_led->turnOff();
        ui->tsms_relays_led->turnOff();

        ui->DCU_Current_HV->setText("0");
        ui->DCU_Current_LV->setText("0");
        ui->BPEL_BAR->setValue(0);
        ui->APPS_BAR->setValue(0);
        ui->BPS_BAR->setValue(0);
    }
    clear_flag = 1;
}
