#ifndef UIWINDOW_HPP
#define UIWINDOW_HPP

#include <QWidget>

#include "can-ids/CAN_IDs.h"

namespace Ui {
    class UiWidget;
}

class UiWindow : public QWidget
{
    Q_OBJECT

public:
    explicit UiWindow(QWidget *parent = nullptr);
    ~UiWindow(void);

signals:
    virtual void send_to_CAN(CANmessage);

public slots:
    virtual void process_CAN_message(CANmessage);

};

#endif // UIWINDOW_HPP
