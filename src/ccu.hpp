#ifndef CCU_HPP
#define CCU_HPP

#include <QWidget>
#include "CAN_IDs.h"
#include "UiWindow.hpp"
#include "ccu_indiv.hpp"
#include "CCU_CAN.h"
#include <QTimer>
namespace Ui {
class CCU;
}

class CCU : public UiWindow
{
    Q_OBJECT

public:
    explicit CCU(QWidget *parent = nullptr);
    ~CCU();
    void retransmit_can(CANmessage msg);
    void process_CAN_message(CANmessage msg);
    void get_current_values();
    void clear_values();


signals:
    void send_to_CAN(CANmessage);
private slots:
    void init();
    void on_send_all_clicked();
    void create_new(QString name, uint16_t dev_id, uint16_t get_parameter, uint16_t set_parameters);

    void on_set_0_clicked();

private:
    Ui::CCU *ui;
    QVector<QString> names_right;
    QVector<QString> names_left;
    QVector<ccu_indiv *> lines;
    QVector<uint16_t> dev_ids;
    QVector<uint16_t> set_parameters;
    QVector<uint16_t> get_parameters;
    QTimer *clear_timer;
    QTimer *get_timer;
};

#endif // CCU_HPP
