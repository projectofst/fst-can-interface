#ifndef BMS_STACK_HPP
#define BMS_STACK_HPP

#include <QWidget>

#include "UiWindow.hpp"
#include "can-ids/CAN_IDs.h"

namespace Ui {
class StackT;
}

class StackT : public UiWindow
{
    Q_OBJECT

public:
    explicit StackT(QWidget *parent = nullptr,uint Id = 1);
    ~StackT() override;

public slots:
    void process_CAN_message(CANmessage) override;

private:
    Ui::StackT *ui;
    int MinVolt;
    int MaxVolt;
    int MinTemp;
    int MaxTemp;

signals:
    virtual void send_to_CAN(CANmessage) override;

};

#endif // BMS_STACK_HPP
