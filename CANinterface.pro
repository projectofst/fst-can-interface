QT += core gui
QT += concurrent
QT += serialport
QT += network
QT += charts
QT += quickwidgets
QT += quickcontrols2
QT += sql
QT += serialbus

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport
TARGET = CANinterface
TEMPLATE = app

# Force C++ mode for C files - crucial to compile can-ids.
QMAKE_CC = g++
CONFIG += silent


#####################################################################
# COMPILER / LINKER  FLAGS

DEFINES = GIT_CURRENT_SHA1="\\\"$(shell git rev-parse HEAD)\\\""

QMAKE_CXXFLAGS += -std=c++17

# Debugging prints
# avoid using many of these at the same time: they all enable asynchronous prints to stderr (race conditions)!
# QMAKE_CXXFLAGS += -DDBG_GUIsssssssssssssss
# QMAKE_CXXFLAGS += -DDBG_SERIAL_RECEPTION
# QMAKE_CXXFLAGS += -DDBG_SERIAL_SEND
# QMAKE_CXXFLAGS += -DDBG_BAT_CAN_RECEIVE
# QMAKE_CXXFLAGS += -DDBG_DETAIL_BAT_RECEIVE

#QMAKE_MAKEFILE = Makefile.qmake

#####################################################################
# SOURCES

INCLUDEPATH +=\
    ass/\
    can-ids/\
    can-ids/Devices/\
    qt-can-com/\
    src/\
    src/Resources/\
    sql/\

SOURCES +=\
    can-ids/Devices/AHRS_CAN.c \
    can-ids/Devices/ARM_CAN.c \
    can-ids/Devices/BMS_MASTER_CAN.c \
    can-ids/Devices/BMS_VERBOSE_CAN.c \
    can-ids/Devices/COMMON_CAN.c \
    can-ids/Devices/DASH_CAN.c \
    can-ids/Devices/DASH_STEER_CAN.c \
    can-ids/Devices/DCU_CAN.c \
    #can-ids/Devices/GPS_CAN.c \
    can-ids/Devices/IIB_CAN.c \
    can-ids/Devices/INTERFACE_CAN.c \
    can-ids/Devices/LOGGER_CAN.c \
    can-ids/Devices/SNIFFER_CAN.c \
    can-ids/Devices/STEER_CAN.c \
    #can-ids/Devices/SUPOT_CAN.c \
    can-ids/Devices/TE_CAN.c \
    can-ids/CAN_IDs.c \
    can-ids/table.c \
    can-ids/erros.c \
    src/lap_time.cpp \
    src/Custom_Lap.cpp \
    src/Resources/filterwidget.cpp \
    src/Resources/ledform.cpp \
    src/Resources/LEDwidget.cpp \
    src/Resources/plot_filter_widget.cpp \
    src/AMS.cpp \
    src/AMS_Battery.cpp \
    src/AMS_Cell.cpp \
    src/AMS_Cell_Line.cpp \
    src/AMS_Details.cpp \
    src/AMS_Stack.cpp \
    src/AMS_Stack_Details.cpp \
    src/AMS_Stack_Summary.cpp \
    qt-can-com/COM.cpp \
    src/COM_LogConvert.cpp \
    src/COM_LogGen.cpp \
    src/COM_LogPlay.cpp \
    qt-can-com/COM_LogRec.cpp \
    src/COM_Manager.cpp \
    src/COM_Select.cpp \
    qt-can-com/COM_SerialPort.cpp \
    src/Console.cpp \
    src/Console_idlist.cpp \
    src/Console_makeid.cpp \
    src/DevStatus.cpp \
    src/General.cpp \
    src/General_Details.cpp \
    src/General_Details_PotProgress.cpp \
    src/Inverters.cpp \
    src/arm_bars.cpp \
    src/arms_torque_set.cpp \
    src/ccu.cpp \
    src/ccu_indiv.cpp \
    src/main.cpp \
    src/Main_About.cpp \
    src/Main_SubWindow.cpp \
    src/Main_Window.cpp \
    src/MotorBench.cpp \
    src/Picasso.cpp \
    src/Plotting.cpp \
    src/Printer.cpp \
    src/Report.cpp \
    src/Sensors.cpp \
    src/Sitrep.cpp \
    src/Isabelle.cpp \
    src/UiWindow.cpp \
    qt-can-com/COM_UdpReceiveSock.cpp \
    qt-can-com/COM_UdpSendSock.cpp \
    can-ids/Devices/WATER_TEMP_CAN.c \
    src/Main_Toolbar.cpp \
    src/General_DriverInputs.cpp \
    src/BMS.cpp \
    src/BMS_Stack.cpp \
    src/COM_Interface.cpp \
    src/SQL_CANID.cpp \
    src/SQL_CANID_Select.cpp \
    src/Plot_Callout.cpp \
    src/comselectline.cpp \
    qt-can-com/COM_LAN.cpp \
    src/run_laps.cpp \
    src/single_lap.cpp \
    src/state_manager.cpp \
    src/timestamp.cpp \
    src/time.cpp \
    src/isa_config.cpp \
    src/pcb_version.cpp \
    src/drs_cal.cpp \
    src/iib_motor_control.cpp \
    src/report_filters.cpp \
    src/report_indiv_filter.cpp \
    src/versionline.cpp\
    src/COM_Kvaser.cpp\
    src/memoryview.cpp \
    src/versions.cpp \
    src/arm.cpp

HEADERS +=\
    can-ids/Devices/AHRS_CAN.h \
    can-ids/Devices/ARM_CAN.h \
    can-ids/Devices/BMS_MASTER_CAN.h \
    can-ids/Devices/BMS_VERBOSE_CAN.h \
    can-ids/Devices/COMMON_CAN.h \
    can-ids/Devices/DASH_CAN.h \
    can-ids/Devices/DASH_STEER_CAN.h \
    can-ids/Devices/DCU_CAN.h \
    #can-ids/Devices/GPS_CAN.h \
    can-ids/Devices/IIB_CAN.h \
    can-ids/Devices/IMU_CAN.h \
    can-ids/Devices/INTERFACE_CAN.h \
    can-ids/Devices/LOGGER_CAN.h \
    can-ids/Devices/SNIFFER_CAN.h \
    can-ids/Devices/STEER_CAN.h \
    #can-ids/Devices/SUPOT_CAN.h \
    can-ids/Devices/TE_CAN.h \
    can-ids/CAN_IDs.h \
    can-ids/table.h \
    can-ids/erros.h \
    src/lap_time.hpp \
    src/Custom_Lap.hpp \
    src/Resources/LEDwidget.hpp \
    src/Resources/ledform.hpp \
    src/Resources/filterwidget.hpp \
    src/Resources/plot_filter_widget.hpp \
    src/AMS.hpp \
    src/AMS_Battery.hpp \
    src/AMS_Cell.hpp \
    src/AMS_Cell_Line.hpp \
    src/AMS_Details.hpp \
    src/AMS_Stack.hpp \
    src/AMS_Stack_Details.hpp \
    src/AMS_Stack_Summary.hpp \
    qt-can-com/COM.hpp \
    src/COM_LogConvert.hpp \
    src/COM_LogGen.hpp \
    src/COM_LogPlay.hpp \
    qt-can-com/COM_LogRec.hpp \
    src/COM_Manager.hpp \
    src/COM_Select.hpp \
    qt-can-com/COM_SerialPort.hpp \
    src/Console.hpp \
    src/Console_idlist.hpp \
    src/Console_makeid.hpp \
    src/DevStatus.hpp \
    src/General.hpp \
    src/General_Details.hpp \
    src/General_Details_PotProgress.hpp \
    src/Inverters.hpp \
    src/Main_About.hpp \
    src/Main_SubWindow.hpp \
    src/Main_Window.hpp \
    src/MotorBench.hpp \
    src/Picasso.hpp \
    src/Plotting.hpp \
    src/Printer.hpp \
    src/Report.hpp \
    src/Sensors.hpp \
    src/Sitrep.hpp \
    src/Isabelle.hpp \
    src/UiWindow.hpp \
    qt-can-com/COM_UdpReceiveSock.hpp \
    qt-can-com/COM_UdpSendSock.hpp \
    can-ids/Devices/WATER_TEMP_CAN.h \
    src/Main_Toolbar.hpp \
    src/General_DriverInputs.hpp \
    src/BMS.hpp \
    src/BMS_Stack.hpp \
    src/COM_Interface.hpp \
    src/SQL_CANID.hpp \
    src/SQL_CANID_Select.hpp \
    src/Plot_Callout.hpp \
    src/arm_bars.hpp \
    src/arms_torque_set.hpp \
    src/ccu.hpp \
    src/ccu_indiv.hpp \
    src/comselectline.hpp \
    qt-can-com/COM_LAN.hpp \
    src/exptrk.hpp \
    src/run_laps.hpp \
    src/single_lap.hpp \
    src/state_manager.hpp \
    src/timestamp.hpp \
    src/time.hpp \
    src/isa_config.hpp \
    src/versions.hpp \
    src/pcb_version.hpp \
    src/drs_cal.hpp \
    src/iib_motor_control.hpp \
    src/report_filters.hpp \
    src/report_indiv_filter.hpp \
    src/versionline.hpp\
    src/COM_Kvaser.hpp\
    src/memoryview.hpp \
    src/versions.hpp \
    src/arm.hpp

FORMS +=\
    src/lap_time.ui \
    src/Custom_Lap.ui \
    src/Resources/filterwidget.ui \
    src/Resources/ledform.ui \
    src/Resources/plot_filter_widget.ui \
    src/AMS.ui \
    src/AMS_Cell_Line.ui \
    src/AMS_Details.ui \
    src/AMS_Stack_Details.ui \
    src/AMS_Stack_Summary.ui \
    src/COM_LogConvert.ui \
    src/COM_Select.ui \
    src/Console.ui \
    src/Console_idlist.ui \
    src/Console_makeid.ui \
    src/DevStatus.ui \
    src/General.ui \
    src/General_Details.ui \
    src/General_Details_PotProgress.ui \
    src/Inverters.ui \
    src/Main_About.ui \
    src/Main_SubWindow.ui \
    src/Main_Window.ui \
    src/MotorBench.ui \
    src/Plotting.ui \
    src/Printer.ui \
    src/Report.ui \
    src/Sensors.ui \
    src/Isabelle.ui \
    src/General_DriverInputs.ui \
    src/BMS.ui \
    src/BMS_Stack.ui \
    src/arm.ui \
    src/arm_bars.ui \
    src/arms_torque_set.ui \
    src/ccu.ui \
    src/ccu_indiv.ui \
    src/comselectline.ui \
    src/SQL_CANID_Select.ui \
    src/isa_config.ui \
    src/run_laps.ui \
    src/single_lap.ui \
    src/versions.ui \
    src/pcb_version.ui \
    src/drs_cal.ui \
    src/iib_motor_control.ui \
    src/report_filters.ui \
    src/report_indiv_filter.ui \
    src/versionline.ui \
    src/memoryview.ui \
    src/versions.ui

RESOURCES += \
    ass/Assets.qrc \
    can-ids/messages.csv \
    can-ids/FST09e.csv \
    messages.qrc

DISTFILES += \
    Makefile \
    can-ids/table.txt
