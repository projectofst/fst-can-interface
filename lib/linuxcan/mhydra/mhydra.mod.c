#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x2717bba0, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xc1883777, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x137ba756, __VMLINUX_SYMBOL_STR(queue_init) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x66cbc671, __VMLINUX_SYMBOL_STR(vCanCleanup) },
	{ 0xdae80100, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xd1bd8467, __VMLINUX_SYMBOL_STR(queue_length) },
	{ 0x8e83bc9f, __VMLINUX_SYMBOL_STR(vCanCardRemoved) },
	{ 0xea6d39ec, __VMLINUX_SYMBOL_STR(seq_puts) },
	{ 0x2c00eab8, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0x9277c379, __VMLINUX_SYMBOL_STR(usb_kill_urb) },
	{ 0x4add9142, __VMLINUX_SYMBOL_STR(convert_vcan_to_hydra_cmd) },
	{ 0xff34230a, __VMLINUX_SYMBOL_STR(softSyncLoc2Glob) },
	{ 0x8bee5663, __VMLINUX_SYMBOL_STR(softSyncAddMember) },
	{ 0xa1ce0943, __VMLINUX_SYMBOL_STR(kthread_create_on_node) },
	{ 0x19ad6985, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xaad8c7d6, __VMLINUX_SYMBOL_STR(default_wake_function) },
	{ 0x4ac6402, __VMLINUX_SYMBOL_STR(queue_empty) },
	{ 0xc61fca8a, __VMLINUX_SYMBOL_STR(queue_push) },
	{ 0x53439884, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0xc2bf1f9f, __VMLINUX_SYMBOL_STR(ticks_init) },
	{ 0xfb578fc5, __VMLINUX_SYMBOL_STR(memset) },
	{ 0xd7597bf6, __VMLINUX_SYMBOL_STR(queue_front) },
	{ 0xe88e8fff, __VMLINUX_SYMBOL_STR(queue_add_wait_for_space) },
	{ 0xcee4b22d, __VMLINUX_SYMBOL_STR(vCanDispatchEvent) },
	{ 0x1916e38c, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x64ba5ae4, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x1f339094, __VMLINUX_SYMBOL_STR(usb_deregister) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x7eebc343, __VMLINUX_SYMBOL_STR(ticks_to_64bit_ns) },
	{ 0x35762982, __VMLINUX_SYMBOL_STR(set_capability_value) },
	{ 0x9166fada, __VMLINUX_SYMBOL_STR(strncpy) },
	{ 0xe052748f, __VMLINUX_SYMBOL_STR(queue_wakeup_on_space) },
	{ 0xaf3fbbe2, __VMLINUX_SYMBOL_STR(queue_pop) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0xe97782a4, __VMLINUX_SYMBOL_STR(usb_free_coherent) },
	{ 0x952664c5, __VMLINUX_SYMBOL_STR(do_exit) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x32099d62, __VMLINUX_SYMBOL_STR(vCanCalc_dt) },
	{ 0xb7b33403, __VMLINUX_SYMBOL_STR(softSyncHandleTRef) },
	{ 0xcb2a2964, __VMLINUX_SYMBOL_STR(module_put) },
	{ 0x79e198fe, __VMLINUX_SYMBOL_STR(vCanInitData) },
	{ 0x6d28af40, __VMLINUX_SYMBOL_STR(usb_submit_urb) },
	{ 0xb601be4c, __VMLINUX_SYMBOL_STR(__x86_indirect_thunk_rdx) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xaa4826f1, __VMLINUX_SYMBOL_STR(usb_bulk_msg) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0xa202a8e5, __VMLINUX_SYMBOL_STR(kmalloc_order_trace) },
	{ 0x2ea2c95c, __VMLINUX_SYMBOL_STR(__x86_indirect_thunk_rax) },
	{ 0x14505bb, __VMLINUX_SYMBOL_STR(wake_up_process) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
	{ 0xcbd4898c, __VMLINUX_SYMBOL_STR(fortify_panic) },
	{ 0x94237769, __VMLINUX_SYMBOL_STR(dlc_bytes_to_dlc_fd) },
	{ 0xb99f54ab, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xe259ae9e, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x680ec266, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0xdca8e931, __VMLINUX_SYMBOL_STR(set_capability_mask) },
	{ 0x5b205e34, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xe443c71a, __VMLINUX_SYMBOL_STR(softSyncRemoveMember) },
	{ 0x4f68e5c9, __VMLINUX_SYMBOL_STR(do_gettimeofday) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x69acdf38, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xf24962b4, __VMLINUX_SYMBOL_STR(vCanInit) },
	{ 0xd8c00bc5, __VMLINUX_SYMBOL_STR(vCanFlushSendBuffer) },
	{ 0xe50b8609, __VMLINUX_SYMBOL_STR(usb_register_driver) },
	{ 0x144e3730, __VMLINUX_SYMBOL_STR(queue_remove_wait_for_space) },
	{ 0xb55603fe, __VMLINUX_SYMBOL_STR(queue_back) },
	{ 0xae3f79db, __VMLINUX_SYMBOL_STR(queue_reinit) },
	{ 0x57563c1d, __VMLINUX_SYMBOL_STR(queue_release) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x4610aee6, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x106945d1, __VMLINUX_SYMBOL_STR(usb_alloc_coherent) },
	{ 0x48778fd2, __VMLINUX_SYMBOL_STR(dlc_dlc_to_bytes_fd) },
	{ 0x7f02188f, __VMLINUX_SYMBOL_STR(__msecs_to_jiffies) },
	{ 0xb11cbd18, __VMLINUX_SYMBOL_STR(dlc_dlc_to_bytes_classic) },
	{ 0xa989a566, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0x1344e359, __VMLINUX_SYMBOL_STR(get_usb_root_hub_id) },
	{ 0xd21d2d11, __VMLINUX_SYMBOL_STR(usb_free_urb) },
	{ 0x235a24c0, __VMLINUX_SYMBOL_STR(try_module_get) },
	{ 0x5b1be770, __VMLINUX_SYMBOL_STR(usb_alloc_urb) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=kvcommon,usbcore";

MODULE_ALIAS("usb:v0BFDp0100d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0102d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0104d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0105d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0106d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0107d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0108d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp0109d*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Ad*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Bd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Cd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Dd*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Ed*dc*dsc*dp*ic*isc*ip*in*");
MODULE_ALIAS("usb:v0BFDp010Fd*dc*dsc*dp*ic*isc*ip*in*");

MODULE_INFO(srcversion, "644A7263473B51F02BFE097");
