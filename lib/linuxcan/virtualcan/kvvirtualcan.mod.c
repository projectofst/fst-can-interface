#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

#ifdef RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x2717bba0, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xc1883777, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x66cbc671, __VMLINUX_SYMBOL_STR(vCanCleanup) },
	{ 0xdae80100, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x2c00eab8, __VMLINUX_SYMBOL_STR(seq_printf) },
	{ 0xcad25464, __VMLINUX_SYMBOL_STR(vCanTime) },
	{ 0x4ac6402, __VMLINUX_SYMBOL_STR(queue_empty) },
	{ 0xfb578fc5, __VMLINUX_SYMBOL_STR(memset) },
	{ 0xd7597bf6, __VMLINUX_SYMBOL_STR(queue_front) },
	{ 0xcee4b22d, __VMLINUX_SYMBOL_STR(vCanDispatchEvent) },
	{ 0x64ba5ae4, __VMLINUX_SYMBOL_STR(current_task) },
	{ 0x27e1a049, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x35762982, __VMLINUX_SYMBOL_STR(set_capability_value) },
	{ 0xe052748f, __VMLINUX_SYMBOL_STR(queue_wakeup_on_space) },
	{ 0xaf3fbbe2, __VMLINUX_SYMBOL_STR(queue_pop) },
	{ 0x79e198fe, __VMLINUX_SYMBOL_STR(vCanInitData) },
	{ 0x6b90a31a, __VMLINUX_SYMBOL_STR(vCanGetCardInfo2) },
	{ 0xdb7305a1, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0xa202a8e5, __VMLINUX_SYMBOL_STR(kmalloc_order_trace) },
	{ 0x2ea2c95c, __VMLINUX_SYMBOL_STR(__x86_indirect_thunk_rax) },
	{ 0xbdfb6dbb, __VMLINUX_SYMBOL_STR(__fentry__) },
	{ 0xb99f54ab, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xe259ae9e, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x5b205e34, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xac7fa852, __VMLINUX_SYMBOL_STR(vCanGetCardInfo) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xf24962b4, __VMLINUX_SYMBOL_STR(vCanInit) },
	{ 0xd8c00bc5, __VMLINUX_SYMBOL_STR(vCanFlushSendBuffer) },
	{ 0x57563c1d, __VMLINUX_SYMBOL_STR(queue_release) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=kvcommon";


MODULE_INFO(srcversion, "5583478915CA62B9CA80281");
